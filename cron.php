<?php
define('BASEPATH','yes');

#error_reporting(0);

#if(php_sapi_name() != 'cli') exit;

require_once('/home1/oneqrepu/public_html/cadmin/cronParser.php');
require_once('/home1/oneqrepu/public_html/cadmin/application/config/database.php');
require_once('/home1/oneqrepu/public_html/cadmin/application/third_party/tcpdf/config/lang/eng.php');
require_once('/home1/oneqrepu/public_html/cadmin/application/third_party/tcpdf/tcpdf.php');

$host = $db['default']['hostname'];
$user = $db['default']['username'];
$passwd = $db['default']['password'];
$db = $db['default']['database'];

$con = mysqli_connect($host, $user, $passwd, $db);

function func_query_first($sql){
    global $con;
    $result = mysqli_query($con,$sql);
	if(!empty($result)){
	   return $row = mysqli_fetch_array($result,MYSQL_ASSOC);
	}else{
		return $row = false;
	}
}

function func_query($sql) {
    global $con;
    $myArray = array();
	$result = mysqli_query($con,$sql);
	if(!empty($result)){
		while($row = mysqli_fetch_array($result, MYSQL_ASSOC)){
			$myArray[] = $row;
		}
		return $myArray;
	}
	else
	{
		return false;
	}
}

function docName($id) {
    $q = func_query_first("SELECT firstname,lastname FROM sr_doctors WHERE id = $id");
    if ($q) {
        return $q['firstname'] . ' ' . $q['lastname'];
    } else{
        return 'N/A';
    }
}

/** Get clients **/
$clients = func_query("SELECT id FROM sr_clients ORDER BY id ASC");
if ($clients) {

    foreach ($clients as $client) {
        $cid = $client['id'];
        /** Get jobs **/
        $jobs = func_query("SELECT * FROM sr_schedule WHERE client_id = $cid ORDER BY id ASC");
        if ($jobs) {
            $time = time();
            foreach ($jobs as $job) {
                date_default_timezone_set($job['timezone']);
                $instance = new CronDispatch();
                $time = $time - 60;
                $time = $instance->getNextLaunchTime($job['cron'], $time);
                $currentHour = date('H');
                $currentMinute = date('i');
                $currentDay = date('l');
                /** Cron date **/
                $cronHour = date('H',$time);
                $cronMinute = date('i',$time);
                $cronDay = date('l',$time);
                /** Check if they matches **/
                $now = date('Y-m-d');
                $run = date('Y-m-d',$job['run']);
                
                #$t = mktime(1,0,1,5,25,2016);
                #$t = strtotime($t);
                #$run = date('Y-m-d',$t);
                
                #$cronHour = 16;
                #$cronMinute = 58;
                
                /*echo $currentHour . ' ' . $cronHour."<br />";
                echo $currentMinute . ' ' . $cronMinute."<br />";
                exit;
                /*
                echo $currentDay . ' ' . $cronDay;
                exit;*/
                
                if ($currentHour == $cronHour && $currentMinute == $cronMinute) { #&& $currentDay == $cronDay) {
                    #if (true) {
                    //if ($job['rating'] == 0) {
                        
                        // Location
                        if ($job['location'] != '0') {
                            // Check if location still exists
                            $g = func_query_first("SELECT 1 FROM sr_locations WHERE id = " . $job['location'] . " AND client_id = $cid");
                            if ($g) {
                                $locationWhere = "AND `srl`.`client_id` = $cid AND `srl`.`id` = " . $job['location'];
                            } else {
                                $locationWhere = '';
                            }
                        }
                        
                        if ($job['doctor_id'] != '0') {
                            // Check if doctor still exists
                            $g = func_query_first("SELECT 1 FROM sr_doctors WHERE id = " . $job['doctor_id'] . " AND client_id = $cid");
                            if ($g) {
                                $doctorsWhere = "AND `sr`.`doctor_id` = " . $job['doctor_id'];
                            } else {
                                $doctorsWhere = '';
                            }
                        }
                        
                        $locationSql = "LEFT JOIN `sr_locations` `srl` ON `sr`.`location` = `srl`.`id`";
                        $locationSql_1 = "srl.name AS locationName,";
        
                        $rating_star = $job['rating'];
                        if ($rating_star == 0) {
                            $sql = "SELECT sn.*, $locationSql_1 (sr.id) as rating_id, sr.doctor_id, sr.rating, sr.rating_no, date_format(entry_date, \"%m/%d/%Y\") as date, IF(sr.rating_no = 1, 'Very Dissatisfied', IF(sr.rating_no = 2, 'Dissatisfied', IF(sr.rating_no = 3, 'Satisfied', IF(sr.rating_no = 4, 'Very Satisfied', IF(sr.rating_no = 5, 'Extremely Satisfied', '' ) ) ) ) ) as ratingtext FROM `sr_rating` `sr` $locationSql LEFT JOIN `sr_newuser` `sn` ON `sn`.`id` = `sr`.`user_id` WHERE date(sr.entry_date) >= '$run' AND date(sr.entry_date) <= '$now' AND `sr`.`client_id` = $cid AND `sr`.`is_delete` = 0 $locationWhere $doctorsWhere ORDER BY `entry_date` ASC";
                        } else {
                            $sql = "SELECT sn.*, $locationSql_1 (sr.id) as rating_id, sr.doctor_id, sr.rating, sr.rating_no, date_format(entry_date, \"%m/%d/%Y\") as date, IF(sr.rating_no = 1, 'Very Dissatisfied', IF(sr.rating_no = 2, 'Dissatisfied', IF(sr.rating_no = 3, 'Satisfied', IF(sr.rating_no = 4, 'Very Satisfied', IF(sr.rating_no = 5, 'Extremely Satisfied', '' ) ) ) ) ) as ratingtext FROM `sr_rating` `sr` $locationSql LEFT JOIN `sr_newuser` `sn` ON `sn`.`id` = `sr`.`user_id` WHERE date(sr.entry_date) >= '$run' AND date(sr.entry_date) <= '$now' `sr`.`client_id` = $cid AND `sr`.`is_delete` = 0 AND `sr`.`rating` = $rating_star $locationWhere $doctorsWhere ORDER BY `entry_date` ASC";
                        }
                        
                        $q = func_query($sql);
                        if ($q) {
                            $runed = time();
                            $html = '';
                            mysqli_query($con,"UPDATE sr_schedule SET run = '$runed' WHERE id = " . $job['id']);
                            if ($job['fileFormat'] == 'pdf') {
                                $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                                $obj_pdf->SetCreator(PDF_CREATOR);
                                $title = "Survey";
                                $obj_pdf->SetTitle($title);
                                $obj_pdf->SetHeaderData('','',$title,'');
                                $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                                $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                                $obj_pdf->SetDefaultMonospacedFont('helvetica');
                                $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                                $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                                $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                                $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                                $obj_pdf->SetFont('helvetica', '', 9);
                                $obj_pdf->setFontSubsetting(false);
                                $obj_pdf->AddPage();
                                
                                $html .= '<html>
                                <head></head>
                                <body>
                                <div id="report">
                                <table border="1" width="100%">
                                  <tr style="text-align:center">
                                    <th style="text-align:center">Full Name</th>
                                    <th style="text-align:center">Phone</th>
                                    <th style="text-align:center">Rating</th>
                                    <th style="text-align:center">Location</th>
                                    <th style="text-align:center">Physician</th>
                                    <th style="text-align:center">Date</th>
                                  </tr>';
                                
                                foreach ($q as $r) {
                                  $html .= '<tr style="text-align:center">
                                    <td>'.$r['firstname'].' '.$r['lastname'].'</td>
                                    <td>'.$r['phone_no'].'</td>
                                    <td>'.$r['ratingtext'].'</td>
                                    <td>'.$r['locationName'].'</td>
                                    <td>'.docName($r['doctor_id']).'</td>
                                    <td>'.$r['date'].'</td>
                                  </tr>';
                                }
                                $html .= '</table></div></body></html>';
                                
                                $obj_pdf->writeHTML($html, true, false, true, false, '');
                                $file = 'report_'.date('m_d_Y').'_'.rand(1,50000);
                                $obj_pdf->Output('/home1/oneqrepu/public_html/cadmin/uploads/reports/'.$file.'.pdf', 'F');
                                if (file_exists('/home1/oneqrepu/public_html/cadmin/uploads/reports/' . $file . '.pdf')) {
                                    $file = $file . '.pdf';
                                    $from_name = "Reports";
                                    $from_mail = "reports@1qreputation.com";
                                    $replyto = "reports@1qreputation.com";
                                    
                                    $message = "Here is your report for " . date('m/d/Y');
                                    
                                    $content = chunk_split(base64_encode(file_get_contents('/home1/oneqrepu/public_html/cadmin/uploads/reports/' . $file)));
                                    $uid = md5(uniqid(time()));
                                    $name = basename($file);
                                    
                                    $header = "From: ".$from_name." <".$from_mail.">\n";
                                    $header .= "Reply-To: ".$replyto."\n";
                                    $header .= "MIME-Version: 1.0\n";
                                    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
                                    $emessage= "--".$uid."\n";
                                    $emessage.= "Content-type:text/plain; charset=iso-8859-1\n";
                                    $emessage.= "Content-Transfer-Encoding: 7bit\n\n";
                                    $emessage .= $message."\n\n";
                                    $emessage.= "--".$uid."\n";
                                    $emessage .= "Content-Type: application/octet-stream; name=\"".$file."\"\n"; // use different content types here
                                    $emessage .= "Content-Transfer-Encoding: base64\n";
                                    $emessage .= "Content-Disposition: attachment; filename=\"".$file."\"\n\n";
                                    $emessage .= $content."\n\n";
                                    $emessage .= "--".$uid."--";
                                    mail($job['email'],'Report for ' . date('m/d/Y'),$emessage,$header);   
                                    #mail($job['email'],'Report for ' . date('m/d/Y'),'test');                                  
                                    #mail($job['email'], 'Report for ' . date('m/d/Y'), $message, $header);
                                }
                            } else if ($job['fileFormat'] == 'csv') {
                                $fname = 'report_csv_'.date('m_d_Y').'_'.rand(1,50000).'.csv';
                                $file = '/home1/oneqrepu/public_html/cadmin/uploads/reports/'. $fname;
                                $fp = fopen($file, 'w');
                                
                                fputcsv($fp,array('Firstname','Lastname','Email','Rating','Location','Physician','Date'));
                                
                            	foreach($q as $row) {
                            	    $doctor = docName($row['doctor_id']);
                            	    $data = array($row['firstname'],$row['lastname'],$row['emailid'],$row['ratingtext'],$row['locationName'],$doctor,$row['date']);
                            		fputcsv($fp, $data);
                                }
                                fclose($fp);
                                
                                if (file_exists($file)) {
                                    $from_name = "Reports";
                                    $from_mail = "reports@1qreputation.com";
                                    $replyto = "reports@1qreputation.com";
                                    
                                    $message = "Here is your report for " . date('m/d/Y');
                                    
                                    $content = chunk_split(base64_encode(file_get_contents($file)));
        
                                    $uid = md5(uniqid(time()));
                                    $name = $fname;
                                    $message = "Here is your report for " . date('m/d/Y');
                                    
                                    $content = chunk_split(base64_encode(file_get_contents('/home1/oneqrepu/public_html/cadmin/uploads/reports/' . $name)));
                                    $uid = md5(uniqid(time()));
                                    $name = basename($file);
                                    
                                    $header = "From: ".$from_name." <".$from_mail.">\n";
                                    $header .= "Reply-To: ".$replyto."\n";
                                    $header .= "MIME-Version: 1.0\n";
                                    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
                                    $emessage= "--".$uid."\n";
                                    $emessage.= "Content-type:text/plain; charset=iso-8859-1\n";
                                    $emessage.= "Content-Transfer-Encoding: 7bit\n\n";
                                    $emessage .= $message."\n\n";
                                    $emessage.= "--".$uid."\n";
                                    $emessage .= "Content-Type: application/octet-stream; name=\"".$name."\"\n"; // use different content types here
                                    $emessage .= "Content-Transfer-Encoding: base64\n";
                                    $emessage .= "Content-Disposition: attachment; filename=\"".$name."\"\n\n";
                                    $emessage .= $content."\n\n";
                                    $emessage .= "--".$uid."--";
                                    mail($job['email'],'Report for ' . date('m/d/Y'),$emessage,$header); 
                                }
                            } else {
                                include_once 'application/third_party/PHPExcel.php';
                                include 'application/third_party/PHPExcel/Writer/Excel2007.php';
                                $objPHPExcel = new PHPExcel();
                                
                                $objPHPExcel->getProperties()->setCreator("Digital Marketing Sapiens");
                                $objPHPExcel->getProperties()->setLastModifiedBy("Digital Marketing Sapiens");
                                $objPHPExcel->getProperties()->setTitle("Report for " . date('m/d/Y'));
                                $objPHPExcel->getProperties()->setSubject("Report for " . date('m/d/Y'));
                                $objPHPExcel->getProperties()->setDescription("Report for " . date('m/d/Y'));
                                
                                $objPHPExcel->setActiveSheetIndex(0);
                                $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Firstname');
                                $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Lastname');
                                $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
                                $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Date');
                                $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Location');
                                $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Physician');
                                $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Rating');
                                
                                $rowCount = 2;
                                foreach($q as $row) {
                                    $doctor = docName($row['doctor_id']);
                                    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $row['firstname']); 
                                    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $row['lastname']); 
                                    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $row['emailid']); 
                                    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $row['date']);
                                    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $row['locationName']);
                                    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $doctor);
                                    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $row['ratingtext']); 
                                    $rowCount++;
                                }
                                
                                $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
                                $fname = 'report_xlsx_'.date('m_d_Y').'_'.rand(1,50000).'.xlsx';
                                $file = '/home1/oneqrepu/public_html/cadmin/uploads/reports/'. $fname;
                                $objWriter->save($file);
                                
                                if (file_exists($file)) {
                                    $from_name = "Reports";
                                    $from_mail = "reports@1qreputation.com";
                                    $replyto = "reports@1qreputation.com";
                                    
                                    $message = "Here is your report for " . date('m/d/Y');
                                    
                                    $content = chunk_split(base64_encode(file_get_contents($file)));
        
                                    $uid = md5(uniqid(time()));
                                    $name = $fname;
                                    $header = "From: ".$from_name." <".$from_mail.">\n";
                                    $header .= "Reply-To: ".$replyto."\n";
                                    $header .= "MIME-Version: 1.0\n";
                                    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
                                    $emessage= "--".$uid."\n";
                                    $emessage.= "Content-type:text/plain; charset=iso-8859-1\n";
                                    $emessage.= "Content-Transfer-Encoding: 7bit\n\n";
                                    $emessage .= $message."\n\n";
                                    $emessage.= "--".$uid."\n";
                                    $emessage .= "Content-Type: application/octet-stream; name=\"".$name."\"\n"; // use different content types here
                                    $emessage .= "Content-Transfer-Encoding: base64\n";
                                    $emessage .= "Content-Disposition: attachment; filename=\"".$name."\"\n\n";
                                    $emessage .= $content."\n\n";
                                    $emessage .= "--".$uid."--";
                                    mail($job['email'],'Report for ' . date('m/d/Y'),$emessage,$header); 
                                }
                            }
                        }
                    //}
                }
            }
        }
    } // foreach clients

}