<?php
Header('Access-Control-Allow-Origin: *');
/**
 * Smoke APP reputation mobile api
 * v.0.1
 * @copyright dmsapiens.com
 */
error_reporting(0);
ini_set('display_error','Off');

function debugf($text) {
    $myfile = fopen("debug.txt", "a");
    $date = date("d/m/Y H:i:s");
    fwrite($myfile,"DEBUG $date : $text\r\n");
    fclose($myfile);
}

$base_url = 'http://dashboard.1qreputation.com';

$servername = 'localhost';
$username = 'oneqrepu_ifwh';
$password = '5nMwhazEQZ47';
$db = 'oneqrepu_cadmin';

require_once 'lib/mail.php';

/** Error function **/
function error($msg) {
    echo json_encode(array('status'=>false,'msg'=>$msg));
    exit;
}

function func_query_first($sql){
    global $con;
    $result = mysqli_query($con,$sql);
	if(!empty($result)){
	   return $row = mysqli_fetch_array($result,MYSQL_ASSOC);
	}else{
		return $row = false;
	}
}

function func_query($sql) {
    global $con;
    $myArray = array();
	$result = mysqli_query($con,$sql);
	if(!empty($result)){
		while($row = mysqli_fetch_array($result, MYSQL_ASSOC)){
			$myArray[] = $row;
		}
		return $myArray;
	}
	else
	{
		return false;
	}
}

$client_id = false;

$con = mysqli_connect($servername, $username, $password,$db);

if (mysqli_connect_error()) {
    error('Could not connect to database, please try again!');
}

mysqli_query($con,"SET NAMES utf8");

if (!isset($_GET['key'])) {
    error('Wrong API key!');
} else {
    $cc = func_query_first("SELECT 1 FROM sr_clients WHERE id = " . (int)$_GET['key']);
    if ($cc) {
        $client_id = (int)$_GET['key'];
    } else {
        error('Wrong API key!');
    }
}

if ($_GET['route'] == 'err') {
    $data = json_decode(file_get_contents("php://input"),true);
    $err = $data['data'];
    $myfile = fopen("testfile.txt", "a");
    fwrite($myfile,"DEBUG: $err\r\n");
    fclose($myfile);
}

/**
 * Search for closest store
 */
if ($_GET['route'] == 'closest') {
    $data = json_decode(file_get_contents("php://input"),true);
    $origLon = $data['data']['lon'];
    $origLat = $data['data']['lat'];
    
    $dist = 50; // miles
    $tableName = 'sr_locations';
    $query = "SELECT id, latitude, longitude, 3956 * 2 * ASIN(SQRT( POWER(SIN(($origLat - latitude)*pi()/180/2),2)+COS($origLat*pi()/180 )*COS(latitude*pi()/180)*POWER(SIN(($origLon-longitude)*pi()/180/2),2))) as distance FROM $tableName WHERE client_id = $client_id AND longitude between ($origLon-$dist/cos(radians($origLat))*69) and ($origLon+$dist/cos(radians($origLat))*69) and latitude between ($origLat-($dist/69)) and ($origLat+($dist/69)) having distance < $dist ORDER BY distance limit 1";
    
    $q = func_query_first($query);
    
    /** Uncoment for debug **
    $doctors = func_query("SELECT * FROM sr_doctors WHERE client_id = $client_id AND FIND_IN_SET(3,location) ORDER BY firstname ASC");
    $final = array();
    foreach ($doctors as $r) {
        $final[] = array('id'=>$r['id'],'name'=>$r['firstname'] . ' ' . $r['lastname'],'address'=>$r['address']);
    }
    echo json_encode(array('status'=>true,'results'=>$final,'location_id'=>3));
    exit;*/
    
    $myfile = fopen("gps.txt", "a");
    fwrite($myfile,"DEBUG: $origLon - $origLat\r\n");
    fclose($myfile);
    
    if (!$q) {
        echo json_encode(array('status'=>false));
        exit;
    } else {
        $lid = $q['id'];
        $doctors = func_query("SELECT * FROM sr_doctors WHERE client_id = $client_id AND FIND_IN_SET($lid,location) ORDER BY firstname ASC");
        if (!$doctors) {
            echo json_encode(array('status'=>false));
            exit;
        } else {
            $final = array();
            $counter = 0;
            foreach ($doctors as $r) {
                $final[] = array('id'=>$r['id'],'name'=>$r['firstname'] . ' ' . $r['lastname'],'address'=>$r['address']);
                $counter++;
                #if ($counter == 6) break;
            }
            echo json_encode(array('status'=>true,'results'=>$final,'location_id'=>$lid));
        }
    }
    exit;
}

if ($_GET['route'] == 'fallbackgps') {
    $tableName = 'sr_locations';
    $query = "SELECT * FROM $tableName WHERE client_id = $client_id ORDER BY id DESC";
    
    $q = func_query($query);
    
    if (!$q) {
        $res = func_query("SELECT * FROM $tableName WHERE client_id = $client_id ORDER BY id ASC");
        echo json_encode(array('status'=>false,'msg'=>'No locations found. Sorry, we can\'t continue.','results'=>$res));
    } else {
        $final = array();
        foreach ($q as $r) {
            $final[] = array('id'=>$r['id'],'name'=>$r['name'],'address'=>$r['address1'],'city'=>$r['city'],'zip'=>$r['zipcode'],'state'=>$r['state']);
        }
        echo json_encode(array('status'=>true,'dist'=>$dist,'results'=>$final));
    }
    exit;
}

/**
 * New user routine
 */
if ($_GET['route'] == 'new') {
    $data = json_decode(file_get_contents("php://input"),true);
    $firstname = $data['data']['Firstname'];
    $lastname = $data['data']['Lastname'];
    $email = $data['data']['Email'];
    $cell = $data['data']['Cell'];
    
    $check = func_query_first("SELECT 1 FROM sr_newuser WHERE phone_no = '$cell' AND client_id = $client_id");
    if ($check) {
        echo json_encode(array('status'=>false,'msg'=>'Phone number already exists.'));
        exit;
    }
    
    mysqli_query($con,"INSERT INTO sr_newuser (firstname,lastname,phone_no,emailid,is_delete,client_id) VALUES ('$firstname','$lastname','$cell','$email','0','$client_id')");
    
    $id = mysqli_insert_id($con);
    
    echo json_encode(array('status'=>true,'id'=>$id));
    exit;
}

/**
 * Dissatisfied routine
 */
if ($_GET['route'] == 'dissatisfied') {
    $data = json_decode(file_get_contents("php://input"),true);
    $userid = $data['data']['userid'];
    $rate = $data['data']['rate'];
    $title = $data['data']['title'];
    $desc = $data['data']['desc'];
    $location = $data['data']['location'];
    $date = date('Y-m-d H:i:s');
    $session_id = md5(time());
    $rating_id = $data['data']['ratingid'];
    echo json_encode(array('status'=>true));
    exit;
}

/**
 * Rate routine
 */
if ($_GET['route'] == 'rateit') {
    $data = json_decode(file_get_contents("php://input"),true);
    $userid = $data['data']['userid'];
    $rate = $data['data']['rate'];
    $date = date('Y-m-d H:i:s');
    $session_id = md5(time());
    $rating_id = $data['data']['ratingid'];
    
    mysqli_query($con,"INSERT INTO sr_rating (user_id,rating,rating_no,session_id,entry_date,is_delete,client_id) VALUES ('$userid','$rate','$rate','$session_id','$date','0','$client_id')");
    $rating_id = mysqli_insert_id($con);
    
    echo json_encode(array('status'=>true,'id'=>$rating_id,'session'=>$session_id));
    exit;
}

/**
 * Log share route
 */
if ($_GET['route'] == 'logshare') {
    $data = json_decode(file_get_contents("php://input"),true);
    $userid = $data['data']['userid'];
    $rate = $data['data']['rate'];
    $type = $data['data']['type'];
    $location = $data['data']['location'];
    $doctor_id = (isset($data['data']['doctor_id'])) ? $data['data']['doctor_id'] : '0';
    $date = date('Y-m-d H:i:s');
    
    mysqli_query($con,"INSERT INTO sr_socialshare (user_id,social_type,rating,entry_date,location,client_id,doctor_id) VALUES ('$userid','$type','$rate','$date','$location','$client_id','$doctor_id')");
    
    echo json_encode(array('status'=>true));
    exit;
}

/**
 * Review routine
 */
if ($_GET['route'] == 'submitreview') {
    $data = json_decode(file_get_contents("php://input"),true);
    $userid = $data['data']['userid'];
    $rate = $data['data']['your_rate'];
    $review = $data['data']['comment'];
    $location = $data['data']['location'];
    $session_id = md5(time() + rand(1,1111111));
    $doctor_id = (isset($data['data']['doctor_id'])) ? $data['data']['doctor_id'] : '0';
    $date = date('Y-m-d H:i:s');
    
    if ($userid == '' || $rate == '' || $review == '') {
        echo json_encode(array('status'=>false));
        exit;
    }
    
   /* if ($userid == 3) {
        echo json_encode(array('status'=>true));
        exit;
    }*/
    
    if ($review == 'Upload') {
        echo json_encode(array('status'=>false));
        exit;
    }

    /** Insert data **/
    mysqli_query($con,"INSERT INTO sr_rating (user_id,rating,rating_no,session_id,entry_date,is_delete,location,client_id,doctor_id) VALUES ('$userid','$rate','$rate','$session_id','$date','0','$location','$client_id','$doctor_id')");
    $rating_id = mysqli_insert_id($con);
    
    /** Insert rating **/
    if ($rate < 4) {
        $status = 0;
    } else {
        $status = 1;
    }
    
    $review = mysqli_real_escape_string($con,$review);
    
    mysqli_query($con,"INSERT INTO sr_review (user_id,review,rating_id,rating_no,status,location,client_id) VALUES ('$userid','$review','$rating_id','$rate','$status','$location','$client_id')");
    
    debugf("User id: $userid submitted text review at $date");
    
    /** Send email **/
    $results = mysqli_query($con,"SELECT admin_email,senderEmail,emails3,4_5_mail,3_under_mail,email_img,email_img1 FROM sr_locations WHERE id = $location AND client_id = $client_id");
    $r = mysqli_fetch_array($results,MYSQL_ASSOC);
    $email = $r['email'];
    
    /** Get firstname of customer **/
    $fname = mysqli_query($con,"SELECT firstname,lastname,emailid,phone_no FROM sr_newuser WHERE id = $userid AND client_id = $client_id");
    $f = mysqli_fetch_array($fname,MYSQL_ASSOC);
    
    if ($rate <= 3) {
        debugf('Sending written review for ' . $rate . ' stars - Location: ' . $location);
        $text = str_replace('%s',$f['firstname'],$r['3_under_mail']);
        $text = str_replace('%r',$rate,$text);
        if ($f['emailid'] != '') {
            $text = str_replace('%e',$f['emailid'],$text);
        } else {
            $text = str_replace('%e','No email address entered',$text);
        }
        $text = str_replace('%p',$f['phone_no'],$text);
        $text = str_replace('%l',$f['lastname'],$text);
        $text = str_replace('%t',nl2br($review),$text);
        $text = nl2br($text);
        $file = file_get_contents('templates/3under.php');
        $file = str_replace('%text',$text,$file);
        
        $clogo = func_query_first("SELECT logo FROM sr_clients WHERE client_id = $client_id");
        if ($clogo) {
            $cl = 'http://dashboard.1qreputation.com/logos/' . $clogo['logo'];
            $file = str_replace('%sitelogo',$cl,$file);
        }
        
        /** Mobile text **/
        $mobile = false;
        $mobile = $r['3_under_mail'];
        $mobile = str_replace('%s',$f['firstname'],$mobile);
        $mobile = str_replace('%r',$rate,$mobile);
        if ($f['emailid'] != '') {
            $mobile = str_replace('%e',$f['emailid'],$mobile);
        } else {
            $mobile = str_replace('%e','No email address entered',$mobile);
        }
        $mobile = str_replace('%p',$f['phone_no'],$mobile);
        $mobile = str_replace('%l',$f['lastname'],$mobile);
        $mobile = str_replace('%t',$review,$mobile);
        
        /** Check image **/
        if ($r['email_img1'] != '') {
            $image = "<div class='image' style='font-size: 12px;mso-line-height-rule: at-least;font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-family: 'Open Sans',sans-serif;color: #60666d;' align='center'>
              <img class='gnd-corner-image gnd-corner-image-center gnd-corner-image-top' style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 900px;' src='".$r['email_img1']."' alt='' width='600' height='156' />
            </div>";
            $file = str_replace('%image',$image,$file);
        } else {
            $file = str_replace('%image','',$file);
        }
        
        /** Send emails for 3 and under **/
        $three_and_under = func_query_first("SELECT emails_321 FROM sr_locations WHERE id = $location AND client_id = $client_id");
        $mails = explode(',',$three_and_under['emails_321']);
        foreach ($mails as $mail) {
            
            if (strpos($mail,'mms.att') !== false || strpos($mail,'vtext.com') !== false || strpos($mail,'txt.att') !== false || strpos($mail,'tmomail.net') !== false) {
                debugf("Sent textual message to mms / sms $mail");
                sendMobile($mail,$mobile,'Reviews');
            } else {
                debugf("Sent review email to $mail for stars $rate - Location Id: $location Sent from email: {$r['senderEmail']}");
                send_email($mail,'New Review for company',$file,$r['senderEmail'],'IFWH');
            }
        }
    } else {
        debugf('Sending written review to admin for ' . $rate . ' stars - Location: ' . $location);
        $text_for_admin = nl2br($r['admin_email']);
        $text_for_admin = str_replace('%s',$f['firstname'],$text_for_admin);
        $text_for_admin = str_replace('%r',$rate,$text_for_admin);
        if ($f['emailid'] != '') {
            $text_for_admin = str_replace('%e',$f['emailid'],$text_for_admin);
        } else {
            $text_for_admin = str_replace('%e','No email address entered',$text_for_admin);
        }
        $text_for_admin = str_replace('%p',$f['phone_no'],$text_for_admin);
        $text_for_admin = str_replace('%l',$f['lastname'],$text_for_admin);
        $text_for_admin = str_replace('%t',$review,$text_for_admin);
        
        $file1 = file_get_contents('templates/3under.php');
        $file1 = str_replace('%text',$text_for_admin,$file1);
        
        $clogo = func_query_first("SELECT logo FROM sr_clients WHERE client_id = $client_id");
        if ($clogo) {
            $cl = 'http://dashboard.1qreputation.com/logos/' . $clogo['logo'];
            $file1 = str_replace('%sitelogo',$cl,$file1);
        }
        
        $text = str_replace('%s',$f['firstname'],$r['4_5_mail']);
        $text = str_replace('%r',$rate,$text);
        if ($f['emailid'] != '') {
            $text = str_replace('%e',$f['emailid'],$text);
        } else {
            $text = str_replace('%e','No email address entered',$text);
        }
        $text = str_replace('%p',$f['phone_no'],$text);
        $text = str_replace('%l',$f['lastname'],$text);
        $text = str_replace('%t',nl2br($review),$text);
        $text = nl2br($text);
        $file = file_get_contents('templates/template.php');
        $file = str_replace('%text',$text,$file);
        
        $clogo = func_query_first("SELECT logo FROM sr_clients WHERE client_id = $client_id");
        if ($clogo) {
            $cl = 'http://dashboard.1qreputation.com/logos/' . $clogo['logo'];
            $file = str_replace('%sitelogo',$cl,$file);
        }
        
        /** Mobile text **/
        $mobile = false;
        $mobile = $r['4_5_mail'];
        $mobile = str_replace('%s',$f['firstname'],$mobile);
        $mobile = str_replace('%r',$rate,$mobile);
        if ($f['emailid'] != '') {
            $mobile = str_replace('%e',$f['emailid'],$mobile);
        } else {
            $mobile = str_replace('%e','No email address entered',$mobile);
        }
        $mobile = str_replace('%p',$f['phone_no'],$mobile);
        $mobile = str_replace('%l',$f['lastname'],$mobile);
        $mobile = str_replace('%t',$review,$mobile);
        
        /** Check image **/
        if ($r['email_img'] != '') {
            $image = "<div class='image' style='font-size: 12px;mso-line-height-rule: at-least;font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-family: 'Open Sans',sans-serif;color: #60666d;' align='center'>
              <img class='gnd-corner-image gnd-corner-image-center gnd-corner-image-top' style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 900px;' src='".$r['email_img']."' alt='' width='600' height='156' />
            </div>";
            $file = str_replace('%image',$image,$file);
        } else {
            $file = str_replace('%image','',$file);
        }
        
        send_email($f['emailid'],'Thank you for your review',$file,$r['senderEmail'],$f['firstname']);
        debugf("Sent review (for customer/patient) email to ".$f['emailid']." for stars $rate - Location Id: $location");
        
        $three_and_under = func_query_first("SELECT emails_45 FROM sr_locations WHERE id = $location AND client_id = $client_id");
        $mails = explode(',',$three_and_under['emails_45']);
        foreach ($mails as $mail) {
            if (strpos($mail,'mms.att') !== false || strpos($mail,'vtext.com') !== false || strpos($mail,'txt.att') !== false || strpos($mail,'tmomail.net') !== false) {
                debugf("Sent textual message to mms / sms $mail");
                sendMobile($mail,$mobile,'Reviews');
            } else {
                debugf("Sent review email to $mail for stars $rate (4-5) - Location Id: $location");
                send_email($mail,'New Review for company',$file1,$r['senderEmail'],'IFWH');
            }
        }
    }
    
    echo json_encode(array('status'=>true));
    exit;
}

/** Load url **/
if ($_GET['route'] == 'loadurl') {
    $data = json_decode(file_get_contents("php://input"),true);
    $network = $data['data']['type'];
    
    if (isset($_GET['lid']) && $_GET['lid'] != '') {
        $loc_id = (int)$_GET['lid'];
    } else {
        $loc_id = 1;
    }
    
    #$q = func_query_first("SELECT link,description,logo FROM sr_social WHERE name = '$network' LIMIT 1");
    $q = func_query_first("SELECT link,description,logo FROM sr_social WHERE name = '$network' AND location_id = $loc_id AND client_id = $client_id LIMIT 1");
    if ($q) {
        if ($network == 'fb') {
            $link = 'http://www.facebook.com/sharer/sharer.php?s=100&&p[url]=' . urlencode($q['link']);
            echo json_encode(array('status'=>true,'link'=>$link));
        } else if ($network == 'yelp') {
            echo json_encode(array('status'=>true,'link'=>$q['link']));
        } else if ($network == 'pinterest') {
            if ($q['logo'] == '') {
                echo json_encode(array('status'=>false));
                exit;
            }
            $link = 'http://www.pinterest.com/pin/create/button/?url=' . urlencode($q['link']) . '&media=' . urlencode($q['logo']) . '&description=' . urlencode($q['description']);
            echo json_encode(array('status'=>true,'link'=>$link));
        } else if ($network == 'twitter') {
            $link = 'https://www.twitter.com/share?text=' . urlencode($q['description']) . '&url=' . urlencode($q['link']);
            echo json_encode(array('status'=>true,'link'=>$link));
        }
        exit;
    }
    
    echo json_encode(array('status'=>false));
    exit;
}

/** Load texts **/
if ($_GET['route'] == 'loaddata') {
    debugf("Loading data for location id: " . $_GET['lid']);
    
    if (isset($_GET['fetch']) && $_GET['fetch'] == 1) {
        echo json_encode(array('status'=>true));
        exit;
    }
    
    if (isset($_GET['lid']) && $_GET['lid'] != '') {
        $loc_id = (int)$_GET['lid'];
    } else {
        /** Get first location **/
        $q = func_query_first("SELECT id FROM sr_locations WHERE client_id = $client_id ORDER BY id ASC LIMIT 1");
        $loc_id = $q['id'];
    }
    
    $results = mysqli_query($con,"SELECT * FROM sr_locations WHERE id = $loc_id AND client_id = $client_id");
    $r = mysqli_fetch_array($results,MYSQL_ASSOC);
    
    $social = false;
    $social_networks = mysqli_query($con,"SELECT name FROM sr_social WHERE active = 1 AND location_id = $loc_id AND client_id = $client_id ORDER BY name ASC");
    if (mysqli_num_rows($social_networks) > 0) {
        while ($row = $social_networks->fetch_assoc()) {
            $social[] = $row['name'];
        }
    }
    
    $q = func_query_first("SELECT COUNT(*) AS total, id FROM sr_locations WHERE client_id = $client_id");
    if ($q['total'] == 1) {
        $locations = 1;
        $location_id = $q['id'];
    } else {
        $locations = $q['total'];
        $location_id = false;
    }
    
    #debugf(print_r($r,true));
    echo json_encode(array('status'=>true,'location'=>$r,'social'=>$social,'url'=>$base_url));
    exit;
}

/**
 * Finish
 */
if ($_GET['route'] == 'finish') {
    $data = json_decode(file_get_contents("php://input"),true);
    $userid = $data['data']['userid'];
    $file = $data['data']['file'];
    $rate = $data['data']['your_rate'];
    $location = $data['data']['location'];
    $doctor_id = (isset($data['data']['doctor_id'])) ? $data['data']['doctor_id'] : '0';
    $session_id = md5(time() + rand(1,1111111));
    $date = date('Y-m-d H:i:s');
    $date1 = time();
    
    $back = $file;
    $fileinfo = pathinfo($back);
    $ext = $fileinfo['extension'];
    
    $temp = explode('/',$file);
    if ($temp[1] == 'private') {
        $file = end($temp);
        if ($file == 'capturedvideo.MOV') {
            $file = rand(1,10000).'_'.time().'.MOV';
            @rename('uploads/capturedvideo.MOV','uploads/'.$file);
        }
    }
    
    /** Get extension of the file **/
    
    if ($ext == 'MOV') {
        $link = 'http://dashboard.1qreputation.com/uploads/' . basename($file);
    } elseif ($ext == 'm4a') {
        $link = 'http://dashboard.1qreputation.com/done/' . basename($back) . '.mp3';
    } elseif ($ext == '3gp') {
        $link = 'http://dashboard.1qreputation.com/done/' . basename($back) . '.mp4';
    }
    
    /** Insert data **/
    mysqli_query($con,"INSERT INTO sr_rating (user_id,rating,rating_no,session_id,entry_date,is_delete,location,client_id,doctor_id) VALUES ('$userid','$rate','$rate','$session_id','$date','0','$location','$client_id','$doctor_id')");
    $rating_id = mysqli_insert_id($con);
    
    /** Insert rating **/
    if ($rate < 4) {
        $status = 0;
    } else {
        $status = 1;
    }
    mysqli_query($con,"INSERT INTO sr_review (user_id,review,rating_id,rating_no,status,location,client_id) VALUES ('$userid','Upload','$rating_id','$rate','$status','$location','$client_id')");
    
    mysqli_query($con,"INSERT INTO sr_reviews (userId,file,date,rating_id,client_id) VALUES ('$userid','$file','$date1','$rating_id','$client_id')");
    
    /** Send email **/
    $results = mysqli_query($con,"SELECT admin_email,senderEmail,emails3,4_5_mail,3_under_mail,email_img,email_img1 FROM sr_locations WHERE id = $location AND client_id = $client_id");
    $r = mysqli_fetch_array($results,MYSQL_ASSOC);
    $email = $r['email'];
    
    /** Get firstname of customer **/
    $fname = mysqli_query($con,"SELECT firstname,lastname,emailid,phone_no FROM sr_newuser WHERE id = $userid AND client_id = $client_id");
    $f = mysqli_fetch_array($fname,MYSQL_ASSOC);
    
    if ($rate <= 3) {
        $text = str_replace('%s',$f['firstname'],$r['3_under_mail']);
        $text = str_replace('%r',$rate,$text);
        if ($f['emailid'] != '') {
            $text = str_replace('%e',$f['emailid'],$text);
        } else {
            $text = str_replace('%e','No email address entered',$text);
        }
        $text = str_replace('%p',$f['phone_no'],$text);
        $text = str_replace('%l',$f['lastname'],$text);
        $text = str_replace('%t',$link,$text);
        $text = nl2br($text);
        $file = file_get_contents('templates/3under.php');
        $file = str_replace('%text',$text,$file);
        
        $clogo = func_query_first("SELECT logo FROM sr_clients WHERE client_id = $client_id");
        if ($clogo) {
            $cl = 'http://dashboard.1qreputation.com/logos/' . $clogo['logo'];
            $file = str_replace('%sitelogo',$cl,$file);
        }
        
        /** Check image **/
        if ($r['email_img1'] != '') {
            $image = "<div class='image' style='font-size: 12px;mso-line-height-rule: at-least;font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-family: 'Open Sans',sans-serif;color: #60666d;' align='center'>
              <img class='gnd-corner-image gnd-corner-image-center gnd-corner-image-top' style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 900px;' src='".$r['email_img1']."' alt='' width='600' height='156' />
            </div>";
            $file = str_replace('%image',$image,$file);
        }
        
        /** Send emails for 3 and under **/
        $three_and_under = func_query_first("SELECT emails_321 FROM sr_locations WHERE id = $location AND client_id = $client_id");
        $mails = explode(',',$three_and_under['emails_321']);
        foreach ($mails as $mail) {
            if (strpos($mail,'mms.att') !== false || strpos($mail,'vtext.com') !== false || strpos($mail,'txt.att') !== false || strpos($mail,'tmomail.net') !== false) {
                debugf("Sent textual message to mms / sms $mail");
                $textsms = 'New review has been submitted. ' . $f['firstname'] . ' ' . $f['lastname'] . ' gave you ' . $rate . ' star(s). Phone number is ' . $f['phone_no'];
                send_email($mail,'New Review for company',$textsms,$r['senderEmail'],'IFWH',true);
            } else {
                send_email($mail,'New Review for company',$file,$r['senderEmail'],'IFWH');
            }
        }
    } else {
        #$text_for_admin = $f['firstname'] . ' has reviewed your company with ' . $rate . ' stars';
        #$text_for_admin = $f['firstname'] . ' has reviewed your company with ' . $rate . ' stars. <br />Email: ' . $f['emailid'] . '<br />Phone: ' . $f['phone_np'] . '<br />Review:<br />'.$link;
        
        $text_for_admin = nl2br($r['admin_email']);
        $text_for_admin = str_replace('%s',$f['firstname'],$text_for_admin);
        $text_for_admin = str_replace('%r',$rate,$text_for_admin);
        if ($f['emailid'] != '') {
            $text_for_admin = str_replace('%e',$f['emailid'],$text_for_admin);
        } else {
            $text_for_admin = str_replace('%e','No email address entered',$text_for_admin);
        }
        $text_for_admin = str_replace('%p',$f['phone_no'],$text_for_admin);
        $text_for_admin = str_replace('%l',$f['lastname'],$text_for_admin);
        $text_for_admin = str_replace('%t',$review,$text_for_admin);
        
        $file1 = file_get_contents('templates/3under.php');
        $file1 = str_replace('%text',$text_for_admin,$file1);
        
        $clogo = func_query_first("SELECT logo FROM sr_clients WHERE client_id = $client_id");
        if ($clogo) {
            $cl = 'http://dashboard.1qreputation.com/logos/' . $clogo['logo'];
            $file1 = str_replace('%sitelogo',$cl,$file1);
        }
        
        $text = str_replace('%s',$f['firstname'],$r['4_5_mail']);
        $text = str_replace('%r',$rate,$text);
        if ($f['emailid'] != '') {
            $text = str_replace('%e',$f['emailid'],$text);
        } else {
            $text = str_replace('%e','No email address entered',$text);
        }
        $text = str_replace('%p',$f['phone_no'],$text);
        $text = str_replace('%l',$f['lastname'],$text);
        $text = str_replace('%t',$link,$text);
        $text = nl2br($text);
        $file = file_get_contents('templates/template.php');
        $file = str_replace('%text',$text,$file);
        
        $clogo = func_query_first("SELECT logo FROM sr_clients WHERE client_id = $client_id");
        if ($clogo) {
            $cl = 'http://dashboard.1qreputation.com/logos/' . $clogo['logo'];
            $file = str_replace('%sitelogo',$cl,$file);
        }
        
        /** Check image **/
        if ($r['email_img'] != '') {
            $image = "<div class='image' style='font-size: 12px;mso-line-height-rule: at-least;font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-family: 'Open Sans',sans-serif;color: #60666d;' align='center'>
              <img class='gnd-corner-image gnd-corner-image-center gnd-corner-image-top' style='border: 0;-ms-interpolation-mode: bicubic;display: block;max-width: 900px;' src='".$r['email_img']."' alt='' width='600' height='156' />
            </div>";
            $file = str_replace('%image',$image,$file);
        }
        
        send_email($f['emailid'],'Thank you for your review',$file,$r['senderEmail'],$f['firstname']);
        
        /** Send emails for 3 and under **/
        $three_and_under = func_query_first("SELECT emails_45 FROM sr_locations WHERE id = $location AND client_id = $client_id");
        $mails = explode(',',$three_and_under['emails_45']);
        foreach ($mails as $mail) {
            if (strpos($mail,'mms.att') !== false || strpos($mail,'vtext.com') !== false || strpos($mail,'txt.att') !== false || strpos($mail,'tmomail.net') !== false) {
                debugf("Sent textual message to mms / sms $mail");
                $textsms = 'New review has been submitted. ' . $f['firstname'] . ' ' . $f['lastname'] . ' gave you ' . $rate . ' star(s). Phone number is ' . $f['phone_no'];
                send_email($mail,'New Review for company',$textsms,$r['senderEmail'],'IFWH',true);
            } else {
                send_email($mail,'New Review for company',$file1,$r['senderEmail'],'IFWH');
            }
        }
    }
    
    
    echo json_encode(array('status'=>true));
    exit;
}

/**
 * Find routine
 */
if ($_GET['route'] == 'search') {
    $data = json_decode(file_get_contents("php://input"),true);
    $email = $data['data']['mail'];
    $phone = $data['data']['phone'];
    $email = '';
    
    if ($phone == '') {
        $results = mysqli_query($con,"SELECT * FROM sr_newuser WHERE client_id = $client_id ORDER BY id ASC");
        if (mysqli_num_rows($results) == 0) {
            error('There are not customers in the database.');
        } else {
            $res = array();
            while ($r = mysqli_fetch_array($results,MYSQL_ASSOC)) {
                $res[] = $r;
            }
        }
        echo json_encode(array('status'=>true,'results'=>$res));
        exit;
    }
    
    if ($phone != '') {
        $q = func_query_first("SELECT * FROM sr_newuser WHERE phone_no = '$phone' AND client_id = $client_id LIMIT 1");
        if (!$q) {
            error('Could not find any customer with this phone number.');
        } else {
            echo json_encode(array('status'=>true,'id'=>$q['id'],'firstname'=>$q['firstname'],'lastname'=>$q['lastname']));
            exit;
        }
    }

}

/** Hande upload voice **/
if ($_GET['route'] == 'uploadvoice') {
    $data = json_decode(file_get_contents("php://input"),true);
    print_r($data);
}

function send_email($to,$subject,$text,$from,$name = false,$mobile = false) {
    if ($to == '') return false;
    
    $mail = new SimpleMail();
    
    if (!$mobile) {
        $mail->setTo($to, $name)
             ->setSubject($subject)
             ->setFrom('reviews@1qreputation.com', 'Review')
             ->addMailHeader('Reply-To', 'no-reply@1qreputation.com', '1qreputation.com')
             ->addGenericHeader('X-Mailer', 'PHP/' . phpversion())
             ->addGenericHeader('Content-Type', 'text/html; charset="utf-8"')
             ->setMessage($text);
        $send = $mail->send();
    } else {
        $mail->setTo($to, $name)
             ->setSubject($subject)
             ->setFrom('reviews@1qreputation.com', 'Review')
             ->addGenericHeader('X-Mailer', 'PHP/' . phpversion())
             ->setMessage($text);
        $send = $mail->send();
    }
}

function sendMobile($to,$text,$name) {
    $mail = new SimpleMail();
    $mail->setTo($to, $name)
             ->setSubject('New Review')
             ->setFrom('reviews@1qreputation.com', 'Review')
             ->addGenericHeader('X-Mailer', 'PHP/' . phpversion())
             ->setMessage($text);
        $send = $mail->send();
}