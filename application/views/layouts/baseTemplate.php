<html>
   <head>
      <meta charset="utf-8">
      <title>Reputation Management Application - ADMIN PANEL</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- basic Meta Tags -->
      <meta name="keywords" content="key">
      <meta name="description" content="description">
      <!-- // Favicon -->
      <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <?php echo link_tag('assets/admin/css/main.css'); ?>
      <?php echo link_tag('assets/admin/css/extra.css'); ?>
      <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/video.css'); ?>" media="screen"/>
      <link rel="stylesheet" href="<?php echo base_url('assets/css/select2.css'); ?>" media="screen"/>
      <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.Jcrop.css'); ?>" media="screen"/>
      <link href="//vjs.zencdn.net/5.5.3/video-js.css" rel="stylesheet"/>
      <script src="//vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>
      <script src="<?php echo base_url('assets/admin/js/jquery-1.11.1.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/admin/js/bootstrap.js'); ?>"></script>
      <script src="<?php echo base_url('assets/admin/js/plugin.js'); ?>"></script>
      <script src="<?php echo base_url('reviews/listing/assets/mediaelement-and-player.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/admin/js/equal.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/crop.js'); ?>"></script>
      <link rel="stylesheet" href="<?php echo base_url('assets/fancy/jquery.fancybox.css?v=2.1.5'); ?>" type="text/css" media="screen" />
      <script type="text/javascript" src="<?php echo base_url('assets/fancy/jquery.fancybox.js?v=2.1.5'); ?>"></script>
   </head>
   <body>
   
      <?php if (isset($this->session->userdata['switched']) && $this->session->userdata['switched'] == 1): ?>
        <div id="infoDiv"><p><strong>You are now logged in as:</strong> <?php echo $this->session->userdata['logged_in']['firstname']." ".$this->session->userdata['logged_in']['lastname']; ?> <strong>from</strong> <?php echo $this->session->userdata['logged_in']['company']; ?></p></div>
      <?php endif; ?>
      
      <?php $person = $this->misc->getPerson(); ?>
   
      <div class="container-fluid" id="appWrapper">
         <div class="container" id="pageContainer">
            <div class="row">
               <div class="col-md-3 col-sm-3 eqheight" id="appSidebar">
                  <div class="profileArea">
                     <?php
                     if(isset($this->session->userdata['logged_in']['logo']) && $this->session->userdata['logged_in']['logo']!="") { ?>
                        <img src="<?php echo base_url(); ?>logos/<?php echo $this->session->userdata['logged_in']['logo']; ?>" alt="" class="img-responsive" height="150px" width="120px">
                     <?php } else {  ?>
                     <img src="<?php echo base_url('uploads/default.png'); ?>" alt="" class="img-responsive" height="150px" width="120px" >
                     <?php } ?>
                     <p>Hello,
                        <?php 	echo $this->session->userdata['logged_in']['firstname']." ".$this->session->userdata['logged_in']['lastname'];  ?>
                     </p>
                     <p><?php echo $this->session->userdata['logged_in']['company']; ?></p>
                     <?php if (!isset($choose)): ?>
                     <a data-original-title="View My Profile" href="<?php echo base_url('usermanagement/view/' . $this->session->userdata['logged_in']['id']); ?>" class="profileBtn" data-toggle="tooltip" data-placement="top" title="">My Profile</a>
                     <?php endif; ?>
                     <a data-original-title="Logout from System" href="<?php echo base_url('index/logout'); ?>" class="logoutBtn" data-toggle="tooltip" data-placement="top" title="">Logout</a>
                  </div>
                  <!-- // profile -->
                  <div class="lightdivider"></div>
                  <?php if (!isset($choose)): ?>
                  <h3 class="hidden-xs">Menu</h3>
                  <?php endif; ?>
                  <div class="navbar navbar-default" role="navigation">
                     <div class="container-fluid">
                        <?php if (!isset($choose)): ?>
                        <div class="navbar-header">
                           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           </button>
                           <h3 class="visible-xs">Menu</h3>
                        </div>
                        <div class="navbar-collapse collapse">
                           <nav>
                              <ul class="nav navbar-nav">
                                 <?php if ($this->misc->check_if_root()): ?>
                                    <li><a href="<?php echo base_url('choose-account'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/dashboard.png'); ?>" alt="Logs"/>-- Root --</a></li>
                                 <?php endif; ?>
                                 <?php if ($this->misc->check_if_root()): ?>
                                    <li><a href="<?php echo base_url('logs'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/dashboard.png'); ?>" alt="Logs"/>-- Logs --</a></li>
                                 <?php endif; ?>
                                 <li><a href="<?php echo base_url('dashboard'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/dashboard.png'); ?>" alt="Dashboard">Dashboard</a></li>
                                 <li><a href="<?php echo base_url('locations'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/location.png'); ?>" alt="Locations">Locations</a></li>
                                 <?php if ($person): ?>
                                 <li><a href="<?php echo base_url('staff'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/user.png'); ?>" alt="<?php echo $person; ?>"><?php echo $person; ?></a></li>
                                 <?php endif; ?>
                                 <li><a href="<?php echo base_url('allsatisfied'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/satis.png'); ?>" alt="Very Satisfied">5 and 4 stars</a></li>
                                 <li><a href="<?php echo base_url('alldissatisfied'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/down.png'); ?>" alt="Dissatisfied">3 and under stars</a></li>
                                 <li><a href="<?php echo base_url('audio'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/audio.png'); ?>" alt="Audio Reviews">Audio Reviews</a></li>
                                 <li><a href="<?php echo base_url('video'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/video.png'); ?>" alt="Video Reviews">Video Reviews</a></li>
                                 <li><a href="<?php echo base_url('social'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/facebook.png'); ?>" width="16px" height="16px" alt="Social Shares">Social Shares</a></li>
                                 <li><a href="<?php echo base_url('reports'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/report.png'); ?>" alt="Reports">Reports</a></li>
                                 <li><a href="<?php echo base_url('schedules'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/schedule.png'); ?>" alt="Schedules">Schedule Reports</a></li>
                                 <li><a href="<?php echo base_url('usermanagement'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/user.png'); ?>" alt="Users">User Management</a></li>
                                 <li><a href="<?php echo base_url('settings'); ?>"><img src="<?php echo base_url('assets/admin/img/ico/settings.png'); ?>" alt="Settings">Settings</a></li>
                               </ul>
                           </nav>
                        </div>
                        <?php endif; ?>
                        <!--/.nav-collapse -->
                     </div>
                     <!--/.container-fluid -->
                  </div>
                  <?php if (!isset($choose)): ?>
                  <div class="divider"></div>
                  <h3>Statistics (All Time)</h3>
                  <nav>
                     <ul>
                        <li><a href="<?php echo base_url('extremly_satisfied'); ?>">
                           <img src="<?php echo base_url(); ?>assets/admin/img/ico/extremly.png" alt="Extremely Satisfied">Extremely Satisfied <span class="badge"><?php print $rating5; ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('very_satisfied'); ?>">
                           <img src="<?php echo base_url(); ?>assets/admin/img/ico/green.png" alt="Very Satisfied">Very Satisfied <span class="badge"><?php print $rating4; ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('satisfied'); ?>">
                           <img src="<?php echo base_url(); ?>assets/admin/img/ico/yellow.png" alt="Satisfied">Satisfied <span class="badge"><?php print $rating3; ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('dissatisfied'); ?>">
                           <img src="<?php echo base_url(); ?>assets/admin/img/ico/red.png" alt="Dissatisfied">Dissatisfied <span class="badge"><?php print $rating2; ?></span></a>
                        </li>
                        <li><a href="<?php echo base_url('very_dissatisfied'); ?>">
                           <img src="<?php echo base_url(); ?>assets/admin/img/ico/darkred.png" alt="Very Dissatisfied">Very Dissatisfied <span class="badge"><?php print $rating1; ?></span></a>
                        </li>
                     </ul>
                  </nav>
                  <div class="divider"></div>
                  <h3>Archives</h3>
                  <nav>
                     <ul>
                        <li><a href="<?php echo base_url('archive'); ?>">
                           <img src="<?php echo base_url(); ?>assets/admin/img/ico/extremly.png" alt="Archived Ratings"/>Archived Ratings </a>
                        </li>
                     </ul>
                  </nav>
                  <?php endif; ?>
               </div>
               <!-- // sidebar -->          <!-- // include sidebar with menu  -->
               <div class="col-md-9 col-sm-9 eqheight" id="appContent">
                  <div id="appHeader">
                     <div class="row">
                        <div class="col-md-8 col-sm-8 appHeaderLeft">
                            <?php if (!isset($choose)): ?>
                           <h1>System Dashboard</h1>
                           <?php else: ?>
                           <h1>Choose an Account to log into</h1>
                           <?php endif; ?>
                           <!--p><img src="<?php echo base_url(); ?>assets/admin/img/ico/profile.png" alt=""> Dr. Mooney &bull; <a href="#">Preview App</a>&bull;<a href="#">Visit Website</a></p-->
                           <p><img src="<?php echo base_url(); ?>assets/admin/img/ico/profile.png" alt=""> <?php echo $this->session->userdata['logged_in']['firstname'].' '.$this->session->userdata['logged_in']['lastname']; ?> | Date: <?php echo date('m/d/Y'); ?></p>
                        </div>
                        <?php if (!isset($choose)): ?>
                        <div class="col-md-4 col-sm-4">
                           <div class="row" id="mainLinks">
                              <a data-original-title="Dashboard" href="<?php echo base_url('dashboard'); ?>" class="col-md-4 col-sm-4 col-xs-4" data-toggle="tooltip" data-placement="top" title="">
                              <img src="<?php echo base_url(); ?>assets/admin/img/ico/dash.png" alt="Dashboard Icon" class="img-responsive">
                              </a>
                              <a data-original-title="Generate Report" href="<?php echo base_url('reports'); ?>" class="col-md-4 col-sm-4 col-xs-4" data-toggle="tooltip" data-placement="top" title="">
                              <img src="<?php echo base_url(); ?>assets/admin/img/ico/reportbig.png" alt="Repots Icon" class="img-responsive">
                              </a>
                              <a data-original-title="Help" href="<?php echo base_url('support'); ?>" class="col-md-4 col-sm-4 col-xs-4" data-toggle="tooltip" data-placement="top" title="">
                              <img src="<?php echo base_url(); ?>assets/admin/img/ico/help.png" alt="Help Icon" class="img-responsive">
                              </a>                      <!-- // include head with shortcuts  -->
                           </div>
                           <!-- // row -->
                        </div>
                        <?php endif; ?>
                     </div>
                     <!-- // row -->
                     <?php if(($this->session->flashdata('error'))):?>
                        <div class="alert alert-danger">
                            <?php echo $this->session->flashdata('error');?>
                        </div>
                    <?php endif;?>
                    
                    <?php if( $this->session->flashdata('success') ):?>
                        <div class="alert alert-success">
                            <?php echo $this->session->flashdata('success');?>
                        </div>
                    <?php endif;?>
                  </div>
                  <!-- // row header -->
                  <?php /* this will be content part -- start */ ?>
                  <?php echo $template['body']; ?>
                  <?php /* this will be content part -- end */ ?>
               </div>
               <!-- // content -->
            </div>
            <!-- // row -->
         </div>
         <!-- // container  -->        <!-- // import footer   -->
      </div>
      <!-- // app wrapper -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Remove Review</h4>
               </div>
               <div class="modal-body">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias eos ratione eveniet doloribus illum reiciendis quo atque fuga qui facilis voluptate modiecusandae. In, beatae, possimus? Officiis quo, unde. Blanditiis rerum, provident accusamus autem consectetur, quasi eligendi vel.</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-primary">Confirm</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <script>
      $(document).ready(function() {
            $('.eqheight').equalHeights();
      });
      </script>
   </body>
</html>