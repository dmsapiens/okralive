<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/mail.png" alt="">Support
         </div>
         <div class="panel-body" id="profileDetails">
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <?php if($this->session->flashdata('cls')) { ?>
            <div class="alert alert-<?php echo $this->session->flashdata('cls'); ?>"><?php echo $this->session->flashdata('msg'); ?></div>
            <?php } ?>
            <?php	echo form_open('support/insert'); ?>
            <div class="row">
               <div class="col-md-6 col-sm-6">
                  <h4>Full Name: <span class="requiredRed">*</span></h4>
                  <input type="text" data-validation="required" name="firstname" placeholder="Enter Your Full Name"  />
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Subject : <span class="requiredRed">*</span></h4>
                  <input type="text" data-validation="required" name="subject" placeholder="Subject" />
               </div>
               <div class="col-md-12 col-sm-12">
                  <h4>Message: <span class="requiredRed">*</span></h4>
                  <textarea name="message" data-validation="required" placeholder="Enter Your Message" /></textarea>
               </div>
               <div class="col-md-12">
                  <input type="submit" value="Send Comment" class="submitBtn">
               </div>
            </div>
            <!-- // row -->
         </div>
      </div>
      </form>
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/question.png" alt="">Why to use support?
         </div>
         <div class="panel-body" id="profileDetails">
            <p>If you have some questions for the developer or bug to report, please let us know.</p>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->
</div><!-- // content -->
</div><!-- // row -->
</div><!-- // container -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
   $.validate({
   	errorMessagePosition: $("<div>")
   });
</script>
<script src="<?php echo base_url('assets/admin/js/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/js/plugin.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/js/icheck.js'); ?>"></script>
<script>
   $('input,textarea').focus(function(){
      $(this).data('placeholder',$(this).attr('placeholder'))
      $(this).attr('placeholder','');
   });
   $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
   });
</script>