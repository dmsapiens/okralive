<?php if(validation_errors()) { ?>
<div class="alert alert-danger">
   <?php echo validation_errors(); ?>
</div>
<?php } ?>
<script type="text/javascript" src='//maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/js/map.js'); ?>"></script>
<?php	echo form_open_multipart('locations/insert_location'); ?>
<div class="panel panel-default">
   <div class="panel-heading"><?php echo (!isset($edit)) ? 'Add new Location' : 'Edit Location'; ?>
   </div>
   <div class="panel-body" id="profileDetails">
      <div class="row">
         <div class="col-md-6 col-sm-6">
            <h4>Find by address:</h4>
            <input type="text" name="search" id="search" value="<?php isset($userlist['search'])?print $userlist['search']:print set_value('search'); ?>" placeholder="Start Typing Address"/>         
            <h4>Street: <span class="requiredRed">*</span></h4>
            <input type="text" name="address1" id="address1" value="<?php isset($userlist['address1'])?print $userlist['address1']:print set_value('address1'); ?>" data-validation="required" />
            <!--h4>Street 2:</h4>
            <input type="text" disabled="true" name="address2" id="address2" value="<?php isset($userlist['address2'])?print $userlist['address2']:print set_value('address2'); ?>"/>
            -->
            <h4>Zip Code: <span class="requiredRed">*</span></h4>
            <input type="text" name="zipcode" id="zipcode" value="<?php isset($userlist['zipcode'])?print $userlist['zipcode']:print set_value('zipcode'); ?>" data-validation="required"/>
         </div>
         <div class="col-md-6 col-sm-6">
            <h4>Name: <span class="requiredRed">*</span></h4>
            <input type="text" name="name"  value="<?php isset($userlist['name'])?print $userlist['name']:print set_value('name'); ?>" placeholder="Location Name" data-validation="required" />
            
            <h4>City: <span class="requiredRed">*</span></h4>
            <input type="text" name="city" id="city" value="<?php isset($userlist['city'])?print $userlist['city']:print set_value('city'); ?>" data-validation="required" />
            <h4>State: <span class="requiredRed">*</span></h4>
            <input type="text" name="state" id="state" value="<?php isset($userlist['state'])?print $userlist['state']:print set_value('state'); ?>"  data-validation="required" />
          </div>
         </div>
         <div class="row">
            <div class="col-md-12 col-sm-12">
                <div id="map" style="width: 100% !important; height:250px !important;"></div>
                <div class="clear:both;"></div>
                <div>You can drag and drop the marker to the correct location</div>                                
            </div>
         </div>
      </div>
      <!-- // row -->
   </div>
   
   <!-- Text configuration -->
   <div class="panel panel-default">
       <div class="panel-heading">Texts Configuration for this location</div>
       <div class="panel-body" id="profileDetails">
          <div class="row">
               <div class="col-md-6 col-sm-6">
                  <h4>Extremly Satisfied Text: <span class="requiredRed">*</span></h4>
                  <textarea name="ex_satisfied_txt" data-validation="required"><?php isset($companydetail['ex_satisfied_txt']) ? print $companydetail['ex_satisfied_txt'] : print set_value('ex_satisfied_txt'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Very Satisfied Text: <span class="requiredRed">*</span></h4>
                  <textarea name="very_satisfied_text" data-validation="required"><?php isset($companydetail['very_satisfied_text']) ? print $companydetail['very_satisfied_text'] : print set_value('very_satisfied_text'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Satisfied Text: <span class="requiredRed">*</span></h4>
                  <textarea name="satisfied_text" data-validation="required"><?php isset($companydetail['satisfied_text']) ? print $companydetail['satisfied_text'] : print set_value('satisfied_text'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Dissatisfied Text: <span class="requiredRed">*</span></h4>
                  <textarea name="dissatisfied_text" data-validation="required"><?php isset($companydetail['dissatisfied_text']) ? print $companydetail['dissatisfied_text'] : print set_value('dissatisfied_text'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Very Dissatisfied Text: <span class="requiredRed">*</span></h4>
                  <textarea name="very_text" data-validation="required"><?php isset($companydetail['very_text']) ? print $companydetail['very_text'] : print set_value('very_text'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Thank you Text: <span class="requiredRed">*</span></h4>
                  <textarea name="thank_you" data-validation="required"><?php isset($companydetail['thank_you']) ? print $companydetail['thank_you'] : print set_value('thank_you'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Pre-defined text for audio review: <span class="requiredRed">*</span></h4>
                  <textarea name="voiceText" data-validation="required"><?php isset($companydetail['voiceText']) ? print $companydetail['voiceText'] : print set_value('voiceText'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Pre-defined text for video review: <span class="requiredRed">*</span></h4>
                  <textarea name="videoText" data-validation="required"><?php isset($companydetail['videoText']) ? print $companydetail['videoText'] : print set_value('videoText'); ?></textarea>
               </div>
               <div class="col-md-12">
                <p><strong>Short codes for audio/video review:</strong></p>
                <p>[name] for customer full name</p>
               </div>
            </div>
          </div>
       </div>
       
    <!-- Emails configuration -->
    <div class="panel panel-default">
       <div class="panel-heading">Emails Configuration for this location</div>
       <div class="panel-body" id="profileDetails">
        <!--div class="row">
           <div class="col-md-6 col-sm-6">
              <h4>Sender email: <span class="requiredRed">*</span></h4>
              <input type="text" placeholder="Sender's Email" name="senderEmail" value="<?php isset($companydetail['senderEmail'])?print $companydetail['senderEmail']:print set_value('senderEmail'); ?>" data-validation="required" />
              <p>Email address from which the emails will be sent to managers and customers.</p>
           </div>
        </div-->
        <div class="row">
            <div class="col-md-6 col-sm-6">
              <h4>Image in email for 4-5 stars:</h4>
              <?php if (isset($companydetail['email_img']) && $companydetail['email_img'] != ''): ?>
                <a href="<?php echo $companydetail['email_img1']; ?>" target="_blank"><img src="<?php echo $companydetail['email_img']; ?>" style="width:100%; margin-bottom:5px !important; margin-top:5px !important;" height="100"/></a>
              <?php endif; ?>
              <input type="file" name="email_image" id="email_image"> 
              <p>Best dimension: 600px x 156px</p>
           </div>
           <div class="col-md-6 col-sm-6">
              <h4>Image in email for 3 stars and under:</h4>
              <?php if (isset($companydetail['email_img1']) && $companydetail['email_img1'] != ''): ?>
                <a href="<?php echo $companydetail['email_img1']; ?>" target="_blank"><img src="<?php echo $companydetail['email_img1']; ?>" style="width:100%; margin-bottom:5px !important; margin-top:5px !important;" height="100"/></a>
              <?php endif; ?>
              <input type="file" name="email_image1" id="email_image1"> 
              <p>Best dimension: 600px x 156px</p>
           </div>
        </div>
           <div class="row">
               <div class="col-md-6 col-sm-6">
                  <h4>Email text for 4 and 5 star ratings: <span class="requiredRed">*</span></h4>
                  <textarea name="4_5_mail" data-validation="required"><?php isset($companydetail['4_5_mail']) ? print $companydetail['4_5_mail'] : print set_value('4_5_mail'); ?></textarea>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Email text for 3 stars and under ratings: <span class="requiredRed">*</span></h4>
                  <textarea name="3_under_mail" data-validation="required"><?php isset($companydetail['3_under_mail']) ? print $companydetail['3_under_mail'] : print set_value('3_under_mail'); ?></textarea>
               </div>
               <div class="col-md-12 col-sm-12">
                <p><strong>For email texts use these short codes:</strong></p>
                <p style="font-weight: bold;">%t for review text OR link to audio/video review | %s for customer first name</p>
                <p style="font-weight: bold;">%l for lastname | %r for number of stars | e for customer email | %p for customer phone</p>
               </div>
           </div>
           <div class="row" style="margin-top: 10px;">
            <div class="col-md-6 col-sm-6">
              <h4>E-mail addresses for 4-5 stars: <span class="requiredRed">*</span></h4>
              <textarea data-validation="required" name="emails45" placeholder="E-mails" /><?php if(isset($userlist['emails_45'])){print $userlist['emails_45'];} ?></textarea>   
              <p>Above persons will receive emails for reviews rated with 4 and 5 stars</p>
              <p>Note: use comma to separate between emails: user@mail.com,user1@mail.com ...</p>
           </div>
            <div class="col-md-6 col-sm-6">
              <h4>E-mail addresses for 3 and under stars: <span class="requiredRed">*</span></h4>
              <textarea data-validation="required" name="emails3" placeholder="E-mails" /><?php if(isset($userlist['emails_321'])){print $userlist['emails_321'];} ?></textarea>   
              <p>Above persons will receive emails for reviews rated with 3 and under stars</p>
              <p>Note: use comma to separate between emails: user@mail.com,user1@mail.com ...</p>
           </div>
         </div>
       </div>
    </div>
    
    <!-- Admin Email -->
    <div class="panel panel-default">
       <div class="panel-heading">Admin Email Notification</div>
       <div class="panel-body" id="profileDetails">
        <div class="row">
            <div class="col-md-12 col-sm-12">
              <h4>Email Text: <span class="requiredRed">*</span></h4>
              <textarea data-validation="required" name="admin_email" placeholder="Email Text"><?php isset($companydetail['admin_email']) ? print $companydetail['admin_email'] : print set_value('admin_email'); ?></textarea>   
              <p>This email will be sent to emails defined above for 4-5 stars as notification that someone has reviewed your company</p>
              <p><strong>Short codes:</strong></p>
              <p>%t for review text OR link to audio/video review</p>
              <p>%s for customer first name</p>
              <p>%l for lastname</p>
              <p>%r for number of stars</p>
              <p>%e for customer email</p>
              <p>%p for customer phone</p>
           </div>
        </div>
       </div>
    </div>
    
    <!-- Appointment Button -->
    <div class="panel panel-default">
       <div class="panel-heading">Appointment Settings</div>
       <div class="panel-body" id="profileDetails">
        <div class="row">
            <div class="col-md-12 col-sm-12">
              <h4>Enable Appointment Button:</h4>
              <select name="ap_enabled">
                <option value="1" <?php if ($companydetail['ap_enabled'] == 1) echo 'selected="selected"'; ?>>Yes</option>
                <option value="0" <?php if ($companydetail['ap_enabled'] == 0) echo 'selected="selected"'; ?>>No</option>
              </select>
           </div>
        </div>
           <div class="row">
               <div class="col-md-6 col-sm-6">
                  <h4>Button Text</h4>
                  <input name="ap_button" type="text" value="<?php isset($companydetail['ap_button']) ? print $companydetail['ap_button'] : print set_value('ap_button'); ?>"/>
               </div>
               <div class="col-md-6 col-sm-6">
                  <h4>Button URL</h4>
                  <input name="ap_url" type="text" value="<?php isset($companydetail['ap_url']) ? print $companydetail['ap_url'] : print set_value('ap_url'); ?>"/>
               </div>
           </div>
       </div>
    </div>
    
    <!-- Social Networks configuration -->
    <div class="panel panel-default">
       <div class="panel-heading">Social Networks Configuration for this location</div>
       <div class="panel-body" id="profileDetails">
        <div class="row">
            <?php if (isset($social)): ?>
            <?php foreach ($social as $r): ?>
            <div class="col-md-12 col-sm-12">
                <h2 style="margin-bottom: 25px; border-bottom:1px solid #ddd; color:#A0ACBF;"><?php echo $r['title']; ?></h2>
                <h4>Link: <span class="requiredRed">*</span></h4>
                <input type="text" data-validation="required" name="link[<?php echo $r['name']; ?>]" placeholder="Link" value="<?php if(isset($r['link'])){print $r['link'];} ?>"/>  
                <?php if ($r['name'] != 'fb' AND $r['name'] != 'yelp'): ?>
                <h4>Description: </h4>
                <textarea name="desc[<?php echo $r['name']; ?>]" placeholder="Description"><?php if(isset($r['description'])){print $r['description'];} ?></textarea>  
                <?php endif; ?>
                <?php if ($r['name'] == 'pinterest'): ?>
                <h4>Image:</h4>
                <input type="file" name="pinterest_image" id="pinterest_image"/><br />
                <?php if ($r['logo'] != ''): ?>
                    <img src="<?php echo $r['logo']; ?>" class="img-responsive"/>
                <?php endif; ?>
                <?php endif; ?>
                <h4>Is Active ?</h4>
                <select name="active[<?php echo $r['name']; ?>]">
                    <option value="1" <?php if ($r['active'] == 1) echo 'selected="selected"'; ?> >Yes</option>
                    <option value="0" <?php if ($r['active'] == 0) echo 'selected="selected"'; ?> >No</option>
                </select>
            </div>
            <?php endforeach; ?>
           <?php else: ?>
            <p>No social networks found</p>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <?php if (isset($edit)): ?>
                <input type="hidden" name="edit" value="<?php echo $userlist['id']; ?>"/>
            <?php endif; ?>
            <input type="hidden" id="lon" name="lon" value="<?php isset($userlist['longitude']) ? print $userlist['longitude'] : ''; ?>"/>
            <input type="hidden" id="lat" name="lat" value="<?php isset($userlist['latitude']) ? print $userlist['latitude'] : ''; ?>"/>            
            <button type="submit" class="saveBtn" name="submit" value="Save">Save</button>
            <button type="button" onclick="javascript:document.location='<?php echo site_url('locations'); ?> ' " class="saveBtn" name="cancel" value="cancel">Cancel</button>
         </div>
       </div>
    </div>
</form>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
   $.validate({
   	errorMessagePosition: $("<div>")
   });
   
   $(document).ready(function() {
        $('#zipcode').on('keypress',function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            } 
        });
        
        function updateFields(address) {
            $('#city').val(address.city);
            $('#state').val(address.stateOrProvince);
            $('#zipcode').val(address.postalCode);
            $('#address1').val(address.addressLine1);
        }
        
        $('#map').locationpicker({
            zoom:11,
            <?php if (isset($edit)): ?>
                <?php if ($userlist['latitude'] == ''): ?>
                    location: {latitude: 29.4241921, longitude: -98.5286477},
                <?php else: ?>
                    location: {latitude: <?php echo $userlist['latitude']; ?>, longitude: <?php echo $userlist['longitude']; ?>},
                <?php endif; ?>
            <?php else: ?>
            location: {latitude: 29.4241921, longitude: -98.5286477},
            <?php endif; ?>
            scrollwheel: true,
            radius: 300,
            enableAutocomplete: true,
            inputBinding: {
                locationNameInput: $('#search')
            },
            onchanged: function(currentLocation, radius, isMarkerDropped) {
                var address = $(this).locationpicker('map').location.addressComponents;
                updateFields(address);
                $('#lon').val(currentLocation.longitude);
                $('#lat').val(currentLocation.latitude);
            }
        });  
   });
</script>	