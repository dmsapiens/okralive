<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Welcome Visitor, Please Log In</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- basic Meta Tags -->
      <meta name="keywords" content="key">
      <meta name="description" content="description">
      <!-- // Favicon -->
      <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <?php echo link_tag('assets/admin/css/main.css'); ?>
      <?php echo link_tag('assets/admin/css/all.css'); ?>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   </head>
   <body>
      <div class="container-fluid" id="loginWrapper" style="min-height: 803px !important;">
         <div class="container">
            <div class="row">
               <div class="col-md-4 col-md-offset-2 col-sm-6 loginDate">
                  <small><?php echo date('l'); ?>, <strong><?php echo date('m/d/Y'); ?></strong></small>
               </div>
            </div>
            <!-- // row -->
            <div class="row">
               <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                  <div id="loginBox">
                     <div class="loginBrand">
                        <img src="<?php echo base_url('templates/site-logo.png'); ?>" alt="Client Logo" class="img-responsive">
                        <br/>
                        <h1>OQRA</h1>
                     </div>
                     <!-- // login brand -->
                     <?php if(validation_errors()) { ?>
                     <div class="alert alert-danger">
                        <?php echo validation_errors(); ?>
                     </div>
                     <?php } ?>
                     
                     <?php if( !empty($error) ):?>
                        <div class="alert alert-danger">
                            <?php echo $error;?>
                        </div>
                    <?php endif;?>
                    
                    <?php if( !empty($success) ):?>
                        <div class="alert alert-success">
                            <?php echo $success;?>
                        </div>
                    <?php endif;?>
                     
                     <?php echo form_open('index/login'); ?>
                     <div class="loginForm">
                        <div class="form-group">
                           <input type="text" name="email" class="userIco" data-validation="email" placeholder="Email"   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'"  data-validation="required"  />
                           <input type="password" name="password" class="passIco" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" data-validation="required" />
                        </div>
                        <div class="row">
                           <div class="col-md-7 col-sm-7">
                              <input type="submit"  value="LogIn">
                           </div>
                           <!--div class="col-md-5 col-sm-5 rememberArea">
                              <label class="checkbox-inline">
                                 <div class="demo-list">
                                    <input tabindex="1" type="checkbox" id="input-1"> <span>Remember Me</span>
                                 </div>
                              </label>
                           </div-->
                        </div>
                     </div>
                     </form><!-- // login form -->
                     <footer>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#forgotPassword">Forgot your password?</a>
                     </footer>
                  </div>
                  <!-- // logn box -->
               </div>
            </div>
            <!-- // row -->
         </div>
         <!-- // container -->
         <div class="row" id="loginFooter" style="margin-top: 50px !important; color:black !important;">
            <div class="container" style="color:black !important;">
               <small><span> &copy; <?php echo date('Y'); ?> Copyright by <?php echo APP_COMPANY; ?>.  All Rights Reserved. </span> &bull; Designed by:
               <a href="http://digitalmarketingsapiens.com/" target="_blank" data-toggle="tooltip" data-placement="top" title="Digital Marketing Sapiens"><img src="<?php echo base_url(); ?>assets/img/ico/dms.png" alt="Digital Media Sapiens"></a> </small>
            </div>
         </div>
      </div>
      <!-- // login wrapper -->
      
      <!-- Forgot Password -->
      <div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel1">Forgot your Password</h4>
               </div>
               <div class="modal-body">
                     <?php echo form_open('index/forgot'); ?>
                     <div class="loginForm">
                        <div class="form-group">
                           <input type="text" name="email1" class="userIco" data-validation="email" placeholder="Enter your email address" data-validation="required"  />
                        </div>
                        <div class="row">
                           <div class="col-md-7 col-sm-7">
                              <input type="submit" name="send" value="Send"/>
                           </div>
                        </div>
                     </div>
                     </form><!-- // login form -->
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
      <script>
         $.validate({
         	errorMessagePosition: $("<div>")
         });
      </script>
      <script src="<?php echo base_url('assets/admin/js/bootstrap.js'); ?>"></script>
      <script src="<?php echo base_url('assets/admin/js/plugin.js'); ?>"></script>
      <script src="<?php echo base_url('assets/admin/js/icheck.js'); ?>"></script>
      <script>
         $('input,textarea').focus(function(){
            $(this).data('placeholder',$(this).attr('placeholder'))
            $(this).attr('placeholder','');
         });
         $('input,textarea').blur(function(){
            $(this).attr('placeholder',$(this).data('placeholder'));
         });
          
      </script>
   </body>
</html>