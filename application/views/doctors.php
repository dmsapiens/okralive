<div id="sumInfo">
   <div class="sumBox">
      <span class="darkBlue"><?php print (isset($total['totalusers']) ? $total['totalusers'] : '0' ); ?></span>
      <h6><?php echo $this->misc->getPerson(); ?></h6>
   </div>
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
   
        <?php if(($this->session->flashdata('error'))):?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if( $this->session->flashdata('cls') ):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
   
      <div class="panel panel-default">
         <div class="panel-heading">
            <img src="<?php echo base_url(); ?>assets/admin/img/ico/userprofile.png" alt=""> <?php echo $this->misc->getPerson(); ?>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-hover">
               <?php if($userlist) {  	 ?>
               <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Email</th>
                  <th scope="col">Locations</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php	if(isset($userlist)) {
                  $i=1;
                  foreach($userlist as $user) {    ?>
               <tr>
                  <td><?php print $user['firstname'] . ' ' . $user['lastname']; ?> </td>
                  <td><?php print ($user['phone']) ? $user['phone'] : 'N/A'; ?></td>
                  <td><?php print ($user['email']) ? $user['email'] : 'N/A'; ?></td>
                  <td><?php echo $this->misc->getdocloc($user['location']); ?></td>
                  <td class="text-center">
                     <a href="<?php echo base_url(); ?>staff/view/<?php print $user['id']; ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="View Reviews">View Reviews</a>
                     <a href="<?php echo base_url(); ?>staff/edit/<?php print $user['id']; ?>" class="editBtn2" data-toggle="tooltip" data-placement="top" title="Edit">Edit</a>
                     <span  data-toggle="modal" data-target="#myModal">
                     <a href="javascript:void(0);" class="removeBtn" data-toggle="tooltip" data-placement="top" title="Remove" data-review-id='<?php print $user['id']; ?>' >Remove</a>
                     </span>
                  </td>
               </tr>
               <?php } } ?>
               <tr class="totalSumTable">
                  <td colspan="6" class="totalSumTable"><span><?php
                     if(isset($type))
                     {
                     	Switch($type)
                     			{
                     				case "admin":
                     					print "Total Admin : ".$total['admin'];
                     					break;
                     				case "regularuser":
                     					print "Total Regular User : ".$total['regularuser'];
                     					break;
                     				case "report":
                     					print "Total Report : ".$total['report'];
                     
                     			}
                     }
                     else {
                     print "Total : ".($total['totalusers']);
                     }
                     	 ?></span>   </td>
               </tr>
               <?php } else { ?>
               <tr>
               <tr>Sorry, No Records Found!</td></tr>
               <?php } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <!-- // col md 6 -->
                  <?php if ($this->misc->check_if_root()): ?>
                  <div class="col-md-7 col-sm-12" id="exportControls">
                     <a href="<?php echo base_url(); ?>staff/add" class="exportXLS">Add <?php echo $this->misc->getPerson(); ?></a>
                  </div>
                  <?php endif; ?>
               </div>
               <!-- // col md 6 -->
         </div>
         <!-- // row -->
         </footer>
      </div>
   </div>
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
               <h4 class="modal-title" id="myModalLabel">Remove</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure ?</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary"  id="delete-user-link">Confirm</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- // col md -->
</div>
<script>
   $(".removeBtn").on("click",function(){
   
   	$("#delete-user-link").attr("onclick", "deleteme("+$(this).data('review-id')+")");
   	// show the modal
   });
   
   function deleteme(rid)
   {
   
   	window.location.href='<?php echo base_url(); ?>/staff/remove/'+rid;
   
   }
</script>