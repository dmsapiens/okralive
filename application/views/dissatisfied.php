<div id="sumInfo">
    <div class="sumBox">
    	<a href="<?php echo base_url('dissatisfied'); ?>">
      <span class="pinkCircle"><?php isset($rating2)?$r2=$rating2:$r2=0; print $r2; ?></span>
      <h6>Dissatisfied</h6>
  </a>
    </div>
    <div class="clearfix"></div>
  </div>
<!-- // sum info -->
<?php echo form_open('dissatisfied',array('name' => 'filterform')); ?>
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <img src="<?php echo base_url(); ?>assets/admin/img/ico/not.png" alt=""/>Dissatisfied Reviews
            <select name="filterduration" id="filterduration" class="form-control" onchange="javascript: document.filterform.submit();">
               <option value="all">All</option>
               <option value="today">Today</option>
               <option value="7">Last Week</option>
               <option value="30">Last Month</option>
               <option value="90">Last 3 Months</option>
               <option value="180">Last 6 Months</option>
               <option value="365">Last Year</option>
            </select>
            <script>
               <?php if(isset($searchterm) && $searchterm != '') { ?>
               	$("#filterduration").val('<?php echo $searchterm; ?>');
               <?php } ?>
            </script>
         </div>
         <div class="panel-body">
           <?php if($dissatisfiedlist) { $i=1; ?>
           <table class="table table-striped table-hover">
              <tr>
                 <th scope="col">No</th>
                 <th scope="col">Full Name</th>
                 <th scope="col">Phone</th>
                 <th scope="col">Rating</th>
                 <th scope="col">Location</th>
                 <th scope="col" class="text-center">Action</th>
              </tr>
              <?php foreach($dissatisfiedlist as $dissatisfied) { ?>
              <tr>
                 <td><?php print $i; ?></td>
                 <td><?php print ucwords($dissatisfied['firstname']); ?> <?php print ucwords($dissatisfied['lastname']); ?></td>
                 <td><?php print $dissatisfied['phone_no']; ?></td>
                 <td><?php switch($dissatisfied['rating'])
                  {
                  	case '2': print "Dissatisfied";
                  	break;
                  }
                  ?></td>
                  <td>
                        <?php echo $dissatisfied['locationName']; ?>
                        <?php if ($this->misc->getPerson() != ''): ?>
                        <br />
                        (<?php echo $dissatisfied['dfname'] . ' ' . $dissatisfied['dflname']; ?>)
                        <?php endif; ?>
                     </td>
                 <td class="text-center">
                    <a href="<?php echo base_url('dashboard/review-details/' . $dissatisfied['rating_id']); ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="View Details">View</a>
                    <span  data-toggle="modal" data-target="#myModal">
                       <a href="javascript:void(0);" class="removeBtn" data-toggle="tooltip" data-placement="top" title="Archive" data-review-id='<?php print $dissatisfied['rating_id']; ?>' >Remove</a>
                    </span>
                    <?php if($dissatisfied['status']=='2') { ?>
                    <a href="javascript:void(0);" class="contactedBtn" data-toggle="tooltip" data-placement="top" title="Contacted">Contacted</a>
                    <?php } else { ?>
                    <a href="<?php echo base_url('dashboard/review-details/' . $dissatisfied['rating_id']); ?>" class="notcontactedBtn" data-toggle="tooltip" data-placement="top" title="Not Contacted">Not Contacted</a>
                    <?php } ?>
                 </td>
              </tr>
              <?php $i++; } ?>
           </table>
           <footer id="contentFooter">
              <div class="row">
                 <div class="col-md-5 col-sm-12">
                    <?php  if(isset($pagination)) { ?>
                    <?php	 echo $pagination;
                       } ?>
                 </div>
                 <!-- // col md 6 -->
                 <?php } else { echo "Sorry, No Records Found!"; } ?>
              </div>
              <!-- // row -->
           </footer>
           <?php if($dissatisfiedlist): ?>
               <hr/>
               <img src="<?php echo base_url(); ?>assets/admin/img/ico/contactedico.png" alt=""> - <small>Contacted</small> &nbsp;
               <img src="<?php echo base_url(); ?>assets/admin/img/ico/notcontactd.png" alt=""> - <small>Not Contacted</small>
           <?php endif; ?>
        </div>
      </div>
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Archive Review</h4>
               </div>
               <div class="modal-body">
                  <p>Are you sure you want to archive this review ?</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-primary" id="delete-user-link">Confirm</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->
</form>
<script>
   $(".removeBtn").on("click",function(){
   
       $("#delete-user-link").attr("onclick", "deleteme("+$(this).data('review-id')+")");
       // show the modal
   });
   
   function deleteme(rid) {
        window.location.href='<?php echo base_url(); ?>/dissatisfied/archivereview/'+rid;
   }
</script>