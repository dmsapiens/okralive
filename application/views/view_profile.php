<div id="sumInfo">
   <div class="row filterBox">
      <div class="col-md-6 col-sm-6" id="profileUser">
         <h5><?php print $userlist['firstname'].' '.$userlist['lastname']?></h5>
         <h3><?php Switch($userlist['usertype'])
            {
            	case "admin":
            		print "Admin";
            		break;
            	case "regularuser":
            		print "Regular User";
            		break;
            	case "report":
            		print "Report";
            		break;
            } ?></h3>
      </div>
      <div class="col-md-3 col-md-offset-3 col-sm-6">
         <a href="<?php echo base_url(); ?>index.php/usermanagement/edit/<?php print $userlist['id']; ?>" class="editProfleBtn">Edit Profile</a>
      </div>
   </div>
   <!-- // row -->
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/userprofile.png" alt="">Profile Details
         </div>
         <div class="panel-body" id="profileDetails">
            <div class="row">
               <div class="col-md-4 col-sm-4">
                  <h4>First Name:</h4>
                  <p><?php print $userlist['firstname'] ; ?></p>
                  <h4>Last Name:</h4>
                  <p><?php print $userlist['lastname'] ; ?></p>
                  <h4>Phone:</h4>
                  <p><?php print $userlist['phoneno'] ; ?></p>
               </div>
               <div class="col-md-5 col-sm-4">
                  <h4>Email:</h4>
                  <p><a href="#"><?php print $userlist['email'] ; ?></a></p>
               </div>
            </div>
            <!-- // row -->
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->