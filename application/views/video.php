<?php
require("vimeo/v/autoload.php");

$lib = new \Vimeo\Vimeo('82d4c6b32df86b84cac7da62a4582661775a80a1', 'ZWGaQ57fb3tq5CO4oVVFsc5HZDZcUS8LhkNIUo1PbKtxulo2jSxBRaCiySbsoQ6+/es2VSFh8U/1338eND1GGuEqv6p17BdlJLSOMbKHBtzRA95/CS5H8UQUC5qaekXo');
$lib->setToken('ac8dece3c70b877daffb254d34a17f2f');
?>
<div id="sumInfo">
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/video.png" alt="">Video Reviews
         </div>
         <div class="panel-body">
            <?php if($archivedratinglist) {  $i=$starting_no+1; ?>
            <table class="table table-striped table-hover">
               <tr>
                  <th scope="col">No</th>
                  <th scope="col">Full Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Rating</th>
                  <th scope="col">Location</th>
                  <th scope="col">Status</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php foreach($archivedratinglist as $archivedrating) {  ?>
               <?php $file = basename($archivedrating['file']); ?>
               <tr>
                  <td scope="col"><?php print $i; ?></td>
                  <td scope="col"><?php print ucwords($archivedrating['firstname'].' '.$archivedrating['lastname']); ?></td>
                  <td scope="col"><?php print $archivedrating['phone_no']; ?></td>
                  <td scope="col">
                  <?php switch($archivedrating['rating'])
                    {
                    	case '5': print "Extremely Satisfied";
                    	break;
                    	case '4': print "Very Satisfied";
                    	break;
                    	case '3': print "Satisfied";
                    	break;
                    	case '2': print "Dissatisfied";
                        break;
                        case '1': print "Very Dissatisfied";
                    }
                 ?>
                 </td>
                 <td scope="col"><?php print $archivedrating['locationName']; ?></td>
                 <td>
                 <?php $response = $lib->request('/me/videos/' . $archivedrating['videoId'], array('per_page' => 1), 'GET'); ?>
                 <?php if (isset($response['body']['status']) && $response['body']['status'] == 'available'): ?>
                 <span class="label label-success">Video is ready</span>
                 <?php else: ?>
                 <span class="label label-warning">Video is processing</span>
                 <?php endif; ?>
                 </td>
                  <td scope="col" class="text-center">
                     <a data-original-title="View Details" href="<?php echo base_url(); ?>dashboard/review-details/<?php echo $archivedrating['id']; ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="">View</a>
                     <a href="<?php echo base_url('/video/archivereview/' . $archivedrating['id']); ?>" class="removeBtn" data-toggle="tooltip" data-placement="top" title="Archive" onclick="return confirm('Are you sure?');" data-review-id='<?php print $archivedrating['rating_id']; ?>'>Remove</a>
                  </td>
               </tr>
               <?php $i++; } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <?php } else { echo "Sorry, No Records Found!"; } ?>
               </div>
               <!-- // row -->
            </footer>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>

<script>
   $(".removeBtn").on("click",function(){
   
       $("#delete-user-link").attr("onclick", "deleteme("+$(this).data('review-id')+")");
       // show the modal
   });
   
   function deleteme(rid) {
        window.location.href='<?php echo base_url(); ?>/video/archivereview/'+rid;
   }
</script>