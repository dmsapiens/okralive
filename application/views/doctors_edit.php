<?php if(validation_errors()) { ?>
<div class="alert alert-danger">
   <?php echo validation_errors(); ?>
</div>
<?php } ?>
<div class="panel panel-default">
   <div class="panel-heading"><?php echo $addTxt; ?>
   </div>
   <div class="panel-body" id="profileDetails">
      <?php	echo form_open_multipart('staff/save'); ?>
      <div class="row">
         <div class="col-md-4 col-sm-4">
            <h4>First Name: <span class="requiredRed">*</span></h4>
            <input type="text" name="firstname"  value="<?php isset($userlist['firstname'])?print $userlist['firstname']:print set_value('firstname'); ?>" placeholder="Enter First Name(required)" required />
            <h4>Last Name: <span class="requiredRed">*</span></h4>
            <input type="text" name="lastname"  value="<?php isset($userlist['lastname'])?print $userlist['lastname']:print set_value('lastname'); ?>" placeholder="Enter Last Name(required)" required />
           </div>
         <div class="col-md-4 col-sm-4">
            <h4>Email: </h4>
            <input type="text" name="email"  value="<?php isset($userlist['email'])?print $userlist['email']:print set_value('email'); ?>" placeholder="Enter Email"/>
             <h4>Phone: </h4>
            <input type="text" name="phone" value="<?php isset($userlist['phone'])?print $userlist['phone']:print set_value('phone'); ?>" placeholder="Enter Phone Number" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" />
         </div>
         <div class="col-md-4 col-sm-4">
             <h4>Address: </h4>
            <input type="text" name="address" value="<?php isset($userlist['address'])?print $userlist['address']:print set_value('address'); ?>"/>
            <input type="hidden" name="degree" value="<?php isset($userlist['degree'])?print $userlist['degree']:print set_value('degree'); ?>"/>
            </div>
         <div class="col-md-12">
         <h4>Location: <span class="requiredRed">*</span></h4>
            <select name="usertype[]" id="usertype" multiple="true" >
               <?php foreach ($locations as $k => $v): ?>
                <?php $t = explode(',',$userlist['location']); ?>
                <?php $sel = in_array($v['id'],$t) ? 'selected="true"' : ''; ?>
                <option <?php echo $sel; ?> value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
               <?php endforeach; ?>
            </select>
         </div>
            <div class="col-md-12 col-sm-12" style="margin-bottom:10px !important;">
              <h4>Override default image:</h4>
              <input type="file" name="image" id="image" value="" onchange="loadFile(event)"/>
           </div>
           <div class="col-md-12 col-sm-12">
              <h4>Image Preview:</h4>
              <div id="imgPrev">
                    <img id="image_preview" <?php if (isset($userlist['image']) && $userlist['image'] != '') echo 'width="300" height="300" src="'.base_url('uploads/' . $userlist['image']).'"'; ?>/>
              </div>
              <input type="hidden" id="x" name='x' value=''/>
              <input type="hidden" id="y" name='y' value=''/>
              <input type="hidden" id="w" name='w' value=''/>
              <input type="hidden" id="h" name='h' value=''/>
           </div>
         <div class="col-md-12">
            <input type="hidden" name="todo" value="<?php print $todo; ?>">      
            <input type="hidden" id="cid" name="cid" value="<?php isset($userlist['id']) ? print $userlist['id'] : print "" ?>">
            <button type="submit" class="saveBtn" name="submit" value="Insert">Save Changes</button>
            <button type="button" onclick="javascript:document.location='<?php echo site_url('staff'); ?> ' " class="saveBtn" name="cancel" value="cancel">Cancel</button>
         </div>
      </div>
      <!-- // row -->
      </form>
   </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
   $.validate({
   	errorMessagePosition: $("<div>")
   });
   
   
   
</script>	
<script language="javascript">


    var loadFile = function(event) {
        var reader = new FileReader();
        var output = document.getElementById('image_preview');
    
        reader.onload = function(){            
            output.src = reader.result;
            $('.jcrop-holder img').attr('src', reader.result);
            $('#image_preview').removeAttr('width');
            $('#image_preview').removeAttr('height');
            $("#image_preview").Jcrop({
                aspectRatio: 1,
                onSelect: storeCoords,
                boxWidth: 300,
                boxHeight: 300
            });
        };  
        if(event.target.files[0] !='undefined' && event.target.files[0] != null) {
            reader.readAsDataURL(event.target.files[0]);
        } else {        
            $("#image_preview").attr('src','').data('Jcrop').destroy();
        }
    };
    
    function storeCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };

   var zChar = new Array(' ', '(', ')', '-', '.');
   var maxphonelength = 13;
   var phonevalue1;
   var phonevalue2;
   var cursorposition;
   
   function ParseForNumber1(object){
   	phonevalue1 = ParseChar(object.value, zChar);
   }
   
   function ParseForNumber2(object){
   	phonevalue2 = ParseChar(object.value, zChar);
   }
   
   function backspacerUP(object,e) {
   
   	if(e){
   		e = e
   	} else {
   		e = window.event
   	}
   
   	if(e.which){
   		var keycode = e.which
   	} else {
   		var keycode = e.keyCode
   	}
   
   	ParseForNumber1(object)
   
   	if(keycode >= 48){
   		ValidatePhone(object)
   	}
   }
   
   function backspacerDOWN(object,e) {
   	if(e){
   		e = e
   	} else {
   		e = window.event
   	}
   
   	if(e.which){
   		var keycode = e.which
   	} else {
   		var keycode = e.keyCode
   	}
   
   	ParseForNumber2(object)
   }
   
   function GetCursorPosition(){
   
   	var t1 = phonevalue1;
   	var t2 = phonevalue2;
   	var bool = false
   	for (i=0; i<t1.length; i++)
   	{
   		if (t1.substring(i,1) != t2.substring(i,1)) {
   			if(!bool) {
   				cursorposition=i
   				bool=true
   			}
   		}
   	}
   }
   
   function ValidatePhone(object){
   
   	var p = phonevalue1
   
   	p = p.replace(/[^\d]*/gi,"")
   
   	if (p.length < 3) {
   		object.value=p
   	} else if(p.length==3){
   		pp=p;
   		d4=p.indexOf('(')
   		d5=p.indexOf(')')
   		if(d4==-1){
   			pp="("+pp;
   		}
   		if(d5==-1){
   			pp=pp+")";
   		}
   		object.value = pp;
   	} else if(p.length>3 && p.length < 7){
   		p ="(" + p;
   		l30=p.length;
   		p30=p.substring(0,4);
   		p30=p30+")"
   
   		p31=p.substring(4,l30);
   		pp=p30+p31;
   
   		object.value = pp;
   
   	} else if(p.length >= 7){
   		p ="(" + p;
   		l30=p.length;
   		p30=p.substring(0,4);
   		p30=p30+")"
   
   		p31=p.substring(4,l30);
   		pp=p30+p31;
   
   		l40 = pp.length;
   		p40 = pp.substring(0,8);
   		p40 = p40 + "-"
   
   		p41 = pp.substring(8,l40);
   		ppp = p40 + p41;
   
   		object.value = ppp.substring(0, maxphonelength);
   	}
   
   	GetCursorPosition()
   
   	if(cursorposition >= 0){
   		if (cursorposition == 0) {
   		cursorposition = 2
   		} else if (cursorposition <= 2) {
   			cursorposition = cursorposition + 1
   		} else if (cursorposition <= 5) {
   			cursorposition = cursorposition + 2
   		} else if (cursorposition == 6) {
   			cursorposition = cursorposition + 2
   		} else if (cursorposition == 7) {
   			cursorposition = cursorposition + 4
   			e1=object.value.indexOf(')')
   			e2=object.value.indexOf('-')
   			if (e1>-1 && e2>-1){
   				if (e2-e1 == 4) {
   					cursorposition = cursorposition - 1
   				}
   			}
   		} else if (cursorposition < 11) {
   			cursorposition = cursorposition + 3
   		} else if (cursorposition == 11) {
   			cursorposition = cursorposition + 1
   		} else if (cursorposition >= 12) {
   			cursorposition = cursorposition
   		}
   
   		var txtRange = object.createTextRange();
   		txtRange.moveStart( "character", cursorposition);
   		txtRange.moveEnd( "character", cursorposition - object.value.length);
   		txtRange.select();
   	}
   
   }
   
   function ParseChar(sStr, sChar)
   {
   	if (sChar.length == null)
   	{
   		zChar = new Array(sChar);
   	}
   	else zChar = sChar;
   
   	for (i=0; i<zChar.length; i++)
   	{
   		sNewStr = "";
   
   		var iStart = 0;
   		var iEnd = sStr.indexOf(sChar[i]);
   
   		while (iEnd != -1)
   		{
   			sNewStr += sStr.substring(iStart, iEnd);
   			iStart = iEnd + 1;
   			iEnd = sStr.indexOf(sChar[i], iStart);
   		}
   		sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);
   
   		sStr = sNewStr;
   	}
   
   	return sNewStr;
   }
</script>
<script src="<?php echo base_url('assets/js/jquery-ui.js'); ?>"></script>
<script>
   $('input,textarea').focus(function(){
      $(this).data('placeholder',$(this).attr('placeholder'))
      $(this).attr('placeholder','');
   });
   $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
   });
    
</script>