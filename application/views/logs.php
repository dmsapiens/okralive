<div id="sumInfo">
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">Logs
         </div>
         <div class="panel-body">
            <?php if($archivedratinglist) {  $i=$starting_no+1; ?>
            <table class="table table-striped table-hover">
               <tr>
                  <th scope="col">Email</th>
                  <th scope="col">Date</th>
                  <th scope="col">Ip Address</th>
                  <th scope="col">City</th>
                  <th scope="col">State</th>
                  <th scope="col">Country</th>
                  <th scope="col" class="text-center">Details</th>
               </tr>
               <?php foreach($archivedratinglist as $archivedrating) {  ?>
               <?php $username = $this->db->query("SELECT email FROM sr_admincontrol WHERE id = ?",array($archivedrating['user_id'])); ?>
               <?php $username = (isset($username->row()->email)) ? $username->row()->email : 'Removed User'; ?>
               <?php #if ($username == 'root@dmsapiens.com') continue; ?>
               <tr>
                  <td scope="col"><?php print $username; ?></td>
                  <td scope="col"><?php print date('m/d/Y h:i:A',$archivedrating['date']); ?></td>
                  <td scope="col"><a target="_blank" href="https://who.is/whois-ip/ip-address/<?php print $archivedrating['ip']; ?>"><?php print $archivedrating['ip']; ?></a></td>
                  <td scope="col"><?php print ($archivedrating['city']=='') ? 'N/A' : $archivedrating['city']; ?></td>
                  <td scope="col"><?php print ($archivedrating['state']=='') ? 'N/A' : $archivedrating['state']; ?></td>
                  <td scope="col"><?php print ($archivedrating['country']=='') ? 'N/A' : $archivedrating['country']; ?></td>
                  <td scope="col" class="text-center"><a href="<?php echo base_url('logs/view/'.$archivedrating['sessionid']); ?>">View</a></td>
               </tr>
               <?php $i++; } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <?php } else { echo "Sorry, No Records Found!"; } ?>
               </div>
               <!-- // row -->
            </footer>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>