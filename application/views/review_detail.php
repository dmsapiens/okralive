<?php
require("vimeo/v/autoload.php");

$lib = new \Vimeo\Vimeo('82d4c6b32df86b84cac7da62a4582661775a80a1', 'ZWGaQ57fb3tq5CO4oVVFsc5HZDZcUS8LhkNIUo1PbKtxulo2jSxBRaCiySbsoQ6+/es2VSFh8U/1338eND1GGuEqv6p17BdlJLSOMbKHBtzRA95/CS5H8UQUC5qaekXo');
$lib->setToken('ac8dece3c70b877daffb254d34a17f2f');
?>
<div id="sumInfo">
   <div class="row filterBox">
      <div class="col-md-6 col-sm-6" id="profileUser">
         <h5><?php print $user; ?></h5>
         <h3>Date of Review: <strong><?php print $entrydate; ?></strong></h3>
      </div>
      <div class="col-md-3 col-md-offset-3 col-sm-6">
      </div>
   </div>
   <!-- // row -->
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url('assets/admin/img/ico/userprofile.png'); ?>" alt="">Visitor Details
         </div>
         <?php if(isset($userdetails)) { ?>
         <div class="panel-body" id="profileDetails">
            <div class="row">
               <?php foreach($userdetails as $details) { ?>
               <div class="col-md-3 col-sm-4">
                  <h4>First Name:</h4>
                  <p><?php print ucwords($details['firstname']); ?></p>
                  <h4>Last Name:</h4>
                  <p><?php print ucwords($details['lastname']); ?></p>
                  <h4>Phone:</h4>
                  <p><?php print $details['phone_no']; ?></p>
               </div>
               <div class="col-md-4 col-sm-4">
                  <h4>Email:</h4>
                  <p><a href="#"><?php print $details['emailid']; ?></a></p>
                  <h4>Visitor Satisfaction</h4>
                  <?php if($details['rating']=='5') { ?>
                  <div class="ratingBar">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <small><?php print "Extremely Satisfied"; ?></small>
                  </div>
                  <?php } ?>	
                  <?php if($details['rating']=='4') { ?>
                  <div class="ratingBar">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <small><?php print "Very Satisfied "; ?></small>
                  </div>
                  <?php } ?>
                  <?php if($details['rating']=='3') { ?>
                  <div class="ratingBar">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <small><?php print "Satisfied"; ?></small>
                  </div>
                  <?php } ?>
                  <?php if($details['rating']=='2') { ?>
                  <div class="ratingBar">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <small><?php print "Dissatisfied"; ?></small>
                  </div>
                  <?php } ?>
                  <?php if($details['rating']=='1') { ?>
                  <div class="ratingBar">
                     <img src="<?php echo base_url(); ?>assets/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <img src="<?php echo base_url(); ?>assets/admin/img/ico/star.png" alt="">
                     <small><?php print "Very Dissatisfied"; ?></small>
                  </div>
                  <?php } ?>
               </div>
               <?php if($details['title']!=null) { ?>
               <div class="col-md-5 col-sm-3">
                  <h4>Title:</h4>
                  <p><?php print $details['title']; ?></p>
               </div>
               <?php } ?>
               </div>
               <div class="row">
               <div class="col-md-12 col-sm-3">
                  <h4>Review:</h4>
                  <?php if ($details['review'] == 'Upload'): ?>
                  <?php if (isset($details['file'])): ?>
                  <?php
                     $a = explode('/',$details['file']);
                     $file = end($a);
                     $c = $file;
                     $ext = explode('.',$file);
                     if ($ext[1] == 'MOV' || $ext[1] == 'mp4') {

                        $response = $lib->request('/me/videos/' . $details['videoId'], array('per_page' => 1), 'GET'); 
                        if (isset($response['body']['status']) && $response['body']['status'] == 'available') {  
                     ?>
                  <div class="videos">
                        <?php
                        $ctime = $response['body']['created_time'];
                        $image = $response['body']['pictures']['sizes'][1]['link'];
                        $id = $details['videoId'];
                        $this->db->query("UPDATE sr_reviews SET thumb = '$image', ctime = '$ctime' WHERE videoId = $id");
                        ?>
                        <?php echo $response['body']['embed']['html']; ?>
                  </div>
                     <?php
                     } else {
                         echo '<p>Processing video review</p>';
                     }
                     } else {
                     if (file_exists('/home1/oneqrepu/public_html/mainadmin/done/' . $file . '.mp3')) {
                         ?>
                  <audio style="margin-top: 10px !important;" class="audio" id="audio-player" src="//1qreputation.com/mainadmin/done/<?php echo $file; ?>.mp3" type="audio/mp3" controls="controls"></audio>
                  <?php
                     } else if ($this->misc->remoteFileExists('http://mobilecommz.com/okra/done/' . $file . '.mp3')) { ?>
                        <audio style="margin-top: 10px !important;" class="audio" id="audio-player" src="//mobilecommz.com/okra/done/<?php echo $file; ?>.mp3" type="audio/mp3" controls="controls"></audio>
                     
                     <?php
                     }else {
                         echo '<p>Processing audio review</p>';
                     }
                     }
                     ?>
                  <?php endif; ?>
                  <?php else: ?>
                  <p><?php print $details['review']; ?></p>
                  <?php endif; ?>   
                  <?php if($details['rating']=='5' || $details['rating']=='4' || $details['rating']=='3') { ?>
                  <?php if (isset($fb) || isset($yelp) || isset($yp) || isset($googl)): ?>
                  <h4>Shared on: </h4>
                  <div class="socialShare">
                     <?php if(isset($fb)) { ?>
                     <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><img src="<?php echo base_url(); ?>assets/admin/img/ico/facebookbig.png" alt=""></a>
                     <?php }
                        if(isset($yelp)) {
                        ?>
                     <a href="#" data-toggle="tooltip" data-placement="top" title="Yelp"><img src="<?php echo base_url(); ?>assets/admin/img/ico/yelpbig.png" alt=""></a>
                     <?php } 
                        if(isset($yp)) { ?>
                     <a href="#" data-toggle="tooltip" data-placement="top" title="Yellow Pages"><img src="<?php echo base_url(); ?>assets/admin/img/ico/yellowpages.png" alt=""></a>
                     <?php } 
                        if(isset($googl)) { ?>
                     <a href="#" data-toggle="tooltip" data-placement="top" title="Google"><img src="<?php echo base_url(); ?>assets/admin/img/ico/google.jpg" alt=""></a>
                     <?php } ?>
                  </div>
                  <?php endif; ?>
                  <?php } ?>
               </div>
               </div>
               <div class="row">
               <div class="col-md-12">
                  <?php if($details['rating']=='1' || $details['rating']=='2' || $details['rating'] == '3') { if($details['status']==0) { ?>
                  <a href="javascript:void(0);" class="saveBtn" onclick="markAsContacted()">Mark As Contacted</a>
                  <?php } else print "Contacted"; } ?>
                  <?php if($details['rating']=='1' ) { ?>
                  <input type="hidden" name="page" value="very_dissatisfied" id="page">
                  <?php } 
                     if($details['rating']=='2') { ?>
                  <input type="hidden" name="page" value="dissatisfied" id="page">
                  <?php }
                     if($details['rating']=='4') { ?>
                  <input type="hidden" name="page" value="very_satisfied" id="page">
                  <?php }
                     if($details['rating']=='5') { ?>
                  <input type="hidden" name="page" value="extremly_satisfied" id="page">
                  <?php }
                     else { ?>
                  <input type="hidden" name="page" value="satisfied" id="page">
                  <?php } ?>
               </div>
            </div>
            <!-- // row -->
            <input type="hidden" name="rid" id="rid" value="<?php echo $rating_id; ?>">
         </div>
         <?php } ?>
         <?php } ?>
      </div>
   </div>
   <!-- // col md -->
</div>
<script src="//vjs.zencdn.net/5.5.3/video.js"></script>
<!-- // content wrapper -->

<script>
   function markAsContacted() {
        var ask = confirm('Are you sure?');
        if (ask) {
            var rid=document.getElementById('rid').value;
   		    window.location.href='<?php echo base_url('dashboard/markedAsContacted'); ?>/'+rid;
        }
   	}
</script>