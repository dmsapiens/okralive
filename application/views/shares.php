<div id="sumInfo">
   <div class="sumBox">
      <a href="<?php echo base_url('social'); ?>">
       <span class="lightgreenCircle"><?php print $total_satisfied; ?></span>
       <h6>Total Shares</h6>
       </a>
      </div>
      <div class="clearfix"></div>
</div>
<!-- // sum info -->
<?php echo form_open('social',array('name' => 'filterform')); ?>
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <img src="<?php echo base_url(); ?>assets/admin/img/ico/sat.png" alt="">Social Shares
            <select name="filterduration" id="filterduration" class="form-control" onchange="javascript: document.filterform.submit();">
               <option value="all">All</option>
               <option value="today">Today</option>
               <option value="7">Last Week</option>
               <option value="30">Last Month</option>
               <option value="90">Last 3 Months</option>
               <option value="180">Last 6 Months</option>
               <option value="365">Last Year</option>
            </select>
            <script>
               <?php if(isset($searchterm) && $searchterm != '') { ?>
               	$("#filterduration").val('<?php echo $searchterm; ?>');
               <?php } ?>
            </script>
         </div>
         <div class="panel-body">
            <?php if($satisfiedlist) { $i=1; ; ?>
            <table class="table table-striped table-hover">
               <tr>
                  <th scope="col">No</th>
                  <th scope="col">Full Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Rating</th>
                  <th scope="col">Location</th>
                  <th scope="col">Network</th>
               </tr>
               <?php foreach($satisfiedlist as $satisfied) {  ?>
               <tr>
                  <td scope="col"><?php print $i; ?></td>
                  <td scope="col"><?php print ucwords($satisfied['firstname'].' '.$satisfied['lastname']); ?></td>
                  <td scope="col"><?php print $satisfied['phone_no']; ?></td>
                  <td>
                  <?php switch($satisfied['rating'])
                    {
                    	case '5': print "Extremely Satisfied";
                    	break;
                    	case '4': print "Very Satisfied";
                    	break;
                    	case '3': print "Satisfied";
                        break;
                        case '2': print "Dissatisfied";
                      	break;
                      	case '1': print "Very Dissatisfied";
                      	break;
                    }
                 ?>
                 </td>
                  <td scope="col"><?php print $satisfied['locationName']; ?></td>
                  <td scope="col"><?php echo ucwords($satisfied['social_type']); ?></td>
               </tr>
               <?php $i++; } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <!-- // col md 6 -->
                  <?php } else { echo "Sorry, No Records Found!"; } ?>
               </div>
               <!-- // row -->
            </footer>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->
</form>