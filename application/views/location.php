<div id="sumInfo">
   <div class="sumBox">
      <span class="darkBlue"><?php print (isset($total) ? $total : '0' ); ?></span>
      <h6><?php if ($total == 1) echo 'Location'; else echo 'Locations'; ?></h6>
   </div>
   <div class="clearfix"></div>
</div>
<!-- // sum info -->

<div class="contentWrapper row">
   <div class="col-md-12">
        
        <?php if( $this->session->flashdata('msg') ):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
   
      <div class="panel panel-default">
         <div class="panel-body">
            <table class="table table-striped table-hover">
               <?php if($locations) { ?>
               <tr>
                  <th scope="col">Location Name</th>
                  <th scope="col">Address</th>
                  <th scope="col">City</th>
                  <th scope="col">Zip Code</th>
                  <th scope="col">State</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php if(isset($locations)) {
                  $i=1;
                  foreach($locations as $location) {    ?>
               <tr>
                  <td><?php print $location['name']; ?> </td>
                  <td><?php print $location['address1']; ?> </td>
                  <td><?php print $location['city']; ?> </td>
                  <td><?php print $location['zipcode']; ?> </td>
                  <td><?php print $location['state']; ?> </td>
                  <td class="text-center">
                     <a href="<?php echo base_url(); ?>locations/edit/<?php print $location['id']; ?>" class="editBtn2" data-toggle="tooltip" data-placement="top" title="Edit">Edit</a>
                     <?php if ($total > 1): ?>
                        <a onclick="return confirm('Are you sure?');" href="<?php echo base_url(); ?>locations/delete/<?php print $location['id']; ?>" class="removeBtn" data-toggle="tooltip" data-placement="top" title="Remove Location" data-review-id='<?php print $location['id']; ?>' >Remove</a>
                     <?php endif; ?>
                  </td>
               </tr>
               <?php } } ?>
               <?php } else { ?>
               <tr>
               <td>Sorry, No Records Found!</td></tr>
               <?php } ?>
            </table>
            <footer id="contentFooter">
                <?php if ($this->misc->check_if_root()): ?>
               <div class="row">
                  <!-- // col md 6 -->
                  <div class="col-md-7 col-sm-12" id="exportControls">
                     <a href="<?php echo base_url(); ?>locations/add" class="exportXLS">Add New Location</a>
                  </div>
               </div>
               <?php endif; ?>
               <!-- // col md 6 -->
         </div>
         <!-- // row -->
         </footer>
      </div>
   </div>
</div>
<!-- // col md -->
</div>