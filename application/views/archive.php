<div id="sumInfo">
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading"><img src="<?php echo base_url(); ?>assets/admin/img/ico/sat.png" alt="">Archived Ratings
         </div>
         <div class="panel-body">
            <?php if($archivedratinglist) {  $i=$starting_no+1; ?>
            <table class="table table-striped table-hover">
               <tr>
                  <th scope="col">No</th>
                  <th scope="col">Full Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Location</th>
                  <th scope="col">Rating</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php foreach($archivedratinglist as $archivedrating) {  ?>
               <tr>
                  <td scope="col"><?php print $i; ?></td>
                  <td scope="col"><?php print ucwords($archivedrating['firstname'].' '.$archivedrating['lastname']); ?></td>
                  <td scope="col"><?php print $archivedrating['phone_no']; ?></td>
                  <td scope="col"><?php print $archivedrating['locationName']; ?></td>
                  <td scope="col"><?php print $archivedrating['rating']; ?></td>
                  <td scope="col" class="text-center">
                     <a data-original-title="View Details" href="<?php echo base_url(); ?>dashboard/review-details/<?php echo $archivedrating['rating_id']; ?>" class="viewBtn" data-toggle="tooltip" data-placement="top" title="">View</a>
                  </td>
               </tr>
               <?php $i++; } ?>
            </table>
            <footer id="contentFooter">
               <div class="row">
                  <div class="col-md-5 col-sm-12">
                     <?php  if(isset($pagination)) { ?>
                     <?php	 echo $pagination;
                        } ?>
                  </div>
                  <?php } else { echo "Sorry, No Records Found!"; } ?>
               </div>
               <!-- // row -->
            </footer>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>