<div class="contentWrapper row">
   <div class="col-md-12">
   
        <?php if( $this->session->flashdata('msg') && $this->session->flashdata('cls') == 'success'):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
        
        <?php if( $this->session->flashdata('cls') && $this->session->flashdata('cls') == 'error'):?>
            <div class="alert alert-warning">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
   
        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="margin-top:10px; margin-left:10px; margin-right:10px;">
            <li class="active"><a href="#about" data-toggle="tab">About Application</a></li>
            <li><a href="#information" data-toggle="tab">Change Information</a></li>
        </ul>
        <div id="my-tab-content" class="tab-content" style="margin-left:10px; margin-right:10px;">
            <div class="tab-pane active" id="about">
                <div class="panel panel-default" style="margin: 0 !important; border-top:0px !important;">
                 <div class="panel-body eq" id="profileDetails">
                    <div class="row">
                       <div class="col-md-3 col-sm-4">
                          <?php if(isset($companydetail['logo']) && $companydetail['logo']!="") { ?>
                          <img src="<?php echo base_url(); ?>logos/<?php echo $companydetail['logo']; ?>" alt="" class="img-responsive" height="150px" width="120px">
                          <?php }
                             else
                             {  ?>
                          <img src="<?php echo base_url(); ?>uploads/default.png" alt="" class="img-responsive" height="150px" width="120px" >
                          <?php	} ?>
                       </div>
                       <div class="col-md-9 col-sm-9">
                          <h4>Company Name:</h4>
                          <p><a href="#"><?php print $companydetail['name'] ; ?></a></p>
                          <h4>Official Email:</h4>
                          <p><a href="#"><?php print $companydetail['client_email'] ; ?></a></p>
                       </div>
                    </div>
                    <!-- // row -->
                 </div>
              </div>
            </div>
            <div class="tab-pane" id="information">
                <?php echo form_open_multipart('settings/update_company'); ?>
                  <div class="panel panel-default" style="margin: 0 !important; border-top:0px !important;">
                     <div class="panel-body eq" id="profileDetails">
                        <div class="row">
                           <div class="col-md-6 col-sm-6">
                              <h4>Company Name: <span class="requiredRed">*</span></h4>
                              <input type="text" placeholder="Enter Company Name" name="name" value="<?php isset($companydetail['name'])?print $companydetail['name']:print set_value('name'); ?>" data-validation="required" />
                              <h4>Official Email: <span class="requiredRed">*</span></h4>
                              <input type="text" placeholder="Enter Email Address" name="email" value="<?php isset($companydetail['client_email'])?print $companydetail['client_email']:print set_value('client_email'); ?>" data-validation="email" />                              
                              <?php if ($this->misc->check_if_root()): ?>
                              <h4>Professionals Label:</h4>
                              <input type="text" placeholder="Physician/Hairstylist ..." name="person_label" value="<?php isset($companydetail['person_label'])?print $companydetail['person_label']:print set_value('person_label'); ?>" />                              
                              <?php endif; ?>
                           </div>
                           <div class="col-md-6 col-sm-6">
                              <h4>Company Logo:</h4>
                              <input type="file" name="upload_image" id="upload_image"><br>    
                           </div>
                           <div class="col-md-6 col-sm-6">
                              <h4>Terms and Conditions text: <span class="requiredRed">*</span></h4>
                              <textarea name="terms" data-validation="required"><?php isset($companydetail['terms']) ? print $companydetail['terms'] : print set_value('terms'); ?></textarea>
                           </div>
                           <div class="col-md-6 col-sm-6">
                              <h4>Number Not found message:</h4>
                              <textarea name="not_found_msg"><?php isset($companydetail['not_found_msg']) ? print $companydetail['not_found_msg'] : print set_value('not_found_msg'); ?></textarea>
                           </div>
                           <div class="col-md-6 col-sm-6">
                              <h4>Exists Number message:</h4>
                              <textarea name="exists_msg"><?php isset($companydetail['exists_msg']) ? print $companydetail['exists_msg'] : print set_value('exists_msg'); ?></textarea>
                           </div>
                           <?php if ($this->misc->check_if_root()): ?>
                           <!--div class="col-md-6 col-sm-6">
                              <h4>Timeout:</h4>
                              <select name="timeout">
                                <option value="" <?php if ($companydetail['timeout'] && $companydetail['timeout'] == '') echo 'selected="selected"'; ?>>No timeout</option>
                                <option value="2" <?php if ($companydetail['timeout'] && $companydetail['timeout'] == '2') echo 'selected="selected"'; ?>>2 minutes</option>
                                <option value="5" <?php if ($companydetail['timeout'] && $companydetail['timeout'] == '5') echo 'selected="selected"'; ?>>5 minutes</option>
                                <option value="7" <?php if ($companydetail['timeout'] && $companydetail['timeout'] == '7') echo 'selected="selected"'; ?>>7 minutes</option>
                                <option value="10" <?php if ($companydetail['timeout'] && $companydetail['timeout'] == '10') echo 'selected="selected"'; ?>>10 minutes</option>
                              </select>
                           </div-->
                           <?php endif; ?>
                        </div>
                          <?php if ($this->misc->check_if_root()): ?>
                          <div class="row">
                            <div class="col-md-6 col-sm-6">
                              <h4>Default professionals image:</h4>
                              <input type="file" name="defaultimage" id="defaultimage" value="" onchange="loadFile(event)"/>
                           </div>
                           </div>
                           <?php if ($this->misc->check_if_root()): ?>
                           <div class="row">
                               <div class="col-md-12 col-sm-12">
                                  <h4>Image preview:</h4>
                                  <div id="imgPrev">
                                        <img id="image_preview" <?php if (isset($companydetail['defaultImage']) && $companydetail['defaultImage'] != '') echo 'width="300" height="300" src="'.base_url('uploads/' . $companydetail['defaultImage']).'"'; ?>/>
                                  </div>
                                  <input type="hidden" id="x" name='x' value=''/>
                                  <input type="hidden" id="y" name='y' value=''/>
                                  <input type="hidden" id="w" name='w' value=''/>
                                  <input type="hidden" id="h" name='h' value=''/>
                               </div>
                           </div>
                           <?php endif; ?>
                          </div>
                          <?php endif; ?>
                        <div class="row">
                           <div class="col-md-12">
                              <button name="submit" type="submit" value="submit" class="saveBtn">Save Settings</button>
                           </div>
                        </div>
                        <!-- // row -->
                     </div>
                  </div>
                  </form>
            </div>
        </div> 
      
   </div>
   <!-- // col md -->
</div>
<!-- // content wrapper -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
   $.validate({
   	errorMessagePosition: $("<div>")
   });
</script>
<script>

    var loadFile = function(event) {
        var reader = new FileReader();
        var output = document.getElementById('image_preview');
    
        reader.onload = function(){            
            output.src = reader.result;
            $('.jcrop-holder img').attr('src', reader.result);
            $('#image_preview').removeAttr('width');
            $('#image_preview').removeAttr('height');
            $("#image_preview").Jcrop({
                aspectRatio: 1,
                onSelect: storeCoords,
                boxWidth: 300,
                boxHeight: 300
            });
        };  
        if(event.target.files[0] !='undefined' && event.target.files[0] != null) {
            reader.readAsDataURL(event.target.files[0]);
        } else {        
            $("#image_preview").attr('src','').data('Jcrop').destroy();
        }
    };
    
    function storeCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };

   $('input,textarea').focus(function(){
      $(this).data('placeholder',$(this).attr('placeholder'))
      $(this).attr('placeholder','');
   });
   $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
   });
   $(document).ready(function() {
      $('#tabs a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
      });
   });
   
   var hash = window.location.hash;
   if (hash == '#location') {
        $('#loc').trigger('click');
   } else if (hash == '#emails') {
        $('#emails_trigger').trigger('click');
   }
</script>