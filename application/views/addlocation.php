<?php if(validation_errors()) { ?>
<div class="alert alert-danger">
   <?php echo validation_errors(); ?>
</div>
<?php } ?>
<script type="text/javascript" src='//maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/js/map.js'); ?>"></script>
<div class="panel panel-default">
   <div class="panel-heading"><?php echo (!isset($edit)) ? 'Add new Location' : 'Edit Location'; ?>
   </div>
   <div class="panel-body" id="profileDetails">
      <?php	echo form_open_multipart('location/insert_location'); ?>
      <div class="row">
         <div class="col-md-6 col-sm-6">
            <h4>Find by address:</h4>
            <input type="text" name="search" id="search" value="<?php isset($userlist['search'])?print $userlist['search']:print set_value('search'); ?>" placeholder="Start Typing Address"/>         
            <h4>Street: <span class="requiredRed">*</span></h4>
            <input type="text" name="address1" id="address1" value="<?php isset($userlist['address1'])?print $userlist['address1']:print set_value('address1'); ?>" data-validation="required" />
            <!--h4>Street 2:</h4>
            <input type="text" disabled="true" name="address2" id="address2" value="<?php isset($userlist['address2'])?print $userlist['address2']:print set_value('address2'); ?>"/>
            -->
            <h4>Zip Code: <span class="requiredRed">*</span></h4>
            <input type="text" name="zipcode" id="zipcode" value="<?php isset($userlist['zipcode'])?print $userlist['zipcode']:print set_value('zipcode'); ?>" data-validation="required"/>
         </div>
         <div class="col-md-6 col-sm-6">
            <h4>Name: <span class="requiredRed">*</span></h4>
            <input type="text" name="name"  value="<?php isset($userlist['name'])?print $userlist['name']:print set_value('name'); ?>" placeholder="Location Name" data-validation="required" />
            
            <h4>City: <span class="requiredRed">*</span></h4>
            <input type="text" name="city" id="city" value="<?php isset($userlist['city'])?print $userlist['city']:print set_value('city'); ?>" data-validation="required" />
            <h4>State: <span class="requiredRed">*</span></h4>
            <input type="text" name="state" id="state" value="<?php isset($userlist['state'])?print $userlist['state']:print set_value('state'); ?>"  data-validation="required" />
          </div>
         </div>
         <div class="row">
            <div class="col-md-12 col-sm-12">
                <div id="map" style="width: 100% !important; height:250px !important;"></div>
                <div class="clear:both;"></div>
                <div>You can drag and drop the marker to the correct location</div>                                
            </div>
         </div>
         <div class="row" style="margin-top: 10px;">
            <div class="col-md-6 col-sm-6">
              <h4>E-mails for 3 and under stars: <span class="requiredRed">*</span></h4>
              <textarea data-validation="required" name="emails3" placeholder="E-mails" /><?php if(isset($userlist['emails_321'])){print $userlist['emails_321'];} ?></textarea>   
              <p>Above persons will receive emails for reviews rated with 3 and under stars</p>
              <p>Note: use comma to separate between emails: user@mail.com,user1@mail.com ...</p>
           </div>
           <div class="col-md-6 col-sm-6">
              <h4>E-mails for 4-5 stars: <span class="requiredRed">*</span></h4>
              <textarea data-validation="required" name="emails45" placeholder="E-mails" /><?php if(isset($userlist['emails_45'])){print $userlist['emails_45'];} ?></textarea>   
              <p>Above persons will receive emails for reviews rated with 4 and 5 stars</p>
              <p>Note: use comma to separate between emails: user@mail.com,user1@mail.com ...</p>
           </div>
         </div>
         <div class="col-md-12">
            <?php if (isset($edit)): ?>
                <input type="hidden" name="edit" value="<?php echo $userlist['id']; ?>"/>
            <?php endif; ?>
            <input type="hidden" id="lon" name="lon" value="<?php isset($userlist['longitude']) ? print $userlist['longitude'] : ''; ?>"/>
            <input type="hidden" id="lat" name="lat" value="<?php isset($userlist['latitude']) ? print $userlist['latitude'] : ''; ?>"/>            
            <button type="submit" class="saveBtn" name="submit" value="Save">Save</button>
            <button type="button" onclick="javascript:document.location='<?php echo site_url('settings'); ?> ' " class="saveBtn" name="cancel" value="cancel">Cancel</button>
         </div>
      </div>
      <!-- // row -->
      </form>
   </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
   $.validate({
   	errorMessagePosition: $("<div>")
   });
   
   $(document).ready(function() {
        $('#zipcode').on('keypress',function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            } 
        });
        
        function updateFields(address) {
            $('#city').val(address.city);
            $('#state').val(address.stateOrProvince);
            $('#zipcode').val(address.postalCode);
            $('#address1').val(address.addressLine1);
        }
        
        $('#map').locationpicker({
            zoom:11,
            <?php if (isset($edit)): ?>
                <?php if ($userlist['latitude'] == ''): ?>
                    location: {latitude: 29.4241921, longitude: -98.5286477},
                <?php else: ?>
                    location: {latitude: <?php echo $userlist['latitude']; ?>, longitude: <?php echo $userlist['longitude']; ?>},
                <?php endif; ?>
            <?php else: ?>
            location: {latitude: 29.4241921, longitude: -98.5286477},
            <?php endif; ?>
            scrollwheel: true,
            radius: 300,
            enableAutocomplete: true,
            inputBinding: {
                locationNameInput: $('#search')
            },
            onchanged: function(currentLocation, radius, isMarkerDropped) {
                var address = $(this).locationpicker('map').location.addressComponents;
                updateFields(address);
                $('#lon').val(currentLocation.longitude);
                $('#lat').val(currentLocation.latitude);
            }
        });  
   });
</script>	