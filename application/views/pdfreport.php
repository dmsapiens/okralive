<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "Survey";
$obj_pdf->SetTitle($title);
//$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->SetHeaderData('','',$title,'');
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
$filters = 'Timeframe: ' . ucfirst($search['timeframe']).'<br />Start Date: ' . $search['fromdate'] . '<br />End Date: ' . $search['todate'] . '<br />Rating: ' . $search['rating'] . '<br />Location: ' . $search['location'];
ob_start();
    // we can have any view part here like HTML, PHP etc
?>	<html>
<head></head>
<body>
<div id="report">
<p><?php echo $filters; ?></p>
<table border="1" width="100%">
  <tr style="text-align:center">
    <th style="text-align:center">Full Name</th>
    <th style="text-align:center">Phone</th>
    <th style="text-align:center">Email</th>
    <th style="text-align:center">Rating</th>
    <th style="text-align:center">Location</th>
    <th style="text-align:center">Date</th>
  </tr>
<?php if($reportlist) {  $i=1; ?>
  <?php foreach($reportlist as $report) {  ?>

  <tr style="text-align:center">
    <td><?php echo $report['firstname']; ?></td>
    <td><?php echo $report['phone_no']; ?></td>
    <td><?php echo $report['emailid']; ?></td>
    <td><?php echo $report['ratingtext']; ?></td>
    <td><?php echo $report['locationName']; ?></td>
    <td><?php echo $report['date']; ?></td>
  </tr>
 <?php $i++; } ?>
 <?php } else { echo "Sorry, No Records Found!"; } ?>


  </table>
</div>
</body>
</html>
<?php    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->Output('report_'.date('m_d_Y').'.pdf', 'D');
?>