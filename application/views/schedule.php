<div id="sumInfo">

    <?php if( $this->session->flashdata('msg') && $this->session->flashdata('cls') == 'success'):?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('msg');?>
        </div>
    <?php endif;?>
    
    <?php if( $this->session->flashdata('cls') && $this->session->flashdata('cls') == 'error'):?>
        <div class="alert alert-warning">
            <?php echo $this->session->flashdata('msg');?>
        </div>
    <?php endif;?>
    
    <?php
    if (isset($info)) {
        $cron = explode(' ',$info['cron']);
        $hour = $cron[1];
        $minute = $cron[0];
        $day = $cron[4];
    }
    ?>

   <div class="row filterBox nopad">
      <div class="col-md-12">
         <h2>Configure Schedule</h2>
      </div>
      <?php $formattributes = array('name' => 'scheduleform'); echo form_open('schedules/save',$formattributes); ?>
      <div class="col-md-4 col-sm-4">
         <label for="">Hour</label>
         <select name="hour" id="hour">
            <option value="1" <?php if (isset($info['cron']) && $hour == '1') echo 'selected="selected"'; ?>>1 AM</option>
            <option value="2" <?php if (isset($info['cron']) && $hour == '2') echo 'selected="selected"'; ?>>2 AM</option>
            <option value="3" <?php if (isset($info['cron']) && $hour == '3') echo 'selected="selected"'; ?>>3 AM</option>
            <option value="4" <?php if (isset($info['cron']) && $hour == '4') echo 'selected="selected"'; ?>>4 AM</option>
            <option value="5" <?php if (isset($info['cron']) && $hour == '5') echo 'selected="selected"'; ?>>5 AM</option>
            <option value="6" <?php if (isset($info['cron']) && $hour == '6') echo 'selected="selected"'; ?>>6 AM</option>
            <option value="7" <?php if (isset($info['cron']) && $hour == '7') echo 'selected="selected"'; ?>>7 AM</option>
            <option value="8" <?php if (isset($info['cron']) && $hour == '8') echo 'selected="selected"'; ?>>8 AM</option>
            <option value="9" <?php if (isset($info['cron']) && $hour == '9') echo 'selected="selected"'; ?>>9 AM</option>
            <option value="10" <?php if (isset($info['cron']) && $hour == '10') echo 'selected="selected"'; ?>>10 AM</option>
            <option value="11" <?php if (isset($info['cron']) && $hour == '11') echo 'selected="selected"'; ?>>11 AM</option>
            <option value="12" <?php if (isset($info['cron']) && $hour == '12') echo 'selected="selected"'; ?>>12 PM</option>
            <option value="13" <?php if (isset($info['cron']) && $hour == '13') echo 'selected="selected"'; ?>>1 PM</option>
            <option value="14" <?php if (isset($info['cron']) && $hour == '14') echo 'selected="selected"'; ?>>2 PM</option>
            <option value="15" <?php if (isset($info['cron']) && $hour == '15') echo 'selected="selected"'; ?>>3 PM</option>
            <option value="16" <?php if (isset($info['cron']) && $hour == '16') echo 'selected="selected"'; ?>>4 PM</option>
            <option value="17" <?php if (isset($info['cron']) && $hour == '17') echo 'selected="selected"'; ?>>5 PM</option>
            <option value="18" <?php if (isset($info['cron']) && $hour == '18') echo 'selected="selected"'; ?>>6 PM</option>
            <option value="19" <?php if (isset($info['cron']) && $hour == '19') echo 'selected="selected"'; ?>>7 PM</option>
            <option value="20" <?php if (isset($info['cron']) && $hour == '20') echo 'selected="selected"'; ?>>8 PM</option>
            <option value="21" <?php if (isset($info['cron']) && $hour == '21') echo 'selected="selected"'; ?>>9 PM</option>
            <option value="22" <?php if (isset($info['cron']) && $hour == '22') echo 'selected="selected"'; ?>>10 PM</option>
            <option value="23" <?php if (isset($info['cron']) && $hour == '23') echo 'selected="selected"'; ?>>11 PM</option>
            <option value="24" <?php if (isset($info['cron']) && $hour == '24') echo 'selected="selected"'; ?>>12 PM</option>
         </select>
      </div>
      <div class="col-md-4 col-sm-4">
         <label for="">Minute</label>
         <select name="minute" id="minute">
            <?php for ($i=1; $i<31; $i++): ?>
                <?php $sel = ($i == $minute) ? 'selected="selected"' : ''; ?>
                <option value="<?php echo $i; ?>" <?php echo $sel; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
         </select>
      </div>
      <div class="col-md-4 col-sm-4">
         <label for="">Day</label>
         <select name="day" id="day">
            <option value="*" <?php if (isset($info['cron']) && $day == '*') echo 'selected="selected"'; ?>>Every Day</option>
            <option value="1" <?php if (isset($info['cron']) && $day == '1') echo 'selected="selected"'; ?>>Monday</option>
            <option value="2" <?php if (isset($info['cron']) && $day == '2') echo 'selected="selected"'; ?>>Tuesday</option>
            <option value="3" <?php if (isset($info['cron']) && $day == '3') echo 'selected="selected"'; ?>>Wednesday</option>
            <option value="4" <?php if (isset($info['cron']) && $day == '4') echo 'selected="selected"'; ?>>Thursday</option>
            <option value="5" <?php if (isset($info['cron']) && $day == '5') echo 'selected="selected"'; ?>>Friday</option>
            <option value="6" <?php if (isset($info['cron']) && $day == '6') echo 'selected="selected"'; ?>>Saturday</option>
            <option value="7" <?php if (isset($info['cron']) && $day == '7') echo 'selected="selected"'; ?>>Sunday</option>
         </select>
      </div>
      <!-- // col sm 6  -->
   </div>
   <!-- // row  -->
   <div class="row filterBox nopad">
      <div class="col-md-4 col-sm-4">
         <label for="">Rating</label>
         <select name="rating" id="rating">
            <option value="0" <?php if (isset($info['rating']) && $info['rating'] == '0') echo 'selected="selected"'; ?>>All</option>
            <option value="5" <?php if (isset($info['rating']) && $info['rating'] == '5') echo 'selected="selected"'; ?>>Extremely Satisfied</option>
            <option value="4" <?php if (isset($info['rating']) && $info['rating'] == '4') echo 'selected="selected"'; ?>>Very Satisfied</option>
            <option value="3" <?php if (isset($info['rating']) && $info['rating'] == '3') echo 'selected="selected"'; ?>>Satisfied</option>
            <option value="2" <?php if (isset($info['rating']) && $info['rating'] == '2') echo 'selected="selected"'; ?>>Dissatisfied</option>
            <option value="1" <?php if (isset($info['rating']) && $info['rating'] == '1') echo 'selected="selected"'; ?>>Very Dissatisfied</option>
         </select>
      </div>
      <!-- // col  -->
      <!-- // col sm 6  -->
      <div class="col-md-4 col-sm-4">
         <label for="">Format</label>
         <select name="format" id="format">
            <option value="pdf" <?php if (isset($info['fileFormat']) && $info['fileFormat'] == 'pdf') echo 'selected="selected"'; ?>>PDF</option>
            <option value="xls" <?php if (isset($info['fileFormat']) && $info['fileFormat'] == 'xls') echo 'selected="selected"'; ?>>XLS</option>
            <option value="csv" <?php if (isset($info['fileFormat']) && $info['fileFormat'] == 'csv') echo 'selected="selected"'; ?>>CSV</option>
         </select>
      </div>
      
      <div class="col-md-4 col-sm-4">
         <label for="">Location</label>
         <select name="location" id="location">
            <option value="0">All Locations</option>
            <?php foreach ($loc as $l): ?>
                <?php $sel = (isset($info['location']) && $info['location'] == $l['id']) ? 'selected="selected"' : ''; ?>
                <option <?php echo $sel; ?> value="<?php echo $l['id']; ?>"><?php echo $l['name']; ?></option>
            <?php endforeach; ?>
         </select>
      </div>
      
      <div class="col-md-4 col-sm-4">
         <label for="">Email</label>
         <input type="text" name="email" value="<?php isset($info['email'])?print $info['email']:print set_value('email'); ?>" id="email" data-validation="email"/>
      </div>
      
      <?php if ($this->misc->getPerson() != ''): ?>
      <div class="col-md-4 col-sm-4">
         <label for=""><?php echo $this->misc->getPerson(); ?></label>
         <select name="doctor" id="doctor">
            <option value="0">All <?php echo $this->misc->getPerson(); ?></option>
            <?php foreach ($doctors as $l): ?>
                <?php $sel = (isset($info['doctor_id']) && $info['doctor_id'] == $l['id']) ? 'selected="selected"' : ''; ?>
                <option <?php echo $sel; ?> value="<?php echo $l['id']; ?>"><?php echo $l['firstname'] . ' '. $l['lastname']; ?></option>
            <?php endforeach; ?>
         </select>
      </div>
      <!-- // col sm 6  -->
      <?php endif; ?>
   </div>
   <!-- // row  -->
   <div class="row filterBox nopad">
      <div class="col-md-4 col-md-offset-4">
         <input type="hidden" id="timezone_offset" name="timezoneoffset" value=""/>
         <input type="hidden" id="timezonedst" name="timezonedst" value=""/>
         <?php if (isset($info['id'])): ?>
         <input type="hidden" name="id" value="<?php echo $info['id']; ?>"/>
         <?php endif; ?>
         <a href="javascript:void(0);" class="generateBtn"><img src="<?php echo base_url(); ?>assets/admin/img/ico/generate.png" alt="">Save Schedule</a>
      </div>
   </div>
   </form>
   <!-- // row  -->
   <div class="clearfix"></div>
</div>
<!-- // sum info -->

<div class="contentWrapper row">
   <div class="col-md-12">
   
      <div class="panel panel-default">
         <div class="panel-heading">
            <img src="<?php echo base_url(); ?>assets/admin/img/misc/schedule.png" alt="">Existing Schedules
         </div>
         <div class="panel-body">
            <table class="table table-striped table-hover">
               <?php if($list) {  	 ?>
               <tr>
                  <th scope="col">Hour</th>
                  <th scope="col">Minute</th>
                  <th scope="col">Day</th>
                  <th scope="col">Rating</th>
                  <th scope="col">Location</th>
                  <th scope="col">Format</th>
                  <th scope="col" class="text-center">Action</th>
               </tr>
               <?php if(isset($list)) { foreach($list as $r) { ?>
               <?php $times = explode(' ',$r['cron']); ?>
               <?php if ($times[1] != '*') { ?>
               <?php $time = mktime($times[1],0,0,1,1,2015); $usHour = date('g A',$time); ?>
               <?php } else { $usHour = 'Every Hours'; } ?>
               <?php
               if ($times[4] == '*') {
                    $day = 'Every Day';
               } else if ($times[4] == '1') {
                    $day = 'Monday';
               } else if ($times[4] == '2') {
                    $day = 'Tuesday';
               } else if ($times[4] == '3') {
                    $day = 'Wednesday';
               } else if ($times[4] == '4') {
                    $day = 'Thursday';
               } else if ($times[4] == '5') {
                    $day = 'Friday';
               } else if ($times[4] == '6') {
                    $day = 'Saturday';
               } else {
                    $day = 'Sunday';
               }
               ?>
               <tr>
                  <td><?php print $usHour; ?> </td>
                  <td><?php echo ($times[0] == '*') ? 'Every Minute' : $times[0]; ?> </td>
                  <td><?php print $day; ?> </td>
                  <td>
                  <?php
                     switch($r['rating'])
                     {
                     	case "0":
                     		echo 'All Ratings';
                     		break;
                     	case "1":
                     		echo 'Very Dissatisfied';
                     		break;
                     	case "2":
                     		echo 'Dissatisfied';
                     		break;
                        case "3":
                            echo 'Satisfied';
                            break;
                        case "4":
                            echo 'Very Satisfied';
                            break;
                        case "5":
                            echo 'Extremely Satisfied';
                            break;
                     }
                  ?>
                  </td>
                  <td><?php print $this->misc->locationName($r['location']); ?></td>
                  <td><?php print ucwords($r['fileFormat']); ?></td>
                  <td class="text-center">
                     <a href="<?php echo base_url(); ?>schedules/edit/<?php print $r['id']; ?>" class="editBtn2" data-toggle="tooltip" data-placement="top" title="Edit">Edit</a>
                     <a href="<?php echo base_url('schedules/remove/'.$r['id']); ?>" onclick="return confirm('Are you sure?');" class="removeBtn" data-toggle="tooltip" data-placement="top" title="Remove Schedule">Remove</a>
                  </td>
               </tr>
               <?php } } ?>
               <?php } else { ?>
               <tr>
               <tr>Sorry, No Records Found!</td></tr>
               <?php } ?>
            </table>
      </div>
   </div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
$.validate({
    errorMessagePosition: $("<div>")
});
$(document).ready(function() {
    
    /*$('#doctor').searchable({
        maxMultiMatch: 50,
        maxListSize: 100,
        exactMatch: false,
        wildcards: true,
        ignoreCase: true,
        latency: 200,
        warnMultiMatch: 'top {0} matches ...',
        warnNoMatch: 'no matches ...',
        zIndex: 'auto' 
    });*/
    
    var now = new Date();
    var later = new Date();
    later.setTime(now.getTime() + 365 * 24 * 60 * 60 * 1000);
    var d1 = new Date();
    var d2 = new Date();
    d1.setDate(1);
    d1.setMonth(1);
    d2.setDate(1);
    d2.setMonth(7);
    if(parseInt(d1.getTimezoneOffset())==parseInt(d2.getTimezoneOffset()))
    {
        $('#timezonedst').val('0');
    }
    else {
        var hemisphere = parseInt(d1.getTimezoneOffset())-parseInt(d2.getTimezoneOffset());
        if((hemisphere>0 && parseInt(d1.getTimezoneOffset())==parseInt(now.getTimezoneOffset())) || (hemisphere<0 && parseInt(d2.getTimezoneOffset())==parseInt(now.getTimezoneOffset()))) { 
            $('#timezonedst').val('0');
        } else { 
            $('#timezonedst').val('1');
        } 
    }
    $('#timezone_offset').val(now.getTimezoneOffset());
    
    $(".generateBtn").on("click",function(){
        if ($('#email').val() == '') {
            alert('Enter your email!');
            return false;
        }
        document.scheduleform.submit();
    });
});
</script>