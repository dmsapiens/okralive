<div id="sumInfo">
   <div class="clearfix"></div>
</div>
<!-- // sum info -->
<div class="contentWrapper row">
   <div class="col-md-12">
   
    <?php if(($this->session->flashdata('error'))):?>
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if( $this->session->flashdata('msg') ):?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('msg');?>
            </div>
        <?php endif;?>
   
      <div class="panel panel-default">
         <div class="panel-heading">Choose the user</div>
         <div class="panel-body">
            <select  class="form-control" name="users" id="users">
                <option value="">All</option>
                <?php $all = $this->db->query("SELECT sr_admincontrol.*,sr_clients.name AS clientName FROM sr_admincontrol LEFT JOIN sr_clients ON sr_admincontrol.client_id = sr_clients.id ORDER BY sr_admincontrol.id DESC"); ?>
                <?php if ($all): ?>
                    <?php $all = $all->result_array(); ?>
                    <?php foreach ($all as $r): ?>
                        <option value="<?php echo $r['id']; ?>-<?php echo $r['client_id']; ?>"><?php echo $r['firstname'].' '.$r['lastname']; ?> from <?php echo $r['clientName']; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
         </div>
      </div>
   </div>
   <!-- // col md -->
</div>
<script>

    $(document).ready(function() {
       $("#users").select2({
        placeholder: "Select user to login"
       }); 
       $('.select2-container--default').css('width','100%');
       
       $('#users').on('change',function() {
            var val = $(this).val();
            var t = val.split('-');
            
            var c = confirm('Are you sure you want to login as this user?');
            if (c) {
                window.location = '<?php echo base_url('account/login/'); ?>/' + t[0] + '/' + t[1];
            }
       })
    });
</script>