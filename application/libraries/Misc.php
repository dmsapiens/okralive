<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Misc {
    
    private $CI;
    
    public function __construct()
    {
        $this->CI =& get_instance();
    }
    
    public function remoteFileExists($url)
    {
        $curl = curl_init($url);
    
        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);
    
        //do request
        $result = curl_exec($curl);
    
        $ret = false;
    
        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
    
            if ($statusCode == 200) {
                $ret = true;   
            }
        }
    
        curl_close($curl);
    
        return $ret;
    }
    
    public function removedoctor($id)
    {
		$this->CI->db->query("DELETE FROM sr_doctors WHERE id = ? AND client_id = ?",array($id,$this->CI->session->userdata['logged_in']['client_id']));
		return true;
    }
    
    public function get_doctor($id)
	{
		$this->CI->db->where('id',$id);
		$query = $this->CI->db->get('sr_doctors');
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		return $query->row_array();
	}
    
    public function update_doctor($image)
	{
		
		$phone_num = $this->CI->input->post('phone');
		$id = $this->CI->input->post('cid');
        $locations = implode(',',$this->CI->input->post('usertype'));
		$data = array(
			'firstname' => $this->CI->input->post('firstname'),
			'lastname' => $this->CI->input->post('lastname'),
			'phone' => $phone_num,
			'email' => $this->CI->input->post('email'),
			'location' => $locations,
			'degree'=>$this->CI->input->post('degree'),
            'address'=>$this->CI->input->post('address'),
            'client_id' => $this->CI->session->userdata['logged_in']['client_id']
		);
        
        if ($image) {
            $data['image'] = $image;
        }
        
		$this->CI->db->where('id', $id);
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->update('sr_doctors', $data);
	}
    
    public function insert_doctor($image = false)
	{
		$phone_num = $this->CI->input->post('phone');
		$data = array(
    		'firstname' => $this->CI->input->post('firstname'),
    		'lastname' => $this->CI->input->post('lastname'),
    		'phone' => $phone_num,
    		'email' => $this->CI->input->post('email'),
    		'address' => $this->CI->input->post('address'),
    		'location' => implode(',',$this->CI->input->post('usertype')),
            'degree' => $this->CI->input->post('degree'),
            'client_id' => $this->CI->session->userdata['logged_in']['client_id']
		);
        if ($image) {
            $data['image'] = $image;
        }
		$q = $this->CI->db->insert('sr_doctors',$data);
	}
    
    public function getdoctorlist($per_page,$starting)
	{
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$query = $this->CI->db->get('sr_doctors',$per_page,$starting);
		return $query->result_array();
	}
    
    public function getdocloc($id)
    {
        $a = explode(',',$id);
        $name = false;
        $br = false;
        if (count($a) > 1) {
            $br = true;
        }
        foreach ($a as $doc) {
            $this->CI->db->where('id',$doc);
            $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
    		$query = $this->CI->db->get('sr_locations');
            $name .= $query->row()->name;
            if ($br) {
                $name .= '<br />';
            }
        }
        
        return $name;
    }
    
    public function gettotaldoctors()
    {
        $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$query = $this->CI->db->get('sr_doctors');
		return $query->num_rows();
    }
    
    public function check_if_root()
    {
        $u = $this->CI->session->userdata('logged_in');
        if ($u['logintype'] == 'root') {
            return true;
        }
        return false;
    }
    
    public function login($email,$password)
    {
        $this->CI->db->from('sr_admincontrol');
        $this->CI->db->join('sr_clients','sr_admincontrol.client_id = sr_clients.id');
		$this->CI->db->where('email = ' . "'" . $email . "'");
		$this->CI->db->where('password = ' . "'" . MD5($password) . "'");
		$this->CI->db->limit(1);
		$query = $this->CI->db->get();
        if($query->num_rows() > 0) {
            return $query->result_array();
		}
        
        return false;
	}
    
    public function loginById($uid,$cid)
    {
        /*$this->CI->db->from('sr_admincontrol');
        $this->CI->db->join('sr_clients','sr_admincontrol.client_id = sr_clients.id');
		$this->CI->db->where('sr_admincontrol.id = '.$uid);
        $this->CI->db->where('sr_admincontrol.client_id = '.$cid);
		$this->CI->db->limit(1);*/
        
        $query = $this->CI->db->query("SELECT sr_clients.*,sr_admincontrol.id AS aid,sr_admincontrol.email,sr_admincontrol.username,sr_admincontrol.firstname,sr_admincontrol.lastname,sr_admincontrol.usertype FROM sr_admincontrol LEFT JOIN sr_clients ON sr_admincontrol.client_id = sr_clients.id WHERE sr_admincontrol.id = $uid AND sr_admincontrol.client_id = $cid LIMIT 1");
        if($query->num_rows() > 0) {
            return $query->result_array();
		}
        
        return false;
    }
    
    public function scheduleList()
    {
        $u = $this->CI->session->userdata('logged_in');
        $userId = $u['id'];
        $q = $this->CI->db->query("SELECT * FROM sr_schedule WHERE user_id = ? AND client_id = ? ORDER BY id DESC",array($userId,$this->CI->session->userdata['logged_in']['client_id']));
        return $q->result_array();
    }
    
    public function scheduleInfo($id)
    {
        $u = $this->CI->session->userdata('logged_in');
        $userId = $u['id'];
        $q = $this->CI->db->query("SELECT * FROM sr_schedule WHERE user_id = ? AND id = ? AND client_id = ? ORDER BY id DESC",array($userId,$id,$this->CI->session->userdata['logged_in']['client_id']));
        return $q->row_array();
    }
    
    public function markAsContacted($id)
    {
        if (!$id) return false;
        
        $data = array(
		  'status' => 2
		);
		$this->CI->db->where('srw.rating_id',$id);
        $this->CI->db->where('srw.client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->update('sr_review srw',$data);
        return true;
    }
    
    public function getsatisfiedlist($values,$perpage,$start,$diss = false,$search = false, $all = false)
    {
        if($search) {
			switch ($search) {
				case 'today' : 		$this->CI->db->where('DATE(sr.entry_date) = CURDATE()'); break;
				case '7' : 			$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)'); break;
				case '30' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)'); break;
 				case '90' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)'); break;
				case '180' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 180 DAY)'); break;
				case '365' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)'); break;
			}
		}
        
        $this->CI->db->select('doctor.firstname AS dfname,doctor.lastname AS dflname,sn.*,(sr.id) as rating_id,(sr.entry_date) as date,sr.id,sr.rating,srw.status,uploads.file,srl.name AS locationName', 'asc');
        $this->CI->db->where('sr.is_delete',0);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
        if (!$diss && !$all) {
            $this->CI->db->where('srw.status',1);
        }
        if ($all) {
            $where = '(`srw`.`status` = 1 OR `srw`.`status` = 0)';
            //$this->CI->db->where('srw.status',0);
            //$this->CI->db->or_where('srw.status',1);
            $this->CI->db->where($where,null,false);
        }
        $this->CI->db->where_in('sr.rating_no', $values);
        $this->CI->db->join('sr_newuser sn', 'sn.id = sr.user_id','left');
        $this->CI->db->join('sr_review srw', 'sr.id = srw.rating_id','left');
        $this->CI->db->join('sr_doctors doctor', 'sr.doctor_id=doctor.id','left');
        // Location
        $this->CI->db->join('sr_locations srl', 'srw.location = srl.id','left');
        $this->CI->db->join('sr_reviews uploads', 'srw.rating_id = uploads.rating_id', 'left');
        $this->CI->db->order_by('date','desc');
        $query = $this->CI->db->get('sr_rating sr',$perpage,$start);     
        #echo $this->CI->db->last_query(); exit;   
        return $query->result_array();
	}
    
    public function getDoctorReviews($values,$perpage,$start,$diss = false,$search = false, $all = false, $doctor_id = false)
    {
        if (!$doctor_id) return false;   
        $this->CI->db->select('doctor.firstname AS dfname,doctor.lastname AS dflname,sn.*,(sr.id) as rating_id,(sr.entry_date) as date,sr.id,sr.rating,srw.status,uploads.file,srl.name AS locationName', 'asc');
        $this->CI->db->where('sr.is_delete',0);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
        $this->CI->db->where_in('sr.rating_no', $values);
        $this->CI->db->where('sr.doctor_id',$doctor_id);
        $this->CI->db->join('sr_newuser sn', 'sn.id = sr.user_id','left');
        $this->CI->db->join('sr_review srw', 'sr.id = srw.rating_id','left');
        $this->CI->db->join('sr_doctors doctor', 'sr.doctor_id=doctor.id','left');
        // Location
        $this->CI->db->join('sr_locations srl', 'srw.location = srl.id','left');
        $this->CI->db->join('sr_reviews uploads', 'srw.rating_id = uploads.rating_id', 'left');
        $this->CI->db->order_by('date','desc');
        $query = $this->CI->db->get('sr_rating sr',$perpage,$start);     
        #echo $this->CI->db->last_query(); exit;   
        return $query->result_array();
	}
    
    public function archiveReview($id)
    {
        if (!$id) return false;
        
        $q = $this->CI->db->query("SELECT rating_id FROM sr_review WHERE rating_id = ? AND client_id = ?",array($id,$this->CI->session->userdata['logged_in']['client_id']));
        if ($q->num_rows() > 0) {
            $this->CI->db->query("UPDATE sr_rating SET is_delete = 1 WHERE id = ? AND client_id = ?",array($q->row()->rating_id,$this->CI->session->userdata['logged_in']['client_id']));
            $this->CI->db->query("UPDATE sr_review SET status = 2 WHERE rating_id = ? AND client_id = ?",array($id,$this->CI->session->userdata['logged_in']['client_id']));
            return true;
        }
    } 
        
    public function getsatisfiedlisttotal($values)
	{
        #$this->CI->db->select('sn.*,(sr.id) as rating_id', 'asc');
        $this->CI->db->select('sr.id as rating_id', 'asc');
        $this->CI->db->where('sr.is_delete',0);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
        $this->CI->db->where_in('sr.rating_no', $values);
        #$this->CI->db->join('sr_newuser sn', 'sn.id= sr.user_id','left');
        $query = $this->CI->db->get('sr_rating sr');
        return $query->num_rows();
	}
    
    public function getratinglist()
    {
        $this->CI->db->select('COUNT(*) AS counts,rating_no', 'ASC');
        $this->CI->db->group_by('rating_no');
	    $this->CI->db->where('sr.is_delete',0);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
	    $query = $this->CI->db->get('sr_rating sr');
	    $this->CI->db->last_query();
	    return $query->result_array();
	}
    
    public function check_password_request($id)
    {
        if (!$id) return false;
        
        $q = $this->CI->db->query("SELECT email FROM sr_admincontrol WHERE tempPasswd = ?",array($id));
        if ($q->num_rows() > 0) {
            $this->CI->load->helper('string');
            $passwd = random_string('alnum', 6);
            $md5 = md5($passwd);
            $this->CI->db->query("UPDATE sr_admincontrol SET tempPasswd = '', password = '$md5' WHERE tempPasswd = ?",array($id));
            
            /** Send email **/
            $msg = '
            Thanks for confirming your request.<br /><br />
            
            Your new password is: '.$passwd.'<br /><br />
            
            Please login to '.base_url('/').'<br /><br />
            
            Thanks,<br/><br />
            Support Team
            ';
            
            $this->sendEmail($q->row()->email,'support@1qreputation.com','New Password',$msg);
            return true;
        }
        
        return false;
    }
    
    public function check_email($email)
    {
        if (!$email) return false;
        
        $q = $this->CI->db->query("SELECT 1 FROM sr_admincontrol WHERE email = ?",array($email));
        if ($q->num_rows() > 0) {
            $this->CI->load->helper('string');
            $md5 = md5(random_string('alnum', 6));
            $this->CI->db->query("UPDATE sr_admincontrol SET tempPasswd = '$md5' WHERE email = ?",array($email));
            
            /** Send email **/
            $msg = '
            Hello,<br /><br />
            To reset your password, click on the URL below to enter confirm your request:
            <br /><br />
            Click on the URL below to enter confirm your request:
            <br /><br />
            <a href="'.base_url('reset-your-password/' . $md5).'">'.base_url('reset-your-password/' . $md5).'</a><br /><br />
            Thank you,<br /><br />
            Support Team
            ';
            
            $this->sendEmail($email,'support@1qreputation.com','Forgot Password',$msg);
            return true;
        }
        
        return false;
    }
    
    private function sendEmail($to,$from,$subject,$msg)
    {
        $this->CI->load->library('email');
        $this->CI->email->from($from, APP_COMPANY);
        $this->CI->email->to($to);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->subject($subject);
        $this->CI->email->message($msg);
        $this->CI->email->send();
    }
    
    public function supportSend()
    {
        $q = $this->CI->db->query("SELECT name,client_email FROM sr_company WHERE id = ?",array($this->CI->session->userdata['logged_in']['client_id']));
        
        $msg = 'Dear Admin,<br /><br />
        
        You have received a suggestion from ' . $q->row()->name . ' here are the details:<br /><br />
        
        Name: ' . $this->CI->input->post('firstname') . '<br/>
        Email: ' . $q->row()->email . '<br />
        Message:<br />
        '.$this->CI->input->post('message').'<br /><br /><br />
        
        Date sent: ' . date('m/d/Y h:i a');
        
        $this->sendEmail('modche@gmail.com','support@1qreputation.com',$this->CI->input->post('subject'),$msg);
        return true;
    }
    
    public function getSatisfiedDetails($id)
    {
   	    $this->CI->db->select("sn.*,sr.rating,doctor.firstname AS dfname,doctor.lastname AS dflname,srw.status,srw.review,srw.title,sr.entry_date,uploads.file,uploads.vimeourl,uploads.width,uploads.videoId,uploads.height", 'asc');
    	$this->CI->db->where('sr.id',$id);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
    	$this->CI->db->join('sr_rating sr', 'sr.user_id= sn.id','left');
    	$this->CI->db->join('sr_review srw', 'srw.rating_id=sr.id','left');
        $this->CI->db->join('sr_doctors doctor', 'sr.doctor_id=doctor.id','left');
        $this->CI->db->join('sr_reviews uploads', 'srw.rating_id = uploads.rating_id', 'left');
    	$query = $this->CI->db->get('sr_newuser sn');
    	return $query->result_array();
    
    }
    
    public function get_socialshare($id)
    {
        $this->CI->db->select("ss.social_type", 'asc');
        $this->CI->db->group_by('social_type');
        $this->CI->db->where('ss.rating_id',$id);
        $this->CI->db->where('ss.client_id',$this->CI->session->userdata['logged_in']['client_id']);
        $query = $this->CI->db->get('sr_socialshare ss');
        return $query->result_array();
    }
    
    public function convertdatetomysql($date)
    {
   	    $day = (int) substr($date, 0, 2);
    	$month = (int) substr($date, 3, 2);
    	$year = (int) substr($date, 6, 4);
    
    	$unixtime = mktime(0, 0, 0, $month, $day, $year);
    	$mysqldate = date('Y-m-d',$unixtime);
    	return $mysqldate;
    }
    
    public function convertdatefrommysql($date)
    {
    	$day = substr($date, 8, 2);
    	$month = substr($date, 5, 2);
    	$year = (int) substr($date, 0, 4);
    	if($day!=null)
    	{
    		$mysqldate = $month."/".$day."/".$year;
    	}
    	else
    	{
    		$mysqldate="";
    	}
    	return $mysqldate;
    }
    
    public function searchterm_handler($searchterm)
	{
    	if($searchterm)
    	{
    		$this->session->set_userdata('searchterm', $searchterm);
    	}
    	elseif($this->session->userdata('searchterm'))
    	{
    		$searchterm = $this->session->userdata('searchterm');
    	}
    	else
    	{
    		$searchterm = '';
    
    	}
    	return $searchterm;
	}
    
    public function getreportlist($data)
	{
		if($data['timeframe']!='' && ($data['fromdate']=='' && $data['todate']==''))
		{
			switch ($data['timeframe'])
			{
				case 'today' : 		$this->CI->db->where('DATE(sr.entry_date) = CURDATE()'); break;
				case '7' : 			$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)'); break;
				case '30' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)'); break;
 				case '90' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)'); break;
				case '180' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 180 DAY)'); break;
				case '365' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)'); break;

			}
		}

		if($data['fromdate']!='')
		{
			list($m,$d,$y) = explode("/",$data['fromdate']);
			$fromdate = $y."-".$m."-".$d;
			$this->CI->db->where('date(sr.entry_date) >= ',$fromdate);
		}
        
        if($data['patient']!='')
		{
			$this->CI->db->where('sr.user_id = ' . $data['patient']);
		}
        
        if($data['doctor']!='')
		{
			$this->CI->db->where('sr.doctor_id = ' . $data['doctor']);
		}

		if($data['todate']!='')
		{
			list($m,$d,$y) = explode("/",$data['todate']);
			$todate = $y."-".$m."-".$d;
			$this->CI->db->where('date(sr.entry_date) <=',$todate);
		}

        $c = false;
		if($data['rating']!='' && $data['rating']!='all')
		{
            if ($data['rating'] == '0') {
                $this->CI->db->where('sr.is_delete',1);
            } else {
                $c = true;
                $this->CI->db->where('sr.rating_no',$data['rating']);
            }
		}

		if($data['havemail']!='' && $data['havemail']!='all')
		{
			switch ($data['havemail'])
			{
				case 'yes' : 		$this->CI->db->where('sn.emailid != ""'); break;
				case 'no' : 		$this->CI->db->where('sn.emailid == ""'); break;
			}
		}
        
        if ($data['location'] != '' && $data['location'] != 'all') {
            $this->CI->db->where('srl.id',$data['location']);
        }
        
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);                

		$this->CI->db->select('sn.*,(sr.id) as rating_id,sr.rating,sr.rating_no,date_format(entry_date,"%m/%d/%Y") as date, srl.name AS locationName', false);
		$this->CI->db->select("IF(sr.rating_no = 1 , 'Very Dissatisfied' , IF(sr.rating_no = 2 , 'Dissatisfied' , IF(sr.rating_no = 3 , 'Satisfied' , IF(sr.rating_no = 4 , 'Very Satisfied' , IF(sr.rating_no = 5 , 'Extremely Satisfied' , '' ) ) ) ) ) as ratingtext",false);
		if ($data['rating']=='all' || $c) {
           $this->CI->db->where('sr.is_delete',0);
        }
		$this->CI->db->join('sr_newuser sn', 'sn.id= sr.user_id','left');
        // Location
        $this->CI->db->join('sr_locations srl', 'sr.location = srl.id','left');
		$this->CI->db->order_by("entry_date", "asc");
        
		$query = $this->CI->db->get('sr_rating sr');
        
		return $query->result_array();
	}
    
    public function getreportlisttotal($data)
	{
		if($data['timeframe']!='' && ($data['fromdate']=='' && $data['todate']==''))
		{
			switch ($data['timeframe'])
			{
				case 'today' : 		$this->CI->db->where('DATE(sr.entry_date) = CURDATE()'); break;
				case '7' : 			$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)'); break;
				case '30' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)'); break;
 				case '90' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)'); break;
				case '180' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 180 DAY)'); break;
				case '365' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)'); break;

			}
		}

		if($data['fromdate']!='')
		{
			list($m,$d,$y) = explode("/",$data['fromdate']);
			$fromdate = $y."-".$m."-".$d;
			$this->CI->db->where('date(sr.entry_date) >= ',$fromdate);
		}
        
        if($data['doctor']!='')
		{
			$this->CI->db->where('sr.doctor_id = ' . $data['doctor']);
		}
        
        if($data['patient']!='')
		{
			$this->CI->db->where('sr.user_id = ' . $data['patient']);
		}

		if($data['todate']!='')
		{
			list($m,$d,$y) = explode("/",$data['todate']);
			$todate = $y."-".$m."-".$d;
			$this->CI->db->where('date(sr.entry_date) <=',$todate);
		}

		if($data['rating']!='' && $data['rating']!='all')
		{
			$this->CI->db->where('sr.rating_no',$data['rating']);
		}

		if($data['havemail']!='' && $data['havemail']!='all')
		{
			switch ($data['havemail'])
			{
				case 'yes' : 		$this->CI->db->where('sn.emailid != ""'); break;
				case 'no' : 		$this->CI->db->where('sn.emailid == ""'); break;
			}
		}

        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->select('sn.*,(sr.id) as rating_id,sr.rating,sr.rating_no', 'asc');
		$this->CI->db->select("IF(sr.rating_no = 1 , 'Very Dissatisfied' , IF(sr.rating_no = 2 , 'Dissatisfied' , IF(sr.rating_no = 3 , 'Satisfied' , IF(sr.rating_no = 4 , 'Very Satisfied' , IF(sr.rating_no = 5 , 'Extremely Satisfied' , '' ) ) ) ) ) as ratingtext",false);
		$this->CI->db->where('sr.is_delete',0);
		$this->CI->db->join('sr_newuser sn', 'sn.id= sr.user_id','left');
		$query = $this->CI->db->get('sr_rating sr');

		return $query->num_rows();
	}
    
    public function getreportlistXls($data)
	{
		if($data['timeframe']!='' && ($data['fromdate']=='' && $data['todate']==''))
		{
			switch ($data['timeframe'])
			{
				case 'today' : 		$this->CI->db->where('DATE(sr.entry_date) = CURDATE()'); break;
				case '7' : 			$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)'); break;
				case '30' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)'); break;
 				case '90' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)'); break;
				case '180' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 180 DAY)'); break;
				case '365' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)'); break;

			}
		}

		if($data['fromdate']!='')
		{
			list($m,$d,$y) = explode("/",$data['fromdate']);
			$fromdate = $y."-".$m."-".$d;
			$this->CI->db->where('date(sr.entry_date) >= ',$fromdate);
		}

		if($data['todate']!='')
		{
			list($m,$d,$y) = explode("/",$data['todate']);
			$todate = $y."-".$m."-".$d;
			$this->CI->db->where('date(sr.entry_date) <=',$todate);
		}

		if($data['rating']!='' && $data['rating']!='all')
		{
			$this->CI->db->where('sr.rating_no',$data['rating']);
		}

		if($data['havemail']!='' && $data['havemail']!='all')
		{
			switch ($data['havemail'])
			{
				case 'yes' : 		$this->CI->db->where('sn.emailid != ""'); break;
				case 'no' : 		$this->CI->db->where('sn.emailid == ""'); break;
			}
		}
        
        if ($data['location'] != '' && $data['location'] != 'all') {
            $this->CI->db->where('srl.id',$data['location']);
        }

        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->select("sn.firstname ,sn.lastname ,sn.phone_no,sn.emailid as Email, srl.name AS locationName", false);
		$this->CI->db->select("IF(sr.rating_no = 1 , 'Very Dissatisfied' , IF(sr.rating_no = 2 , 'Dissatisfied' , IF(sr.rating_no = 3 , 'Satisfied' , IF(sr.rating_no = 4 , 'Very Satisfied' , IF(sr.rating_no = 5 , 'Extremely Satisfied' , '' ) ) ) ) ) as rating",false);
		$this->CI->db->select("date_format(entry_date,'%m/%d/%Y') as date",false);
		$this->CI->db->where('sr.is_delete',0);
        // Location
        $this->CI->db->join('sr_locations srl', 'sr.location = srl.id','left');
		$this->CI->db->join('sr_newuser sn', 'sn.id= sr.user_id','left');
		$this->CI->db->order_by("entry_date", "asc");

		$query = $this->CI->db->get('sr_rating sr');
        
		return $query->result_array();
	}
    
    public function locationName($id)
    {
        if ($id == 0) return 'All locations';
        $q = $this->CI->db->get_where('sr_locations',array('id'=>$id));
        return $q->row()->name;
    }
    
    public function getdetail()
	{
        $q = $this->CI->db->get_where('sr_clients',array('id'=>$this->CI->session->userdata['logged_in']['client_id']));
        return $q->row_array();
	}
    
    public function getalldata($id)
    {
        $q = $this->CI->db->get_where('sr_locations',array('id'=>$id));
        return $q->row_array();
    }
    
    public function getPerson()
    {
        $person = $this->CI->db->query("SELECT person_label FROM sr_clients WHERE id = ?",array($this->CI->session->userdata['logged_in']['client_id']));
        $result = $person->row(); 
        $person = ($result) ? $result->person_label : '';
        return $person;
    }
    
    public function update_company($filename)
    {
        if (!empty($_FILES['upload_image']['name'])) {
            if ($this->check_if_root()) {
                $data = array(
                    'name' => $this->CI->input->post('name'),
                    'client_email' => $this->CI->input->post('email'),
                    'terms' => $this->CI->input->post('terms'),
                    'not_found_msg' => $this->CI->input->post('not_found_msg'),
                    'exists_msg' => $this->CI->input->post('exists_msg'),
                    'timeout' => (int)$this->CI->input->post('timeout'),
                    'logo'=>$filename,
                    'last_update'=>date('Y-m-d H:i:s'),
                    'person_label' => $this->CI->input->post('person_label')
                );
            } else {
                $data = array(
                    'name' => $this->CI->input->post('name'),
                    'client_email' => $this->CI->input->post('email'),
                    'terms' => $this->CI->input->post('terms'),
                    'not_found_msg' => $this->CI->input->post('not_found_msg'),
                    'exists_msg' => $this->CI->input->post('exists_msg'),
                    'timeout' => (int)$this->CI->input->post('timeout'),
                    'logo'=>$filename,
                    'last_update'=>date('Y-m-d H:i:s')
                );
            }
        } else {
            if ($this->check_if_root()) {
                $data = array(
                    'name' => $this->CI->input->post('name'),
                    'client_email' => $this->CI->input->post('email'),
                    'terms' => $this->CI->input->post('terms'),
                    'not_found_msg' => $this->CI->input->post('not_found_msg'),
                    'exists_msg' => $this->CI->input->post('exists_msg'),
                    'timeout' => (int)$this->CI->input->post('timeout'),
                    'last_update'=>date('Y-m-d H:i:s'),
                    'person_label' => $this->CI->input->post('person_label')
                );
            } else {
                $data = array(
                    'name' => $this->CI->input->post('name'),
                    'client_email' => $this->CI->input->post('email'),
                    'terms' => $this->CI->input->post('terms'),
                    'not_found_msg' => $this->CI->input->post('not_found_msg'),
                    'exists_msg' => $this->CI->input->post('exists_msg'),
                    'timeout' => (int)$this->CI->input->post('timeout'),
                    'last_update'=>date('Y-m-d H:i:s')
                );
            }
        }

        $this->CI->db->where('id', $this->CI->session->userdata['logged_in']['client_id']);
        $this->CI->db->update('sr_clients', $data);
        
        $this->CI->db->query("UPDATE sr_locations SET terms = ? WHERE client_id = ?",array($this->CI->input->post('terms'),$this->CI->session->userdata['logged_in']['client_id']));
    }
    
    public function update_company_defaultimage($filename)
    {
        if (!empty($_FILES['defaultimage']['name'])) {
            if ($this->check_if_root()) {
                $data = array(
                    'defaultImage' => $filename,
                );
            } else {
                $data = array(
                    'defaultImage' => $filename,
                );
            }
            
             $this->CI->db->where('id', $this->CI->session->userdata['logged_in']['client_id']);
             $this->CI->db->update('sr_clients', $data);
        }
    }
    
    public function social($id = false)
    {
        if ($id) {
            $q = $this->CI->db->query("SELECT * FROM sr_social WHERE location_id = ? AND client_id = ? ORDER BY id ASC",array($id,$this->CI->session->userdata['logged_in']['client_id']));
            if ($q->num_rows() == 0) {
                $q = $this->CI->db->query("SELECT * FROM sr_social WHERE location_id = 0 ORDER BY id ASC",array($id));
            }
            return $q->result_array();
        } else if ($id == 'zero') {
            $q = $this->CI->db->query("SELECT * FROM sr_social WHERE location_id = 0 ORDER BY id ASC");
            return $q->result_array();
        } else {
            $q = $this->CI->db->query("SELECT * FROM sr_social WHERE client_id = ? ORDER BY id ASC",array($this->CI->session->userdata['logged_in']['client_id']));
            return $q->result_array();
        }
    }
    
    public function getLogs($per_page,$starting)
	{
		$this->CI->db->select("id,date,ip,user_id,city,country,state,sessionid",false);
        $this->CI->db->order_by("date", "asc");
		$query = $this->CI->db->get('logs',$per_page,$starting);
		return $query->result_array();
	}
    
    public function getClients($per_page,$starting,$total = false, $uid = false)
	{
	    if ($total) {
	        $this->CI->db->select("*",false);
            $this->CI->db->order_by("id", "asc");
		    $query = $this->CI->db->get('sr_clients');
            return $query->num_rows();
	    }
		$this->CI->db->select("*",false);
        if ($uid) {
            $this->CI->db->where("id = $uid");
        }
        $this->CI->db->order_by("id", "asc");
		$query = $this->CI->db->get('sr_clients',$per_page,$starting);
		return $query->result_array();
	}
    
    public function getLogDetails($per_page,$starting,$hash)
	{
		$this->CI->db->select("date,action,sessionid",false);
        $this->CI->db->where("sessionid = '$hash'");
        $this->CI->db->order_by("date", "asc");
		$query = $this->CI->db->get('logs_actions',$per_page,$starting);
		return $query->result_array();
	}
    
    public function logAction($message)
    {
        $u = $this->CI->session->userdata('logged_in');
        /*if ($u['logintype'] == 'root') {
            return;
        }*/
        $userId = $u['id'];
        $hash = $u['hash'];
        $data = array(
            'user_id' => $userId,
            'date' => time(),
            'action' => $message,
            'sessionid' => $hash,
            'client_id' => $this->CI->session->userdata['logged_in']['client_id']
        );
        
        $this->CI->db->insert('logs_actions', $data);
        return true;
    }
    
    public function gettotallogs()
	{
		$query = $this->CI->db->query("SELECT * FROM logs");
		return $query->num_rows();
	}
    
    public function getLogActions($hash)
	{
		$query = $this->CI->db->query("SELECT * FROM logs WHERE sessionid = ?",array($hash));
		return $query->num_rows();
	}
    
    public function getshares($perpage,$start,$search = false)
    {
        if($search) {
			switch ($search) {
				case 'today' : 		$this->CI->db->where('DATE(sr.entry_date) = CURDATE()'); break;
				case '7' : 			$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)'); break;
				case '30' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)'); break;
 				case '90' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)'); break;
				case '180' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 180 DAY)'); break;
				case '365' : 		$this->CI->db->where('DATE(sr.entry_date) >= DATE_SUB(CURDATE(), INTERVAL 365 DAY)'); break;
			}
		}
        
        $this->CI->db->select('sr.*,user.firstname,user.lastname,user.phone_no,user.emailid,srl.name AS locationName', 'asc');
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
        $this->CI->db->join('sr_newuser user', 'sr.user_id = user.id','left');
        // Location
        $this->CI->db->join('sr_locations srl', 'sr.location = srl.id','left');
        $this->CI->db->order_by('sr.id','desc');
        $query = $this->CI->db->get('sr_socialshare sr',$perpage,$start);
        return $query->result_array();
	}
    
    public function getArchivedRatingList($per_page,$starting)
	{
		$this->CI->db->select("sn.*,(sr.id) as rating_id", 'asc');
		$this->CI->db->select("IF(sr.rating = 2, 'Dissatisfied', IF (sr.rating = 1 , 'Very Dissatisfied', IF (sr.rating_no = 3 , 'Satisfied' , IF(sr.rating_no = 4 , 'Very Satisfied' , IF(sr.rating_no = 5 , 'Extremely Satisfied' , '' ) ) ) ) ) as rating, srl.name AS locationName",false);
		$this->CI->db->where('sr.is_delete',1);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->join('sr_newuser sn', 'sn.id= sr.user_id','left');
        $this->CI->db->join('sr_locations srl', 'sr.location = srl.id','left');
		$query = $this->CI->db->get('sr_rating sr',$per_page,$starting);
		return $query->result_array();
	}
    
    public function getarchivedratingstotal()
	{
		$this->CI->db->select('sn.*,(sr.id) as rating_id', 'asc');
		$this->CI->db->where('sr.is_delete',1);
        $this->CI->db->where('sr.client_id',$this->CI->session->userdata['logged_in']['client_id']);
		$this->CI->db->join('sr_newuser sn', 'sn.id= sr.user_id','left');
		$query = $this->CI->db->get('sr_rating sr');
		return $query->num_rows();
	}
    
    public function getsharesTotal()
    {
        $q = $this->CI->db->query("SELECT * FROM sr_socialshare WHERE client_id = ?",array($this->CI->session->userdata['logged_in']['client_id']));
        return $q->num_rows();
    }
    
    public function getAudioTotal()
    {
        $q = $this->CI->db->query("SELECT * FROM sr_reviews WHERE file LIKE '%.m4a' OR file LIKE '%.3gp' AND client_id = ?",array($this->CI->session->userdata['logged_in']['client_id']));
        return $q->num_rows();
    }
    
    public function getAudioRatings($per_page,$starting)
    {
        $client_id = $this->CI->session->userdata['logged_in']['client_id'];
        $sql = "SELECT srl.name AS locationName, rating.id, audio.file, audio.client_id, rating.rating, user.phone_no, user.firstname, user.lastname, user.emailid FROM sr_reviews audio LEFT JOIN sr_rating rating ON audio.rating_id = rating.id LEFT JOIN sr_locations srl ON rating.location = srl.id LEFT JOIN sr_newuser user ON audio.userId = user.id WHERE audio.client_id = $client_id AND (audio.file LIKE '%.m4a' OR audio.file LIKE '%.3gp') AND rating.is_delete = 0 ORDER BY rating.id DESC LIMIT $starting,$per_page";
        $q = $this->CI->db->query($sql);
        return $q->result_array();
    }
    
    public function getVideoTotal()
    {
        $q = $this->CI->db->query("SELECT * FROM sr_reviews WHERE file LIKE '%.MOV' AND client_id = ?",array($this->CI->session->userdata['logged_in']['client_id']));
        return $q->num_rows();
    }
    
    public function getVideoRatings($per_page,$starting)
    {   
        $client_id = $this->CI->session->userdata['logged_in']['client_id'];
        $sql = "SELECT srl.name AS locationName, rating.id, audio.file, audio.videoId, audio.client_id, rating.rating, user.phone_no, user.firstname, user.lastname, user.emailid FROM sr_reviews audio LEFT JOIN sr_rating rating ON audio.rating_id = rating.id LEFT JOIN sr_locations srl ON rating.location = srl.id LEFT JOIN sr_newuser user ON audio.userId = user.id WHERE audio.client_id = $client_id AND rating.is_delete = 0 AND (audio.file LIKE '%.MOV') OR (audio.file LIKE '%.mp4') ORDER BY rating.id DESC LIMIT $starting,$per_page";
        $q = $this->CI->db->query($sql);
        return $q->result_array();
    }
    
    public function update_social()
    {
        $data = $this->CI->input->post();
        
        foreach ($data['link'] as $k=>$v) {
            $this->CI->db->query("UPDATE sr_social SET link = ? WHERE id = $k AND client_id = ?",array($v,$this->CI->session->userdata['logged_in']['client_id']));
        }
        
        foreach ($data['desc'] as $k=>$v) {
            $this->CI->db->query("UPDATE sr_social SET description = ? WHERE id = $k AND client_id = ?",array($v,$this->CI->session->userdata['logged_in']['client_id']));
        }
        
        foreach ($data['active'] as $k=>$v) {
            $this->CI->db->query("UPDATE sr_social SET active = ? WHERE id = $k AND client_id = ?",array($v,$this->CI->session->userdata['logged_in']['client_id']));
        }
        
        if (!empty($_FILES['pinterest_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '1024';
            $config['max_height'] = '1024';
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $config['remove_spaces'] = true;
    
            $this->CI->load->library('upload');
            $this->CI->upload->initialize($config);
    
            if (!$this->CI->upload->do_upload('pinterest_image')) {
                echo $this->CI->upload->display_errors();
                die();
            } else {
                $data1 = $this->CI->upload->data();
                $filename = base_url('uploads/' . $data1['file_name']);
                $this->CI->db->query("UPDATE sr_social SET logo = ? WHERE name = 'pinterest' AND client_id = ?",array($filename,$this->CI->session->userdata['logged_in']['client_id']));
            }
        }
        
        return true;
    }
    
    public function add_location($edit = false)
    {
        $data = array(
            'name' => $this->CI->input->post('name'),
            'address1' => $this->CI->input->post('address1'),
            'address2' => '0', // $this->CI->input->post('address2')
            'city' => $this->CI->input->post('city'),
            'state' => $this->CI->input->post('state'),
            'zipcode' => $this->CI->input->post('zipcode'),
            'longitude' => $this->CI->input->post('lon'),
            'latitude' => $this->CI->input->post('lat'),
            'search' => $this->CI->input->post('search'),
            'emails_321' => $this->CI->input->post('emails3'),
            'emails_45' => $this->CI->input->post('emails45'),
            'client_id' => $this->CI->session->userdata['logged_in']['client_id']
        );
     
        if ($edit) {
            $this->CI->db->where('id', $edit);
            $this->CI->db->where('client_id',$this->CI->session->userdata['logged_in']['client_id']);
            $this->CI->db->update('sr_locations', $data);
        } else {
            $this->CI->db->insert('sr_locations', $data);
        }
        return true;
    }
    
    public function add_new_location($edit = false)
    {
        
        $data = array(
            'name' => $this->CI->input->post('name'),
            'address1' => $this->CI->input->post('address1'),
            'address2' => '0', // $this->CI->input->post('address2')
            'city' => $this->CI->input->post('city'),
            'state' => $this->CI->input->post('state'),
            'zipcode' => $this->CI->input->post('zipcode'),
            'longitude' => $this->CI->input->post('lon'),
            'latitude' => $this->CI->input->post('lat'),
            'search' => $this->CI->input->post('search'),
            'emails_321' => $this->CI->input->post('emails3'),
            'emails_45' => $this->CI->input->post('emails45'),
            'ex_satisfied_txt' => $this->CI->input->post('ex_satisfied_txt'),
            'very_satisfied_text' => $this->CI->input->post('very_satisfied_text'),
            'satisfied_text' => $this->CI->input->post('satisfied_text'),
            'dissatisfied_text' => $this->CI->input->post('dissatisfied_text'),
            'very_text' => $this->CI->input->post('very_text'),
            'thank_you' => $this->CI->input->post('thank_you'),
            'voiceText' => $this->CI->input->post('voiceText'),
            'videoText' => $this->CI->input->post('videoText'),
            'emails3' => $this->CI->input->post('emails3'),
            'senderEmail' => $this->CI->input->post('senderEmail'),
            '4_5_mail' => $this->CI->input->post('4_5_mail'),
            '3_under_mail' => $this->CI->input->post('3_under_mail'),
            'client_id' => $this->CI->session->userdata['logged_in']['client_id'],
            'ap_button' => $this->CI->input->post('ap_button'),
            'ap_url' => $this->CI->input->post('ap_url'),
            'ap_enabled' => $this->CI->input->post('ap_enabled'),
            'admin_email' => $this->CI->input->post('admin_email')
        );
        
        /** 4 -5 stars **/
        if (!empty($_FILES['email_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '1024';
            $config['max_height'] = '1024';
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $config['remove_spaces'] = true;
    
            $this->CI->load->library('upload');
            $this->CI->upload->initialize($config);
    
            if (!$this->CI->upload->do_upload('email_image')) {
                echo $this->CI->upload->display_errors();
                die();
            } else {
                $data1 = $this->CI->upload->data();
                $filename = base_url('uploads/' . $data1['file_name']);
                $data['email_img'] = $filename;
            }
        }
        
        /** 3 and under stars **/
        if (!empty($_FILES['email_image1']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '1024';
            $config['max_height'] = '1024';
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $config['remove_spaces'] = true;
    
            $this->CI->load->library('upload');
            $this->CI->upload->initialize($config);
    
            if (!$this->CI->upload->do_upload('email_image1')) {
                echo $this->CI->upload->display_errors();
                die();
            } else {
                $data1 = $this->CI->upload->data();
                $filename = base_url('uploads/' . $data1['file_name']);
                $data['email_img1'] = $filename;
            }
        }
        
        $social = $this->CI->input->post();

        if ($edit) {
            $this->CI->db->where('id', $edit);
            $this->CI->db->update('sr_locations', $data);
            
            $check = $this->CI->db->query("SELECT 1 FROM sr_social WHERE location_id = ? AND client_id = ?",array($edit,$this->CI->session->userdata['logged_in']['client_id']));
            if ($check->num_rows() > 0) {
            
                foreach ($social['link'] as $k=>$v) {
                    $this->CI->db->query("UPDATE sr_social SET link = ? WHERE name = '$k' AND location_id = ? AND client_id = ?",array($v,$edit,$this->CI->session->userdata['logged_in']['client_id']));
                }
                
                foreach ($social['desc'] as $k=>$v) {
                    $this->CI->db->query("UPDATE sr_social SET description = ? WHERE name = '$k' AND location_id = ? AND client_id = ?",array($v,$edit,$this->CI->session->userdata['logged_in']['client_id']));
                }
                
                foreach ($social['active'] as $k=>$v) {
                    $this->CI->db->query("UPDATE sr_social SET active = ? WHERE name = '$k' AND location_id = ? AND client_id = ?",array($v,$edit,$this->CI->session->userdata['logged_in']['client_id']));
                }
                
                if (!empty($_FILES['pinterest_image']['name'])) {
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '1000';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '1024';
                    $config['overwrite'] = false;
                    $config['encrypt_name'] = true;
                    $config['remove_spaces'] = true;
            
                    $this->CI->load->library('upload');
                    $this->CI->upload->initialize($config);
            
                    if (!$this->CI->upload->do_upload('pinterest_image')) {
                        echo $this->CI->upload->display_errors();
                        die();
                    } else {
                        $data1 = $this->CI->upload->data();
                        $filename = base_url('uploads/' . $data1['file_name']);
                        $this->CI->db->query("UPDATE sr_social SET logo = ? WHERE name = 'pinterest' AND location_id = ? AND client_id = ?",array($filename,$edit,$this->CI->session->userdata['logged_in']['client_id']));
                    }
                }
            
            } else {
                $final = array();
                foreach ($social['link'] as $k=>$v) {
                    $final[$k]['link'] = $v;
                }
                
                foreach ($social['desc'] as $k=>$v) {
                    $final[$k]['desc'] = $v;
                }
                
                foreach ($social['active'] as $k=>$v) {
                    $final[$k]['active'] = $v;
                }
                
                $final['fb']['icon'] = 'facebook.png';
                $final['twitter']['icon'] = 'twitter.png';
                $final['pinterest']['icon'] = 'pinterest.png';
                $final['yelp']['icon'] = 'yelp.png';
                
                $final['fb']['title'] = 'Facebook';
                $final['twitter']['title'] = 'Twitter';
                $final['pinterest']['title'] = 'Pinterest';
                $final['yelp']['title'] = 'Yelp';
                
                $final['fb']['name'] = 'fb';
                $final['twitter']['name'] = 'twitter';
                $final['pinterest']['name'] = 'pinterest';
                $final['yelp']['name'] = 'yelp';
                
                foreach ($final as $k=>$v) {
                    $name = $v['name'];
                    $link = $v['link'];
                    $desc = isset($v['desc']) ? $v['desc'] : '';
                    $icon = $v['icon'];
                    $active = $v['active'];
                    $title = $v['title'];
                    $cid = $this->CI->session->userdata['logged_in']['client_id'];
                    $this->CI->db->query("INSERT INTO sr_social (name,link,description,active,title,location_id,client_id) VALUES ('$name','$link','$desc','$active','$title','$edit','$cid')");
                }
                
                if (!empty($_FILES['pinterest_image']['name'])) {
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '1000';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '1024';
                    $config['overwrite'] = false;
                    $config['encrypt_name'] = true;
                    $config['remove_spaces'] = true;
            
                    $this->CI->load->library('upload');
                    $this->CI->upload->initialize($config);
            
                    if (!$this->CI->upload->do_upload('pinterest_image')) {
                        echo $this->CI->upload->display_errors();
                        die();
                    } else {
                        $data1 = $this->CI->upload->data();
                        $filename = base_url('uploads/' . $data1['file_name']);
                        $this->CI->db->query("UPDATE sr_social SET logo = ? WHERE name = 'pinterest' AND location_id = ? AND client_id = ?",array($filename,$edit,$this->CI->session->userdata['logged_in']['client_id']));
                    }
                }
            }
            
        } else {
            $this->CI->db->insert('sr_locations', $data);
            $location_id = $this->CI->db->insert_id();
            
            // Make array
            $final = array();
            foreach ($social['link'] as $k=>$v) {
                $final[$k]['link'] = $v;
            }
            
            foreach ($social['desc'] as $k=>$v) {
                $final[$k]['desc'] = $v;
            }
            
            foreach ($social['active'] as $k=>$v) {
                $final[$k]['active'] = $v;
            }
            
            $final['fb']['icon'] = 'facebook.png';
            $final['twitter']['icon'] = 'twitter.png';
            $final['pinterest']['icon'] = 'pinterest.png';
            $final['yelp']['icon'] = 'yelp.png';
            
            $final['fb']['title'] = 'Facebook';
            $final['twitter']['title'] = 'Twitter';
            $final['pinterest']['title'] = 'Pinterest';
            $final['yelp']['title'] = 'Yelp';
            
            $final['fb']['name'] = 'fb';
            $final['twitter']['name'] = 'twitter';
            $final['pinterest']['name'] = 'pinterest';
            $final['yelp']['name'] = 'yelp';
            
            foreach ($final as $k=>$v) {
                $name = $v['name'];
                $link = $v['link'];
                $desc = isset($v['desc']) ? $v['desc'] : '';
                $icon = $v['icon'];
                $active = $v['active'];
                $title = $v['title'];
                $cid = $this->CI->session->userdata['logged_in']['client_id'];
                $this->CI->db->query("INSERT INTO sr_social (name,link,description,active,title,location_id,client_id) VALUES ('$name','$link','$desc','$active','$title','$location_id','$cid')");
            }
            
            if (!empty($_FILES['pinterest_image']['name'])) {
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '1000';
                $config['max_width'] = '1024';
                $config['max_height'] = '1024';
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $config['remove_spaces'] = true;
        
                $this->CI->load->library('upload');
                $this->CI->upload->initialize($config);
        
                if (!$this->CI->upload->do_upload('pinterest_image')) {
                    echo $this->CI->upload->display_errors();
                    die();
                } else {
                    $data1 = $this->CI->upload->data();
                    $filename = base_url('uploads/' . $data1['file_name']);
                    $this->CI->db->query("UPDATE sr_social SET logo = ? WHERE name = 'pinterest' AND location_id = ? AND client_id = ?",array($filename,$location_id,$this->CI->session->userdata['logged_in']['client_id']));
                }
            }
        }
        return true;
    }
    
    public function location_info($id)
    {
        $q = $this->CI->db->query("SELECT * FROM sr_locations WHERE id = ? AND client_id = ?",array($id,$this->CI->session->userdata['logged_in']['client_id']));
        return $q->row_array();
    }
    
    public function update_texts() {
        $data = array(
            'ex_satisfied_txt' => $this->CI->input->post('ex_satisfied_txt'),
            'very_satisfied_text' => $this->CI->input->post('very_satisfied_text'),
            'satisfied_text' => $this->CI->input->post('satisfied_text'),
            'dissatisfied_text' => $this->CI->input->post('dissatisfied_text'),
            'very_text' => $this->CI->input->post('very_text'),
            'thank_you' => $this->CI->input->post('thank_you'),
            'terms' => $this->CI->input->post('terms'),
            'voiceText' => $this->CI->input->post('voiceText'),
            'videoText' => $this->CI->input->post('videoText')
        );
        $this->CI->db->where('id', 1);
	    $this->CI->db->update('sr_company', $data);
    }
    
    public function update_mails() {
        $data = array(
            'emails3' => $this->CI->input->post('emails3'),
            'senderEmail' => $this->CI->input->post('senderEmail'),
            '4_5_mail' => $this->CI->input->post('4_5_mail'),
            '3_under_mail' => $this->CI->input->post('3_under_mail')
        );
        
        /** 4 -5 stars **/
        if (!empty($_FILES['email_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '1024';
            $config['max_height'] = '1024';
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $config['remove_spaces'] = true;
    
            $this->CI->load->library('upload');
            $this->CI->upload->initialize($config);
    
            if (!$this->CI->upload->do_upload('email_image')) {
                echo $this->CI->upload->display_errors();
                die();
            } else {
                $data1 = $this->CI->upload->data();
                $filename = base_url('uploads/' . $data1['file_name']);
                $data['email_img'] = $filename;
            }
        }
        
        /** 3 and under stars **/
        if (!empty($_FILES['email_image1']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '1000';
            $config['max_width'] = '1024';
            $config['max_height'] = '1024';
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $config['remove_spaces'] = true;
    
            $this->CI->load->library('upload');
            $this->CI->upload->initialize($config);
    
            if (!$this->CI->upload->do_upload('email_image1')) {
                echo $this->CI->upload->display_errors();
                die();
            } else {
                $data1 = $this->CI->upload->data();
                $filename = base_url('uploads/' . $data1['file_name']);
                $data['email_img1'] = $filename;
            }
        }
        
        $this->CI->db->where('id', 1);
	    $this->CI->db->update('sr_company', $data);
        return true;
    }
    
    public function locations()
    {
        $client_id = $this->CI->session->userdata['logged_in']['client_id'];
        $q = $this->CI->db->query("SELECT * FROM sr_locations WHERE client_id = $client_id ORDER BY id ASC");
        return $q->result_array();
    }
    
}