<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Support extends CI_Controller {
    
	function __construct()
	{
		parent::__construct();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        
		$this->template->set_layout('baseTemplate');
	}

	public function index()
	{
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->template->build('support',$this->data);
	}
    
	public function insert()
	{
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
		$this->misc->supportSend();
		$this->session->set_flashdata('msg','Message sent successfully!');
		$this->session->set_flashdata('cls','success');
		redirect('support');
	}
}