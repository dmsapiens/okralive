<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends CI_Controller {
	

	function __construct()
	{
		parent::__construct();
        
		$this->data = array();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        
		$this->template->set_layout('baseTemplate');
	}

	public function index()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        /** Total locations **/;
        #$this->data['total'] = $this->db->count_all('sr_locations');

        $q = $this->db->query("SELECT 1 FROM sr_locations WHERE client_id = ?",$this->session->userdata['logged_in']['client_id']);
        $this->data['total'] = $q->num_rows();
        $this->misc->logAction('Viewed Locations Page');
        
        $this->data['locations'] = $this->misc->locations();
        $this->template->build('location',$this->data);		
	}
    
    public function delete($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        if ($id == 1) {
            $this->session->set_flashdata('error', 'You cannot delete first location, you can only edit it!');
            redirect('locations');
        }
        
        $q = $this->db->query("SELECT 1 FROM sr_locations WHERE client_id = ?",$this->session->userdata['logged_in']['client_id']);
        $t = $q->num_rows();
        if ($t == 1) {
            $this->session->set_flashdata('error', 'This is your only location, you cannot delete it!');
            redirect('locations');
        }
        
        $id = (int)$id;
        $this->db->query("DELETE FROM sr_locations WHERE id = ? AND client_id = ?",array($id,$this->session->userdata['logged_in']['client_id']));
        $this->db->query("DELETE FROM sr_social WHERE location_id = ? AND client_id = ?",array($id,$this->session->userdata['logged_in']['client_id']));
        $this->misc->logAction('Removed Location with id ' . $id); 
        $this->session->set_flashdata('msg', 'Location has been deleted successfully!');
        redirect('locations');
    }
    
    public function add()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$this->misc->check_if_root()) {
            $this->misc->logAction('Unauthorized attempt to add new location!!!'); 
            $this->session->set_flashdata('error', 'You are not allowed to add new location!');
            redirect('locations');
        }
        
        $this->data = array();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        $this->data['social'] = $this->misc->social('zero');
        $this->template->build('addnewlocation',$this->data);	
    }
    
    public function edit($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        if (!$this->misc->location_info($id)) {
            redirect('/');
        }
        
        $this->data = array();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        
        $this->data['userlist'] = $this->misc->location_info($id);
        $this->data['companydetail']= $this->misc->getalldata($id);
        $this->data['social'] = $this->misc->social($id);
        $this->data['edit'] = true;
        
        $this->template->build('addnewlocation',$this->data);	
    }
    
    public function insert_location()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $edit = false;
        if ($this->input->post('edit')) {
            $edit = $this->input->post('edit');
        }
        
        $this->misc->add_new_location($edit);
        
        $this->session->set_flashdata('msg','Location has been successfully added!');
        $this->session->set_flashdata('cls','success');
        redirect('locations');
    }

}