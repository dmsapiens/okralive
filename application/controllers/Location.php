<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->template->set_layout('baseTemplate');
	}

	public function index()
	{
	   redirect('/');		
	}
    
    public function add()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->data = array();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        
        $this->template->build('addlocation',$this->data);	
    }
    
    public function edit($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        $this->data = array();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        
        $this->data['userlist'] = $this->misc->location_info($id);
        $this->data['edit'] = true;
        
        $this->template->build('addlocation',$this->data);	
    }
    
    public function insert_location()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $edit = false;
        if ($this->input->post('edit')) {
            $edit = $this->input->post('edit');
        }
        
        $this->session->set_flashdata('msg','New Location has been successfully added!');
        $this->session->set_flashdata('cls','success');
        $this->misc->add_location($edit);
        redirect('settings/#location');
    }
    
    public function delete($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        /** Check number of locations **/
        $q = $this->db->query("SELECT 1 FROM sr_locations WHERE client_id = ?",$this->session->userdata['logged_in']['client_id']);
        if ($q->num_rows() == 1) {
            $this->session->set_flashdata('msg','Location could not be removed since this is the only one!');
            $this->session->set_flashdata('cls','error');
            redirect('settings');
        }
        
        $this->db->query("DELETE FROM sr_locations WHERE id = ? AND client_id = ?",array($id,$this->session->userdata['logged_in']['client_id']));
        
        $this->session->set_flashdata('msg','Location has been successfully removed!');
        $this->session->set_flashdata('cls','success');
        redirect('settings');
    }
 
 }