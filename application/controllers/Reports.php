<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->data = array();
        $this->template->set_layout('baseTemplate');
	}
    
    public function doctors()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $id = (int)$_GET['id'];
        
        if ($id == 0) {
            $id = 'all';
        } else {
            if (!$id) return;
            if (!is_numeric($id)) return;
        }
        
        if ($id == 'all') {
            $q = $this->db->query("SELECT * FROM sr_doctors WHERE client_id = ?",array($this->session->userdata['logged_in']['client_id']));
        } else {
            $q = $this->db->query("SELECT * FROM sr_doctors WHERE FIND_IN_SET($id,location) AND client_id = ?",array($this->session->userdata['logged_in']['client_id']));
        }
        
        $h = '<option value="all">All</option>';
        
        if ($q) {
            $r = $q->result_array();
            foreach ($r as $row) {
                $h .= '<option value="'.$row['id'].'">'.$row['firstname'].' '.$row['lastname'].'</option>';
            }
        }
        echo $h;
        exit;
    }

    public function index()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$this->data[$rating] = $row['counts'];
    	}
        
        $searchdata['timeframe'] = "";
		if($this->input->get_post('timeframe')!='')
		{
			$searchdata['timeframe'] = $this->input->post('timeframe');
		}
		$this->data['timeframe']	=	$searchdata['timeframe'];

		$searchdata['fromdate'] = "";
		if($this->input->get_post('fromdate')!='')
		{
			$searchdata['fromdate'] = $this->input->post('fromdate');
		}
		$this->data['fromdate']	=	$searchdata['fromdate'];

		$searchdata['todate'] = "";
		if($this->input->get_post('todate')!='')
		{
			$searchdata['todate'] = $this->input->post('todate');
		}
		$this->data['todate']	=	$searchdata['todate'];

		$searchdata['rating'] = "";
		if($this->input->get_post('rating')!='')
		{
			$searchdata['rating'] = $this->input->post('rating');
		}
		$this->data['rating']	=	$searchdata['rating'];

		$searchdata['havemail'] = "";
		if($this->input->get_post('havemail')!='')
		{
			$searchdata['havemail'] = $this->input->post('havemail');
		}
		$this->data['havemail']	=	$searchdata['havemail'];

		$searchdata['format'] = "";
		if($this->input->get_post('format')!='')
		{
			$searchdata['format'] = $this->input->post('format');
		}
		$this->data['format']	=	$searchdata['format'];

		if($this->input->get_post('filterterm')=='')
		{
			$filterterm=0;
			$this->session->unset_userdata('searchterm');
		}
        
        
        $searchdata['doctor'] = "";
		if($this->input->get_post('doctor')!='')
		{
			$searchdata['doctor'] = $this->input->post('doctor');
		}
		$this->data['doctor']	=	$searchdata['doctor'];
        if($this->input->get_post('doctor')=='')
		{
			$searchdata['doctor']='';
			$this->session->unset_userdata('doctor');
		}
        
        $searchdata['patient'] = "";
		if($this->input->get_post('patient')!='')
		{
			$searchdata['patient'] = $this->input->post('patient');
		}
		$this->data['patient']	=	$searchdata['patient'];
        if($this->input->get_post('patient')=='')
		{
			$searchdata['patient']='';
			$this->session->unset_userdata('patient');
		}
        
        $searchdata['location'] = "";
		if($this->input->get_post('location')!='')
		{
			$searchdata['location'] = $this->input->post('location');
		}
        $this->data['location']	=	$searchdata['location'];

		$this->session->set_userdata('searchdata', $searchdata);
        
        /** Get locations, move this to misc **/
        $l = $this->db->query("SELECT id,name FROM sr_locations WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['loc'] = $l->result_array();
        
        $l = $this->db->query("SELECT id,firstname,lastname FROM sr_doctors WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['doctors'] = $l->result_array();
        
        $l = $this->db->query("SELECT DISTINCT(phone_no),id,firstname,lastname FROM sr_newuser WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['patients'] = $l->result_array();

		$this->data['total_rows'] = $this->misc->getreportlisttotal($searchdata);
		$this->data['starting_no']		= $this->uri->segment(4);
		$this->data['reportlist']	=	false;
        $this->data['default'] = true;
		$this->template->build('reports',$this->data);
	}
    
    public function showreport()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$this->data[$rating] = $row['counts'];
    	}
        
        $searchdata['timeframe'] = "";
		if($this->input->get_post('timeframe')!='')
		{
			$searchdata['timeframe'] = $this->input->post('timeframe');
		}
		$this->data['timeframe']	=	$searchdata['timeframe'];

		$searchdata['fromdate'] = "";
		if($this->input->get_post('fromdate')!='')
		{
			$searchdata['fromdate'] = $this->input->post('fromdate');
		}
		$this->data['fromdate']	=	$searchdata['fromdate'];

		$searchdata['todate'] = "";
		if($this->input->get_post('todate')!='')
		{
			$searchdata['todate'] = $this->input->post('todate');
		}
		$this->data['todate']	=	$searchdata['todate'];

		$searchdata['rating'] = "";
		if($this->input->get_post('rating')!='')
		{
			$searchdata['rating'] = $this->input->post('rating');
		}
		$this->data['rating']	=	$searchdata['rating'];

		$searchdata['havemail'] = "";
		if($this->input->get_post('havemail')!='')
		{
			$searchdata['havemail'] = $this->input->post('havemail');
		}
		$this->data['havemail']	=	$searchdata['havemail'];

		$searchdata['format'] = "";
		if($this->input->get_post('format')!='')
		{
			$searchdata['format'] = $this->input->post('format');
		}
		$this->data['format']	=	$searchdata['format'];

		if($this->input->get_post('filterterm')=='')
		{
			$filterterm=0;
			$this->session->unset_userdata('searchterm');
		}
        
        $searchdata['location'] = "";
		if($this->input->get_post('location')!='')
		{
			$searchdata['location'] = $this->input->post('location');
		}
        $this->data['location']	=	$searchdata['location'];
        
        $searchdata['doctor'] = "";
		if($this->input->get_post('doctor')!='')
		{
			$searchdata['doctor'] = $this->input->post('doctor');
		}
		$this->data['doctor']	=	$searchdata['doctor'];
        if($this->input->get_post('doctor')=='')
		{
			$searchdata['doctor']='';
			$this->session->unset_userdata('doctor');
		}
        
        $searchdata['patient'] = "";
		if($this->input->get_post('patient')!='')
		{
			$searchdata['patient'] = $this->input->post('patient');
		}
		$this->data['patient']	=	$searchdata['patient'];
        if($this->input->get_post('patient')=='')
		{
			$searchdata['patient']='';
			$this->session->unset_userdata('patient');
		}

		$this->session->set_userdata('searchdata', $searchdata);
        
        /** Get locations, move this to misc **/
        $l = $this->db->query("SELECT id,name FROM sr_locations ORDER BY id ASC");
        $this->data['loc'] = $l->result_array();
        
        $l = $this->db->query("SELECT id,firstname,lastname FROM sr_doctors WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['doctors'] = $l->result_array();
        
        $l = $this->db->query("SELECT DISTINCT(phone_no),id,firstname,lastname FROM sr_newuser WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['patients'] = $l->result_array();

		$this->data['total_rows'] = $this->misc->getreportlisttotal($searchdata);
		$this->data['starting_no']		= $this->uri->segment(4);
		$this->data['reportlist']	=	$this->misc->getreportlist($searchdata);
        $this->data['default'] = false;
		$this->template->build('reports',$this->data);
    }

	public function exportXls()
	{
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
       
		$this->load->library("excel");
		$this->excel->setActiveSheetIndex(0);
		$searchdata	=	$this->session->userdata('searchdata');
		$reportdata['reportlist']	=	$this->misc->getreportlistXls($searchdata);
		$this->excel->stream('report_'.date('m_d_Y').'.xls', $reportdata,$searchdata);
	}

	function genratepdf()
	{
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
		$this->load->helper('pdf_helper');
		$searchdata	=	$this->session->userdata('searchdata');
		$this->data['reportlist']	=	$this->misc->getreportlist($searchdata);
        $this->data['search'] = $searchdata;
		$this->load->view('pdfreport', $this->data);
	}

	public function genratecsv()
	{
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
    	$filename = "_" . date('m_d_Y') . ".csv";
    	$searchdata	=	$this->session->userdata('searchdata');
    	$result		=	$this->misc->getreportlistXls($searchdata);
    
    	$attachment = false;
    	$headers = true;
        header( 'Content-Type: text/csv;charset=utf-8' );
        header( 'Content-Disposition: attachment;filename=report'.$filename);
        $fp = fopen('php://output', 'w');
    
    	foreach($result as $row)
    	{
    		$title=array_keys($row);
    	}
        
    	fputcsv($fp, $title);
    
    	foreach($result as $row)
    	{
    		fputcsv($fp, $row);
        }
    
        fclose($fp);
    	exit;
    }
}