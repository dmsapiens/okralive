<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usermanagement extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();

        $this->data = array();
        $this->data = array(
            'rating1' => 0,
            'rating2' => 0,
            'rating3' => 0,
            'rating4' => 0,
            'rating5' => 0);
            
        $ratinglist = $this->misc->getratinglist();
        foreach ($ratinglist as $row) {
            $rating = 'rating' . $row['rating_no'];
            $this->data[$rating] = $row['counts'];
        }

        $this->template->set_layout('baseTemplate');
    }

    public function index()
    {
        
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if ($this->input->get_post('filterterm') == "") {
            $filterterm = 0;
        }

        $searchterm = $this->users->searchterm_handler($this->input->get_post('selecttype', true));
        if ($searchterm != '')
            $filterterm = 1;

        if (isset($filterterm) && $filterterm == 1) {
            $config['suffix'] = '/filter?filterterm=' . $filterterm;
            $config['first_url'] = site_url('/usermanagement/index/filter?filterterm=' . $filterterm);
        }

        $data['searchterm'] = $searchterm;

        $this->load->library('pagination');
        $config['total_rows'] = $this->users->getuserlisttotal($data);
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 7;
        $config['base_url'] = site_url('/usermanagement/index');
        $config['full_tag_open'] = '<nav><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = true;
        $config['last_link'] = false;

        $this->pagination->initialize($config);

        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['per_page'] = $config['per_page'];
        $this->data['starting_no'] = $this->uri->segment(3);
        $this->data['searchterm'] = $searchterm;
        $this->data['userlist'] = $this->users->getuserlist($data, $config['per_page'],$this->data['starting_no']);
        
        $cnt = $this->data['usercount'] = $this->users->getusercount();

        $add1 = 0;
        foreach ($cnt as $row) {
            $userno = $row['usertype'];
            $add = $this->data['total'][$userno] = $row['counts'];
            $add1 = $add + $add1;
        }

        $this->data['total']['totalusers'] = $add1;
        $this->template->build('users', $this->data);
    }
    
    public function add()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$this->misc->check_if_root()) {
            $this->misc->logAction('Unauthorized Attempt to add new user!!!');
            redirect('/usermanagement');
        }
        
        $this->data['todo'] = 'Insert';
        $this->template->build('users_edit', $this->data);
    }

    public function edit($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->data['todo'] = 'Update';

        if (is_numeric($this->uri->segment(3))) {
            $this->data['userlist'] = $this->users->get_user($id);
        } else {
            redirect('usermanagement');
        }

        $this->template->build('users_edit', $this->data);
    }
    
    public function view($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (is_numeric($this->uri->segment(3))) {
            $this->data['userlist'] = $this->users->get_userview($id);
        } else {
            redirect('usermanagement');
        }
        $this->template->build('view_profile', $this->data);
    }

    public function insert_user()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $todo = $this->input->post('todo');
        $this->data['todo'] = $todo;
        #$file = $_FILES['upload_image']['name'];
        $filename = false;
        $this->data['userlist']['firstname'] = $this->input->post('firstname');
        $this->data['userlist']['lastname'] = $this->input->post('lastname');
        $this->data['userlist']['phoneno'] = $this->input->post('phoneno');
        $mail = $this->input->post('email');
        $this->data['userlist']['email'] = $this->input->post('email');
        $this->data['userlist']['note'] = $this->input->post('note');
        $this->data['userlist']['usertype'] = $this->input->post('usertype');
        $this->data['userlist']['password'] = $this->input->post('password');
        $this->form_validation->set_rules('email', 'Email','trim|required|xss_clean|valid_email|callback_check_email');

        if ($this->form_validation->run() == false) {
            if ($todo == 'Insert') {
                $this->template->build("users_edit", $this->data);
            } else 
                if ($todo == 'Update') {
                    $this->data['userlist']['id'] = $this->input->post('cid');
                    $this->template->build("users_edit", $this->data);
                }
        } else {
            if ($todo == 'Insert') {
                /*if ($file != null) {
                    $upload = $this->do_upload();
                    $filename = $upload['upload_data']['file_name'];
                } else {
                    $filename = false;
                }*/
                $this->users->insert_user($filename);
                $this->session->set_flashdata('msg', 'User added successfully!');
                $this->session->set_flashdata('cls', 'success');
            } else
                if ($todo == 'Update') {
                    /*if ($file != null) {
                        $upload = $this->do_upload();
                        $filename = $upload['upload_data']['file_name'];
                    } else {
                        $filename = false;
                    }*/
                    $this->session->set_flashdata('msg', 'User updated successfully!');
                    $this->session->set_flashdata('cls', 'success');
                    $this->users->update_user($filename);
                    
                    $sess_array = array();
                    
                    $image = $this->db->query("SELECT * FROM sr_admincontrol WHERE id = ?",array($this->session->userdata['logged_in']['id']));
                    $u = $this->session->userdata('logged_in');
                    $hash = $u['hash'];
                    $logo = $u['logo'];
                    $company = $u['company'];
                    $cid = $u['client_id'];
                    $sess_array = array(
                        'id' => $this->session->userdata['logged_in']['id'],
                        'firstname' => $image->row()->firstname,
                        'lastname' => $image->row()->lastname,
                        'email' => $image->row()->email,
                        'logintype' => $image->row()->usertype,
                        'upload_image' => $image->row()->upload_image,
                        'hash' => $hash,
                        'logo' => $logo,
                        'company' => $company,
                        'client_id' => $cid
                    );
                    $this->misc->logAction('Updated User: ' . $image->row()->email);
                    $this->session->set_userdata('logged_in', $sess_array);
                    
                }
            redirect('usermanagement');
        }

    }

    public function do_upload()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';
        $config['max_size'] = '1000';
        $config['max_width'] = '1024';
        $config['max_height'] = '1024';
        $config['overwrite'] = true;
        $config['encrypt_name'] = true;
        $config['remove_spaces'] = true;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('upload_image')) {
            echo $this->upload->display_errors();
        } else {
            return array('upload_data' => $this->upload->data());
        }
    }

    public function check_email()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $email = $this->input->post('email');
        $todo = $this->input->post('todo');
        $cid = $this->input->post('cid');

        if ($todo == 'Insert') {
            $res = $this->db->get_where('sr_admincontrol', array('email' => $email,'is_delete' => 0));
        } else
            if ($todo == 'Update') {
                $res = $this->db->query('select * from sr_admincontrol where email=\'' . $email .'\' and id != ' . $cid . ' and is_delete=0');
            }
        if ($res->num_rows() > 0) {
            $this->form_validation->set_message('check_email', 'Email Already Exist');
            return false;
        }
        return true;
    }

    public function remove_user($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->users->remove_user($id);
        $this->session->set_flashdata('msg', 'User deleted successfully!');
        $this->session->set_flashdata('cls', 'success');
        $this->misc->logAction('Removed User with id '.$id);
        redirect('usermanagement');
    }
}