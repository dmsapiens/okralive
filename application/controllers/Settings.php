<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	

	function __construct()
	{
		parent::__construct();
        
		$this->data = array();
		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
        
		$this->template->set_layout('baseTemplate');
	}

	public function index()
	{
	   if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
	   $this->data['companydetail']= $this->misc->getdetail();
       $this->data['social'] = $this->misc->social();
       $this->data['locations'] = $this->misc->locations();
	   $this->template->build('settings',$this->data);		
	}
    
    public function update_social()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->session->set_flashdata('msg','Social Networks are successfully Saved!');
        $this->session->set_flashdata('cls','success');
        $this->misc->update_social();
        redirect('settings');
    }
    
    public function update_texts()
	{
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->session->set_flashdata('msg','Text are successfully Saved!');
        $this->session->set_flashdata('cls','success');
        $this->misc->update_texts();
        redirect('settings');
	}
    
    public function update_mails()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->session->set_flashdata('msg','E-mails are successfully Saved!');
        $this->session->set_flashdata('cls','success');
        $this->misc->update_mails();
        redirect('settings/#emails');
    }
	
	public function update_company()
	{
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
		$file = $_FILES['upload_image']['name'];
		$this->data['userlist']['note']= $this->input->post('note');
		$this->data['userlist']['usertype']= $this->input->post('usertype');
		$this->data['userlist']['email']= $this->input->post('email');
        $this->data['userlist']['not_found_msg']= $this->input->post('not_found_msg');
        $this->data['userlist']['exists_msg']= $this->input->post('exists_msg');
        $this->data['userlist']['timeout']= $this->input->post('timeout');
        if ($this->misc->check_if_root()) {
            $this->data['userlist']['person_label'] = $this->input->post('person_label');
        }
        
        $defaultImage = false;
        if ($this->misc->check_if_root()) {
            $defaultImage = $_FILES['defaultimage']['name'];
        }
        

		if($file!=null)
		{
			$im = $this->do_upload();
            $filename = $im['upload_data']['file_name'];
		} else {
		  $filename = false;
		}
        
        if ($defaultImage) {
            if ($this->input->post('person_label') == '') {
                $this->session->set_flashdata('msg','You need to enter professionals label');
                $this->session->set_flashdata('cls','error');
                redirect('settings');
            } else {
                $dImage = $this->do_upload(true);
                $dFilename = $dImage['upload_data']['file_name'];
                $this->misc->update_company_defaultimage($dFilename);
            }
        } else {
            if ($this->input->post('person_label') != '') {
                $this->session->set_flashdata('msg','You need to upload default professionals image');
                $this->session->set_flashdata('cls','error');
                redirect('settings');
            }
        }
        
        $this->misc->update_company($filename);
		$this->session->set_flashdata('msg','Settings successfully Saved!');
		$this->session->set_flashdata('cls','success');

        redirect('settings');
	}
	
	private function do_upload($defaultImage = false)
	{
	   
        if ($defaultImage) {
            $config['upload_path'] = './uploads/';
    		$config['allowed_types'] = 'jpg|png';
    		$config['max_size'] = '3500';
    		$config['max_width']  = '2000';
    		$config['max_height']  = '2000';
    		$config['overwrite'] = TRUE;
    		$config['encrypt_name'] = TRUE;
    		$config['remove_spaces'] = TRUE;
    
    		$this->load->library('upload');
    		$this->upload->initialize($config);
    		
    		if ( ! $this->upload->do_upload('defaultimage')) {
    			  echo 'Image upload: ' . $this->upload->display_errors();
                  exit;
    		} else {
    		      $upload_data = $this->upload->data();
                  $this->load->library('image_lib');
                  $image_config['image_library'] = 'gd2';
                  $image_config['source_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                  $image_config['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                  $image_config['quality'] = "100%";
                  $image_config['maintain_ratio'] = FALSE;
                  $image_config['x_axis'] = $this->input->post('x');
                  $image_config['y_axis'] = $this->input->post('y');
                  $image_config['width'] = $this->input->post('w');
                  $image_config['height'] = $this->input->post('h');
                  $this->image_lib->initialize($image_config);
                
                  if (!$this->image_lib->crop()) {
                        $this->session->set_flashdata('msg','Error in cropping image, please try again.');
                        $this->session->set_flashdata('cls','error');
                        redirect('settings');
                  } else {
                        return array('upload_data' => $this->upload->data());
                  }
    		}
        }
       
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '1000';
		$config['max_width']  = '1024';
		$config['max_height']  = '1024';
		$config['overwrite'] = TRUE;
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;

		$this->load->library('upload');
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload('upload_image'))
		{
			echo $this->upload->display_errors();
		}
		else
		{
			return array('upload_data' => $this->upload->data());
		}
	}
}