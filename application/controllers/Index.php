<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	   if ($this->session->userdata('logged_in')) {
	       redirect('dashboard');
	   } else {
	       $this->load->view('login');
        }
	}
    
    public function login()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
	    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {
            if ($this->misc->check_if_root()) {
                redirect('choose-account');
                exit;
            }
            redirect('dashboard');
        }
    }
    
    public function logout()
    {
        $this->misc->logAction('Logged out from the system');
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('/');
    }
    
    public function forgot()
    {
        $messages = array();
        $this->form_validation->set_rules('email1', 'Email', 'trim|required|xss_clean|callback_check_email');

        if($this->form_validation->run() == FALSE) {
            
        } else {
            $messages['success'] = 'Check your e-mail for further instructions';
        }
        
        $this->load->view('login',$messages);
    }
    
    public function reset($id = false)
    {
        if (!$id) {
            redirect('/');
        }
        
        $messages = array();
        if ($this->misc->check_password_request($id)) {
            $messages['success'] = 'New password has been sent';
        } else {
            $messages['error'] = 'Wrong request link or password already changed';
        }
        
        $this->load->view('login',$messages);
    }
    
    public function check_email()
    {
        if ($this->misc->check_email($this->input->post('email1'))) {
            return true;
        }
        $this->form_validation->set_message('check_email', 'Invalid e-mail');
        return false;
    }
    
    public function check_database($password)
    {
        $email = $this->input->post('email');
        $result = $this->misc->login($email, $password);
        
        if ($result) {
            $sess_array = array();
            $date = time();
			foreach($result as $row) {
                $sess_array = array(
                    'id' => $row['id'],
                    'firstname' => $row['firstname'],
                    'lastname' => $row['lastname'],
                    'email' => $row['email'],
                    'upload_image' => $row['upload_image'],
                    'logintype' => $row['usertype'],
                    'hash' => md5($date),
                    'logo' => $row['logo'],
                    'company' => $row['name'],
                    'client_id' => $row['client_id']
                );
                $this->session->set_userdata('logged_in', $sess_array);
                
                $ip = $_SERVER['REMOTE_ADDR'];
                $user_id = $row['id'];
                $session_id = md5($date);
                
                $this->load->library('geoipc');
                $this->geoipc->InfoIP();
                $city = $this->geoipc->result_city(); 
                $country_name = $this->geoipc->result_country_name();
                $region_name = $this->geoipc->result_region_name();
                $cid = $row['client_id'];
                
                $this->misc->logAction('Logged into the system');
                
                $this->db->query("INSERT INTO logs (user_id,ip,date,country,city,state,sessionid,client_id) VALUES ('$user_id','$ip','$date','$country_name','$city','$region_name','$session_id','$cid')");
			}
            return true;
        }
        
        $this->form_validation->set_message('check_database', 'Invalid Email or Password');
        return false;
    }
}