<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();

        $this->data = array();
        $this->data = array(
            'rating1' => 0,
            'rating2' => 0,
            'rating3' => 0,
            'rating4' => 0,
            'rating5' => 0);
            
        $ratinglist = $this->misc->getratinglist();
        foreach ($ratinglist as $row) {
            $rating = 'rating' . $row['rating_no'];
            $this->data[$rating] = $row['counts'];
        }

        $this->template->set_layout('baseTemplate');
	}
    
    public function view()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!is_numeric($this->uri->segment(3))) {
            redirect('staff');
        }
        
        $id = (int)$this->uri->segment(3);
        
        $data = array();

        $data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$data[$rating] = $row['counts'];
    	}
        
        $data['satisfiedlist'] = $this->misc->getDoctorReviews(array('5','4','3','2','1'),5,0,false,false,false,$id);
        #$data['total_satisfied'] = $this->misc->getDoctorReviews(array('5','4','3','2','1'));

        $this->template->build('doctor_reviews',$data);
    }
    
    public function index()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->load->library('pagination');
        $config['total_rows'] = $this->misc->gettotaldoctors();
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 7;
        $config['base_url'] = site_url('/staff/index');
        $config['full_tag_open'] = '<nav><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = true;
        $config['last_link'] = false;

        $this->pagination->initialize($config);

        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['per_page'] = $config['per_page'];
        $this->data['starting_no'] = $this->uri->segment(3);
        $this->data['userlist'] = $this->misc->getdoctorlist($config['per_page'],$this->data['starting_no']);

        $this->data['total']['totalusers'] = $config['total_rows'];
        $this->template->build('doctors',$this->data);
    }
    
    public function add()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$this->misc->check_if_root()) {
            $this->misc->logAction('Unauthorized attempt to add new location!!!'); 
            $this->session->set_flashdata('error', 'You are not allowed to add new location!');
            redirect('locations');
        }
        
        $this->data['todo'] = 'Insert';
        $this->data['addTxt'] = 'Add new '.$this->misc->getPerson();

        $locations = $this->db->query("SELECT id,name FROM sr_locations WHERE client_id = ?", array($this->session->userdata['logged_in']['client_id']));
        $this->data['locations'] = $locations->result_array();
        
        $this->template->build('doctors_edit', $this->data);
    }
    
    public function remove($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->misc->removedoctor($id);
        $this->session->set_flashdata('msg', 'Physician removed successfully!');
        $this->session->set_flashdata('cls', 'success');
        $this->misc->logAction('Removed Physician with id '.$id);
        redirect('staff');
    }
    
    public function edit($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->data['todo'] = 'Update';
        $this->data['addTxt'] = 'Edit '.$this->misc->getPerson();
        $locations = $this->db->query("SELECT id,name FROM sr_locations WHERE client_id = ?", array($this->session->userdata['logged_in']['client_id']));
        $this->data['locations'] = $locations->result_array();

        if (is_numeric($this->uri->segment(3))) {
            $this->data['userlist'] = $this->misc->get_doctor($id);
        } else {
            redirect('staff');
        }

        $this->template->build('doctors_edit', $this->data);
    }
    
    public function save()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$this->misc->check_if_root()) {
            $this->misc->logAction('Unauthorized attempt to add new location!!!'); 
            $this->session->set_flashdata('error', 'You are not allowed to add new location!');
            redirect('locations');
        }
        
        $defaultImage = $_FILES['image']['name'];
        
        $todo = $this->input->post('todo');
        $this->data['todo'] = $todo;
        $this->data['userlist']['firstname'] = $this->input->post('firstname');
        $this->data['userlist']['lastname'] = $this->input->post('lastname');
        $this->data['userlist']['phone'] = $this->input->post('phone');
        $this->data['userlist']['email'] = $this->input->post('email');
        $this->data['userlist']['usertype'] = $this->input->post('usertype');
        $this->data['userlist']['degree'] = $this->input->post('degree');
        $this->data['userlist']['address'] = $this->input->post('address');
        #$this->form_validation->set_rules('email', 'Email','trim|required|xss_clean|valid_email|callback_check_email');
        
        $l = $this->input->post('usertype');
        if (!$l) {
            $this->session->set_flashdata('error', 'You need to select at least one location!');
            if ($todo == 'Update') {
                redirect('staff/edit/' . $this->input->post('cid'));
            } else {
                redirect('staff/add');
            }
        }

        if ($todo == 'Insert') {
            if ($defaultImage != null) {
                $dImage = $this->do_upload(true);
                $dFilename = $dImage['upload_data']['file_name'];
            } else {
                $dFilename = false;
            }
            $this->misc->insert_doctor($dFilename);
            $this->session->set_flashdata('msg', 'Physician added successfully!');
            $this->session->set_flashdata('cls', 'success');
        } else {
            if ($todo == 'Update') {
                if ($defaultImage != null) {
                    $dImage = $this->do_upload(true);
                    $dFilename = $dImage['upload_data']['file_name'];
                }  else {
                    $dFilename = false;
                }
                $this->misc->update_doctor($dFilename);
                $this->session->set_flashdata('msg', 'Physician updated successfully!');
                $this->session->set_flashdata('cls', 'success');
            }
        }
        
        redirect('staff');
    }
    
    public function check_email()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $email = $this->input->post('email');
        $todo = $this->input->post('todo');
        $cid = $this->input->post('cid');

        if ($todo == 'Insert') {
            $res = $this->db->get_where('sr_doctors', array('email' => $email));
        } else
            if ($todo == 'Update') {
                $res = $this->db->query('select * from sr_doctors where email=\'' . $email .'\' and id != ' . $cid);
            }
        if ($res->num_rows() > 0) {
            $this->form_validation->set_message('check_email', 'Email Already Exist');
            return false;
        }
        return true;
    }
    
    private function do_upload($defaultImage = false)
	{
        if ($defaultImage) {
            $config['upload_path'] = './uploads/';
    		$config['allowed_types'] = 'jpg|png';
    		$config['max_size'] = '3500';
    		$config['max_width']  = '2000';
    		$config['max_height']  = '2000';
    		$config['overwrite'] = TRUE;
    		$config['encrypt_name'] = TRUE;
    		$config['remove_spaces'] = TRUE;
    
    		$this->load->library('upload');
    		$this->upload->initialize($config);
    		
    		if ( ! $this->upload->do_upload('image')) {
    			  echo 'Image upload: ' . $this->upload->display_errors();
                  exit;
    		} else {
    		      $upload_data = $this->upload->data();
                  $this->load->library('image_lib');
                  $image_config['image_library'] = 'gd2';
                  $image_config['source_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                  $image_config['new_image'] = $upload_data["file_path"] . $upload_data["file_name"];
                  $image_config['quality'] = "100%";
                  $image_config['maintain_ratio'] = FALSE;
                  $image_config['x_axis'] = $this->input->post('x');
                  $image_config['y_axis'] = $this->input->post('y');
                  $image_config['width'] = $this->input->post('w');
                  $image_config['height'] = $this->input->post('h');
                  $this->image_lib->initialize($image_config);
                
                  if (!$this->image_lib->crop()) {
                        $this->session->set_flashdata('msg','Error in cropping image, please try again.');
                        $this->session->set_flashdata('cls','error');
                        redirect('staff');
                  } else {
                        return array('upload_data' => $this->upload->data());
                  }
    		}
        }
	}
}