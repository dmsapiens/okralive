<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Allsatisfied extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
        $this->template->set_layout('baseTemplate');
	}
    
    public function index()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }

        $data = array();
        
        $data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$data[$rating] = $row['counts'];
    	}
        
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        if ($this->input->get_post('filterduration') != '') {
            $data['satisfiedlist'] = $this->misc->getsatisfiedlist(array('5','4'),20,$page,false,$this->input->get_post('filterduration'),true);
            $data['searchterm'] = $this->input->get_post('filterduration');
        } else {
            $data['satisfiedlist'] = $this->misc->getsatisfiedlist(array('5','4'),20,$page,false,false,true);
        }
        
        $data['total_satisfied'] = $this->misc->getsatisfiedlisttotal(array('5','4'));
        
        $this->load->library('pagination');
        $config['total_rows'] = $data['total_satisfied'];
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;
		$config['num_links'] = round($data['total_satisfied'] / 20);
		$config['base_url'] = site_url('allsatisfied/index');
		$config['full_tag_open'] = '<nav><ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = true;
		$config['last_link'] = false;
		$this->pagination->initialize($config);
        
        $data['pagination']	= $this->pagination->create_links();
        $this->template->build('allsatisfied',$data);
    }
    
    public function archivereview($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        $data = array();
        
        if ($this->misc->archiveReview($id)) {
            $this->session->set_flashdata('success', 'Review has been archived');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong, please check the link and try again');
        }
        
        redirect('allsatisfied');
    }
 
}