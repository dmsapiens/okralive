<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->template->set_layout('baseTemplate');
		$this->data = array();

		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);

		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
	}

	public function index()
	{
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->load->library('pagination');
		$config['total_rows'] = $this->misc->getVideoTotal();
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;
		$config['num_links'] = 7;
		$config['base_url'] = site_url('video/index');
		$config['full_tag_open'] = '<nav><ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = true;
		$config['last_link'] = FALSE;

		$this->pagination->initialize($config);
		$this->data['pagination']	= $this->pagination->create_links();
		$this->data['per_page']		= $config['per_page'];
		$this->data['starting_no']		=  ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$this->data['archivedratinglist']=$this->misc->getVideoRatings($config['per_page'],$this->data['starting_no']);

		$this->template->build('video',$this->data);
	}
    
    public function archivereview($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('/');
        }
        
        if ($this->misc->archiveReview($id)) {
            $this->session->set_flashdata('success', 'Review has been archived');
        } else {
            $this->session->set_flashdata('error', 'Something went wrong, please check the link and try again');
        }
        
        redirect('video');
    }

}