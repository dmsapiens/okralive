<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Root extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->template->set_layout('baseTemplate');
		$this->data = array();

		$this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);

		$ratinglist=$this->misc->getratinglist();
		foreach($ratinglist as $row)
		{
			$rating='rating'.$row['rating_no'];
			$this->data[$rating]=$row['counts'];
		}
	}
    
    public function login($user_id=false,$client_id=false)
    {
        redirect('/');
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$this->misc->check_if_root()) {
            $this->misc->logAction('Unauthorized attempt to access root page!!!');
            redirect('/');
        }
        
        if ($user_id && $client_id) {
            $uid = (int)$user_id;
            $cid = (int)$client_id;
        } else {
            $this->misc->logAction('Unauthorized attempt to access root page!!!');
            redirect('/');
        }
        
        if (!is_numeric($user_id) || !is_numeric($client_id)) {
            $this->misc->logAction('Unauthorized attempt to access root page!!!');
            redirect('/');
        }
        
        $result = $this->misc->loginById($uid,$cid);
        
        if (!$result) {
            $this->session->set_flashdata('error', 'Could not log you in at the moment, please try again later');
            redirect('root');
        }
        
        $sess_array = array();
        $date = time();
        
		foreach($result as $row) {
            $sess_array = array(
                'id' => $row['aid'],
                'firstname' => $row['firstname'],
                'lastname' => $row['lastname'],
                'email' => $row['email'],
                'logintype' => 'root',
                'hash' => md5($date),
                'logo' => $row['logo'],
                'company' => $row['name'],
                'client_id' => $row['id']
            );
            $this->session->set_userdata('logged_in', $sess_array);
            $this->session->set_userdata('switched',1);
            redirect('/');
        }
    }

	public function index()
	{
	    redirect('/');
	    if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$this->misc->check_if_root()) {
            $this->misc->logAction('Unauthorized attempt to access root page!!!');
            redirect('/');
        }
        
        $this->load->library('pagination');
		$config['total_rows'] = $this->misc->getClients(false,false,true);
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;
		$config['num_links'] = 7;
		$config['base_url'] = site_url('root/index');
		$config['full_tag_open'] = '<nav><ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a><b>';
		$config['cur_tag_close'] = '</b></a></li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['first_link'] = true;
		$config['last_link'] = FALSE;

		$this->pagination->initialize($config);
		$this->data['pagination']	= $this->pagination->create_links();
		$this->data['per_page']		= $config['per_page'];
		$this->data['starting_no']		= $this->uri->segment(3);
		$this->data['archivedratinglist']=$this->misc->getClients($config['per_page'],$this->data['starting_no']);

		$this->template->build('root',$this->data);
	}

}