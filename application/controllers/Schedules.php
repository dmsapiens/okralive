<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedules extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->data = array();
        $this->template->set_layout('baseTemplate');
	}

    public function index()
    {
        
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$this->data[$rating] = $row['counts'];
    	}
        
        $this->data['list'] = $this->misc->scheduleList();
        
        /** Get locations, move this to misc **/
        $l = $this->db->query("SELECT id,name FROM sr_locations WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['loc'] = $l->result_array();
        
        $l = $this->db->query("SELECT id,firstname,lastname FROM sr_doctors WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['doctors'] = $l->result_array();
        
		$this->template->build('schedule',$this->data);
	}
    
    public function save()
    {
        
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        $time_zone_name = timezone_name_from_abbr("",-$this->input->post('timezoneoffset') * 60, $this->input->post('timezonedst'));
        
        $data = array(
            'user_id' => $this->session->userdata['logged_in']['id'],
            'cron' => $this->input->post('minute') . ' ' . $this->input->post('hour') . ' * * ' . $this->input->post('day'),
            'fileFormat' => $this->input->post('format'),
            'rating' => $this->input->post('rating'),
            'email' => $this->input->post('email'),
            'run' => time(),
            'location' => $this->input->post('location'),
            'timezone' => $time_zone_name,
            'client_id' => $this->session->userdata['logged_in']['client_id'],
            'doctor_id' => $this->input->post('doctor')
        );
        
        if ($this->input->post('id')) {
            $this->db->where('id', $this->input->post('id'));
            $this->db->where('client_id',$this->session->userdata['logged_in']['client_id']);
            $this->db->update('sr_schedule', $data);
        } else {
            $this->db->insert('sr_schedule',$data);
        }
        
        $this->session->set_flashdata('msg','Schedule has been saved!');
        $this->session->set_flashdata('cls','success');
        
        redirect('schedules');
    }
    
    public function edit($id)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('schedules');
        }
        
        $this->data = array('rating1'=> 0,'rating2'=> 0,'rating3'=> 0,'rating4'=> 0,'rating5'=> 0);
        
        $ratinglist = $this->misc->getratinglist();
        foreach($ratinglist as $row) {
            $rating = 'rating'.$row['rating_no'];
    		$this->data[$rating] = $row['counts'];
    	}
        
        /** Get locations, move this to misc **/
        $l = $this->db->query("SELECT id,name FROM sr_locations WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['loc'] = $l->result_array();
        
        $l = $this->db->query("SELECT id,firstname,lastname FROM sr_doctors WHERE client_id = ? ORDER BY id ASC",array($this->session->userdata['logged_in']['client_id']));
        $this->data['doctors'] = $l->result_array();
        
        $this->data['info'] = $this->misc->scheduleInfo($id);
        $this->data['list'] = $this->misc->scheduleList();
        
		$this->template->build('schedule',$this->data);
    }
    
    public function remove($id = false)
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('/');
        }
        
        if (!$id) {
            redirect('schedules');
        }
        
        $this->db->query("DELETE FROM sr_schedule WHERE id = ? AND client_id = ?",array($id,$this->session->userdata['logged_in']['client_id']));
        
        $this->session->set_flashdata('msg', 'Schedule has been removed successfully!');
        $this->session->set_flashdata('cls', 'success');
        redirect('schedules');
    }
    
}