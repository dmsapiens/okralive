<?php
Header('Access-Control-Allow-Origin: *');

$servername = 'localhost';
$username = 'oneqrepu_ifwh';
$password = '5nMwhazEQZ47';
$db = 'oneqrepu_ifwh';

$con = mysqli_connect($servername, $username, $password,$db);

function func_query_first($sql){
    global $con;
    $result = mysqli_query($con,$sql);
	if(!empty($result)){
	   return $row = mysqli_fetch_array($result,MYSQL_ASSOC);
	}else{
		return $row = false;
	}
}

function func_query($sql) {
    global $con;
    $myArray = array();
	$result = mysqli_query($con,$sql);
	if(!empty($result)){
		while($row = mysqli_fetch_array($result, MYSQL_ASSOC)){
			$myArray[] = $row;
		}
		return $myArray;
	}
	else
	{
		return false;
	}
}

if (isset($_GET['location']) && $_GET['location'] != '') {
    $location = (int)$_GET['location'];
    if (!is_numeric($location)) {
        $location = 1;
    }
} else {
    $location = 1;
}

$reviews = func_query("SELECT * FROM sr_review WHERE status = 1 AND review = 'Upload' AND location = '$location' ORDER BY id DESC");
$text_reviews = func_query('SELECT * FROM sr_review WHERE status = 1 AND review != "Upload" AND location = '.$location.' ORDER BY id DESC');

$videoText = func_query_first("SELECT videoText FROM sr_locations WHERE id = " . $location);
$videoText = $videoText['videoText'];

$audioText = func_query_first("SELECT audioText FROM sr_locations WHERE id = " . $location);
$audioText = $audioText['audioText'];
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Bootstrap -->
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.vertical-tabs.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->  

    <link rel="stylesheet" href="listing/style.css" media="screen"/>
    <link href="//vjs.zencdn.net/5.2.4/video-js.css" rel="stylesheet"/>

  </head>
  <body style="background-color: #fff !important;">
  
    <style>
    .vid_dms video {
        -webkit-transform: rotate(90deg); /* Safari and Chrome */
        -moz-transform: rotate(90deg);   /* Firefox */
        -ms-transform: rotate(90deg);   /* IE 9 */
        -o-transform: rotate(90deg);   /* Opera */
        transform: rotate(90deg);
    }
    
    .vid_dms_android video {
        -webkit-transform: rotate(270deg); /* Safari and Chrome */
        -moz-transform: rotate(270deg);   /* Firefox */
        -ms-transform: rotate(270deg);   /* IE 9 */
        -o-transform: rotate(270deg);   /* Opera */
        transform: rotate(270deg);
    }
    </style>

  <div class="container" id="frame-wrapper" style="width: 100% !important;">
    <div class="row">

       <div class="col-md-3 col-sm-4"> <!-- required for floating -->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tabs-left">
              <li class="active"><a href="#reviews" data-toggle="tab"><i class="fa fa-pencil-square fa-lg fa-fw"></i> Reviews</a></li>
              <li><a href="#video-reviews" data-toggle="tab"><i class="fa fa-video-camera fa-lg fa-fw"></i>  Video Reviews</a></li>
              <li><a href="#audio-review" data-toggle="tab"><i class="fa fa-microphone fa-lg fa-fw"></i> Audio Reviews</a></li>
            </ul>
        </div>

            <div class="col-md-9 col-sm-8">
                <!-- Tab panes -->
                <div class="tab-content">

                  <div class="tab-pane fade in active" id="reviews">

                    <?php if (!$text_reviews): ?>
                        <p style="text-align: center; margin-top:80px;">No reviews found.</p>
                    <?php else: ?>
                    <?php foreach ($text_reviews as $r): ?>
                        <?php $usr = func_query_first("SELECT firstname,lastname FROM sr_newuser WHERE id = ".$r['user_id']); ?>
                        <?php $date = func_query_first("SELECT entry_date FROM sr_rating WHERE id = ".$r['rating_id']); ?>
                        <?php $date = strtotime($date['entry_date']); $date = date('m/d/Y',$date); ?>
                        <div class="row" itemscope itemtype="http://schema.org/Review">
                            <div class="col-md-3 col-sm-4 frame-review-details">
    
                                <div itemprop="author" itemscope itemtype="http://schema.org/Person">
                                    <h2 itemprop="name"><?php echo $usr['firstname']. ' ' .$usr['lastname']; ?></h2>
                                </div>
        
                                <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="frame-item-rating">
                                    <meta itemprop="worstRating" content="1"><span itemprop="ratingValue"><?php echo $r['rating_no']; ?></span> / <span itemprop="bestRating">5</span> stars
                                </div>
                                
                                <div class="frame-item-reviews">
                                    <?php $rest = 5 - $r['rating_no']; ?>
                                    <?php for($i=0;$i<$r['rating_no'];$i++): ?>
                                        <i class="fa fa-star"></i>
                                    <?php endfor; ?>
                                    <?php if ($rest > 0): ?>
                                        <?php for($i=0;$i<$rest;$i++): ?>
                                            <i class="fa fa-star-o"></i>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="frame-date-reviews"><meta itemprop="datePublished" content="<?php echo $date; ?>"><i class="fa fa-clock-o"></i> <?php echo $date; ?></div>
    
                            </div>
                            
                            <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization" style="display: none;">
                                <span itemprop="name">Institute for Womans Health</span>
                            </div>
    
                            <div class="col-md-9 col-sm-8 frame-review-content">
                                <h3 itemprop="name">Review by <?php echo $usr['firstname']. ' ' .$usr['lastname']; ?></h3>
                                <p itemprop="reviewBody"><?php echo nl2br($r['review']); ?></p>
                            </div>
    
                        </div>
                        <!-- // row  -->
                        <hr/>
                    <?php endforeach; ?>
                <?php endif; ?>


            <!--div class="col-md-12 frame-page-navi">
            <nav>
              <ul class="pagination">
                <li>
                  <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>                 
            </div-->


           </div>
           <!-- // reviews  -->

                  <div class="tab-pane fade" id="video-reviews">
                  
                    <?php if (!$reviews): ?>
                        <p style="text-align: center; margin-top:80px;">No video reviews found.</p>
                    <?php else: ?>
                        <?php $c = 0; ?>
                        <?php foreach ($reviews as $r): ?>
                            <?php $usr = func_query_first("SELECT firstname,lastname FROM sr_newuser WHERE id = ".$r['user_id']); ?>
                            <?php $data = func_query_first("SELECT file,date FROM sr_reviews WHERE rating_id = ".$r['rating_id']); ?>
                            <?php 
                            $a = explode('/',$data['file']);
                            $file = end($a);
                            $ext = explode('.',$file);
                            $mp4 = false;
                            if ($ext[1] == 'MOV') {
                                if (file_exists('/home1/oneqrepu/public_html/ifwh/done/' . $file . '.mp4')) {
                                    $c1 = 'thumb_' . basename($file,'.MOV');
                                    $google = '//108.179.231.123/ifwh/done/'.$file.'.mp4';
                                    $file = 'done/'.$file.'.mp4';
                                    $c++;
                            ?>
                      <div class="row">

                          <div class="col-md-5 col-sm-6">
                              <div class="frame-video">

                                <div class="item">
                                    <div class="view2 view-first2">
                                        <video class="video-js" controls preload="none" width="320" height="320" data-setup='{"poster":"//108.179.231.123/ifwh/done/<?php echo $c1; ?>.jpg"}' >
                                            <source src="<?php echo $file; ?>" type="video/mp4"/>
                                        </video>
                                    </div>
                                </div><!-- // item -->  

                            
                              </div>
                              <!-- // frame video  -->
                          </div>
                          <!-- // col md 6  -->

                          <div class="col-md-6 col-sm-6 ">

                              <div class="frame-video-desc">
                                  
                                 <div itemscope itemtype="http://schema.org/Review">


                            <div itemprop="author" itemscope itemtype="http://schema.org/Person">
                            <h2 itemprop="name"><?php echo $usr['firstname']. ' ' .$usr['lastname']; ?></h2>
                        
                            <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="frame-item-rating">

                                <meta itemprop="worstRating" content="1"><span itemprop="ratingValue"><?php echo $r['rating_no']; ?></span> / <span itemprop="bestRating">5</span> stars

                            </div>
                            <!-- // review rating  -->

                                    <div class="frame-item-reviews">
                                        <?php $rest = 5 - $r['rating_no']; ?>
                                        <?php for($i=0;$i<$r['rating_no'];$i++): ?>
                                            <i class="fa fa-star"></i>
                                        <?php endfor; ?>
                                        <?php if ($rest > 0): ?>
                                            <?php for($i=0;$i<$rest;$i++): ?>
                                                <i class="fa fa-star-o"></i>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </div>
                                    <!-- // frame item reviews  -->

                                    <div class="frame-date-reviews">
                                        <meta itemprop="datePublished" content="<?php echo date('m/d/Y',$data['date']); ?>">
                                        <i class="fa fa-clock-o"></i> <?php echo date('m/d/Y',$data['date']); ?>
                                    </div>
                                    <!-- // date  -->

                                </div>
                                <!-- // author  -->
                                <script type="application/ld+json">
                                {
                                  "@context": "http://schema.org",
                                  "@type": "VideoObject",
                                  "name": "Institute for Womans Health",
                                  "description": "Institute for Womans Health",
                                  "thumbnailUrl": "http://108.179.231.123/ifwh/done/<?php echo $c1; ?>.jpg",
                                  "uploadDate": "<?php echo date('Y-m-d',$data['date']); ?>",
                                  "contentUrl": "<?php echo $google; ?>"
                                }
                                </script>
                              </div>
                              <!-- // frame videod esc  -->

                          </div>
                          <!-- // col md 6  -->

                        </div>


                      </div>
                      <?php } } else if ($ext[1] == '3gp') { ?>
                      <?php
                      if (file_exists('/home1/oneqrepu/public_html/ifwh/done/' . $file. '.mp4')) {
                            $file = 'ifwh/done/'.$file. '.mp4';
                            $c++;
                      ?>
                      <div class="row">

                          <div class="col-md-5 col-sm-6">
                              <div class="frame-video">

                                <div class="item">
                                    <div class="view2 view-first2">
                                     <video class="video-js" controls preload="none" width="320" height="320" data-setup='{"poster":"//108.179.231.123/ifwh/done/<?php echo $c1; ?>.jpg"}' >
                                        <source src="<?php echo $file; ?>" type="video/mp4"/>
                                     </video>
                                    </div>
                                </div><!-- // item -->  

                            
                              </div>
                              <!-- // frame video  -->
                          </div>
                          <!-- // col md 6  -->

                          <div class="col-md-6 col-sm-6 ">

                              <div class="frame-video-desc">
                                  
                                 <div itemscope itemtype="http://schema.org/Review">


                            <div itemprop="author" itemscope itemtype="http://schema.org/Person">
                            <h2 itemprop="name"><?php echo $usr['firstname']. ' ' .$usr['lastname']; ?></h2>
                        
                            <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="frame-item-rating">

                                <meta itemprop="worstRating" content="1"><span itemprop="ratingValue"><?php echo $r['rating_no']; ?></span> / <span itemprop="bestRating">5</span> stars

                            </div>
                            <!-- // review rating  -->

                                    <div class="frame-item-reviews">
                                        <?php $rest = 5 - $r['rating_no']; ?>
                                        <?php for($i=0;$i<$r['rating_no'];$i++): ?>
                                            <i class="fa fa-star"></i>
                                        <?php endfor; ?>
                                        <?php if ($rest > 0): ?>
                                            <?php for($i=0;$i<$rest;$i++): ?>
                                                <i class="fa fa-star-o"></i>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </div>
                                    <!-- // frame item reviews  -->

                                    <div class="frame-date-reviews">
                                        <meta itemprop="datePublished" content="<?php echo date('m/d/Y',$data['date']); ?>">
                                        <i class="fa fa-clock-o"></i> <?php echo date('m/d/Y',$data['date']); ?>
                                    </div>
                                    <!-- // date  -->

                                </div>
                                <!-- // author  -->
                                <script type="application/ld+json">
                                {
                                  "@context": "http://schema.org",
                                  "@type": "VideoObject",
                                  "name": "Institute for Womans Health",
                                  "description": "Institute for Womans Health",
                                  "thumbnailUrl": "http://108.179.231.123/ifwh/done/<?php echo $c1; ?>.jpg",
                                  "uploadDate": "<?php echo date('Y-m-d',$data['date']); ?>",
                                  "contentUrl": "<?php echo $google; ?>"
                                }
                                </script>
                              </div>
                              <!-- // frame videod esc  -->

                          </div>
                          <!-- // col md 6  -->

                        </div>


                      </div>
                      <hr>
                      <?php } } else { echo '<p style="text-align: center; margin-top:80px;">No video reviews found.</p>'; } ?>
                      <!-- // row  -->

                      
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </div>
                  <!-- // video reviews  -->





                  <div class="tab-pane fade" id="audio-review">
                      <?php if (!$reviews): ?>
                        <p style="text-align: center; margin-top:80px;">No audio reviews found.</p>
                      <?php else: ?>
                        <?php foreach ($reviews as $r): ?>
                        <?php $usr = func_query_first("SELECT firstname,lastname FROM sr_newuser WHERE id = ".$r['user_id']); ?>
                        <?php $data = func_query_first("SELECT file,date FROM sr_reviews WHERE rating_id = ".$r['rating_id']); ?>
                        <?php 
                        $a = explode('/',$data['file']);
                        $file = end($a);
                        $ext = explode('.',$file);
                        $mp4 = false;
                        if ($ext[1] != 'MOV') {
                            if (file_exists('/home1/oneqrepu/public_html/ifwh/done/' . $file . '.mp3')) {
                                $file = '//108.179.231.123/ifwh/done/'.$file.'.mp3';
                            ?>
                    <div class="row">

                          <div class="col-md-5 col-sm-5">
                              <div class="frame-video">

                                <audio class="audio" id="audio-player" src="<?php echo $file; ?>" type="audio/mp3" controls="controls"></audio>
                            
                              </div>
                              <!-- // frame video  -->
                          </div>
                          <!-- // col md 6  -->

                          <div class="col-md-6 col-sm-7 ">

                              <div class="frame-audio-desc">
                                  
                                 <div>


                            <div>
                            <h2><?php echo $usr['firstname']. ' ' .$usr['lastname']; ?></h2>
                        
                            <div class="frame-item-rating">

                                <span itemprop="ratingValue"><?php echo $r['rating_no']; ?></span> / <span>5</span> stars

                            </div>
                            <!-- // review rating  -->

                                    <div class="frame-item-reviews">
                                        <?php $rest = 5 - $r['rating_no']; ?>
                                        <?php for($i=0;$i<$r['rating_no'];$i++): ?>
                                            <i class="fa fa-star"></i>
                                        <?php endfor; ?>
                                        <?php if ($rest > 0): ?>
                                            <?php for($i=0;$i<$rest;$i++): ?>
                                                <i class="fa fa-star-o"></i>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </div>
                                    <!-- // frame item reviews  -->

                                    <div class="frame-date-reviews">
                                        <i class="fa fa-clock-o"></i> <?php echo date('m/d/Y',$data['date']); ?>
                                    </div>
                                    <!-- // date  -->

                                </div>
                                <!-- // author  -->


                              </div>
                              <!-- // frame videod esc  -->

                          </div>
                          <!-- // col md 6  -->

                        </div>
                        <script type="application/ld+json">
                        {
                          "@context": "http://schema.org/",
                          "@type": "Review",
                          "itemReviewed": {
                            "@type": "Organization",
                            "name": "Institute for Womens Health"
                          },
                          "reviewRating": {
                            "@type": "Rating",
                            "ratingValue": "<?php echo $r['rating_no']; ?>"
                          },
                          "name": "Institute for Womens Health",
                          "author": {
                            "@type": "Person",
                            "name": "<?php echo $usr['firstname']. ' ' .$usr['lastname']; ?>"
                          },
                          "reviewBody": "Institute for Womens Health",
                          "publisher": {
                            "@type": "Organization",
                            "name": "Institute for Womens Health"
                          }
                        }
                        </script>
                      </div>
                      <!-- // row  -->

                      <hr>
                      <?php } } ?>
                      <?php endforeach; ?>
                      <?php endif; ?>
                  </div>
                  <!-- // audio review  -->

                </div>
            </div>  



    </div>
    <!-- // row  -->

    <div class="row" id="page-footer" style="margin-top: 15px;">
        <div class="col-md-12 col-sm-12">          

        <small>Powered by <a href="http://digitalmarketingsapiens.com/reputation/" target="_blank">Reputation Management App</a>, Designed by <a href="http://digitalmarketingsapiens.com/" target="_blank">DMSapiens.</a></small>

        </div>
        <!-- // copy bar  -->

    </div>
    <!-- // footer  -->

  </div>
  <!-- // frame wrapper  -->

    <script src="js/vendor/modernizr-2.7.1.min.js"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    <script src="js/plugin.js"></script>
    <script src="js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>

    <script src="listing/assets/mediaelement-and-player.min.js"></script>
    <script src="//vjs.zencdn.net/5.2.4/video.js"></script>
  </body>
</html>