<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="assets/mediaelement-and-player.min.js"></script>
    
    <link rel="stylesheet" href="style.css" media="screen"/>
    <style>
    video {
        -webkit-transform: rotate(90deg); /* Safari and Chrome */
        -moz-transform: rotate(90deg);   /* Firefox */
        -ms-transform: rotate(90deg);   /* IE 9 */
        -o-transform: rotate(90deg);   /* Opera */
        transform: rotate(90deg);
    }
    .videos {
        margin-bottom:10px !important;
    }
    </style>
    
      <link href="http://vjs.zencdn.net/5.2.4/video-js.css" rel="stylesheet"/>
      
</head>
<?php
$servername = 'localhost';
$username = 'mobilec6_survey';
$password = 'wQ=tkV-&C;xW';
$db = 'mobilec6_survey';

$con = mysqli_connect($servername, $username, $password,$db);

function func_query_first($sql){
    global $con;
    $result = mysqli_query($con,$sql);
	if(!empty($result)){
	   return $row = mysqli_fetch_array($result,MYSQL_ASSOC);
	}else{
		return $row = false;
	}
}

function func_query($sql) {
    global $con;
    $myArray = array();
	$result = mysqli_query($con,$sql);
	if(!empty($result)){
		while($row = mysqli_fetch_array($result, MYSQL_ASSOC)){
			$myArray[] = $row;
		}
		return $myArray;
	}
	else
	{
		return false;
	}
}

$q = func_query("SELECT * FROM sr_reviews ORDER BY id DESC");

if(!$q) die("No reviews so far");
?>

<div style="float:left; width:100%;">
    <div style="width: 300px; float:left;">
    <h1>Audio Reviews</h1>
    <?php foreach ($q as $r): ?>
        <?php $i = func_query_first("SELECT firstname,lastname FROM sr_newuser WHERE id = " . $r['userId']); ?>
        <?php 
        $a = explode('/',$r['file']);
        $file = end($a);
        $ext = explode('.',$file);
        $mp3 = false;
        if ($ext[1] == 'MOV') {
            $file = 'http://mobilecommz.com/smoke/reputationapp/uploads/'.$file;
            $mp3 = false;
        } else {
            if (file_exists('/home/mobilec6/public_html/smoke/reputationapp/done/' . $file . '.mp3')) {
                $file = 'http://mobilecommz.com/smoke/reputationapp/done/'.$file.'.mp3';
                $mp3 = true;
            } else {
                continue;
            }
        }
         
        ?>
        <?php if ($mp3): ?>
        <div class="audio-player">
            <h1><?php echo $i['firstname'].' '.$i['lastname']; ?></h1>
            <audio class="audio" id="audio-player" src="<?php echo $file; ?>" type="audio/mp3" controls="controls"></audio>
        </div>
        <?php endif;?>
    <?php endforeach; ?>
    </div>
    <div style="width: 500px; float:left; margin-left:10px;">
    <h1>Video Reviews</h1>
    <?php foreach ($q as $r): ?>
        <?php $i = func_query_first("SELECT firstname,lastname FROM sr_newuser WHERE id = " . $r['userId']); ?>
        <?php 
        $a = explode('/',$r['file']);
        $file = end($a);
        $ext = explode('.',$file);
        $mp4 = false;
        if ($ext[1] == 'MOV') {
            if (file_exists('/home/mobilec6/public_html/smoke/reputationapp/done/' . $file . '.mp4')) {
                $file = '../reputationapp/done/'.$file.'.mp4';
                #$file = 'http://mobilecommz.com/smoke/reputationapp/done/'.$file.'.mp4';
                $mp4 = true;
            } else {
                continue;
            }
        } else {
            $mp4 = false;
        }
         
        ?>
        <?php if ($mp4): ?>
        <!--div id="stage">
            <video width="240" height="320">
              <source src="<?php echo $file; ?>" type="video/mp4">                
                <p>Your browser doesn't support the HTML5 video tag it seems. 
                   You can see this video as part of a collection 
                   <a href="http://www.archive.org/download/AnimatedMechanicalArtPiecesAtMit/">
                     at archive.org</a>.
                </p>         
            </video>
            <div id="controls"></div>
        <video width="320" height="240" poster="poster.jpg" controls="controls" preload="none" style="margin-bottom: 10px;">
            <source type="video/mp4" src="<?php echo $file; ?>" />
            <object width="320" height="240" type="application/x-shockwave-flash" data="assets/flashmediaelement.swf">
                <param name="movie" value="assets/flashmediaelement.swf" />
                <param name="flashvars" value="controls=true&file=<?php echo $file; ?>" />
                <img src="video.gif" width="320" height="240" title="No video playback capabilities" />
            </object>
        </video>
        </div-->
        
        <div class="videos">
            <video class="video-js" controls preload="none" width="320" height="320" data-setup="{}">
                <source src="<?php echo $file; ?>" type="video/mp4"/>
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video.
                </p>
            </video>
        </div>
        
        <?php endif;?>
    <?php endforeach; ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.audio').mediaelementplayer({
            alwaysShowControls: true,
            features: ['playpause','volume','progress'],
            audioVolume: 'horizontal',
            audioWidth: 400,
            audioHeight: 120
        });
    });
</script>

<script src="http://vjs.zencdn.net/5.2.4/video.js"></script>